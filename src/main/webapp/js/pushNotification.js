//var VueResource = require('vue-resource');
//Vue.use(VueResource);

var getOrganization = function() {
	$.ajax({
		url : '/MedicalApi/OrganizationList', // url位置
		type : 'post', // post/get
		data : {}, // 輸入的資料
		success : function(response) {
			if (response.result == 'success') { 
				pushNotification.organizationList = [];
				for (var i = 0; i < response.data.length; i++) {
					var obj = {}
					obj.Name = response.data[i].Name;
					pushNotification.organizationList.push(obj);	
				}
			}
		},// 成功後要執行的函數
		error : function(e) {
			alert(JSON.stringify*(e))
		} // 錯誤後執行的函數
	});

	// this.$http.post("http://survey.med-net.com:8080/MedicalApi/GetAppVersion",{}).then(
	// function (response) {
	// console.log(response);
	// }).catch(function (error) {
	// console.log(error);
	//    
	// });

};

var save = function() {

	var data = {
			message : pushNotification.message,
			messageEn : pushNotification.messageEn,
			maxAge : pushNotification.maxAge,
			minAge : pushNotification.minAge,
			organization : pushNotification.organization,
			gender : pushNotification.gender,
			//beforeDate : pushNotification.beforeDate,
			messageEn : pushNotification.messageEn,
			url : pushNotification.url
	}
	if(pushNotification.beforeDate) {
		data.beforeDate = '1'
	}else{
		data.beforeDate = ''
	}

	$.ajax({
		url : '/MedicalApi/SendAdvertisementNotification', // url位置
		type : 'post', // post/get
		contentType : 'application/json; charset=utf-8',
	    dataType: "json",
		data : JSON.stringify(data), // 輸入的資料
		success : function(response) {
			alert('發送成功');
		},// 成功後要執行的函數
		error : function(e) {
			alert('發送失敗 ',JSON.stringify(e));
		} // 錯誤後執行的函數
	});
}

var pushNotification = new Vue({
	el : '#pushNotification',
	data : {
		organizationList:[],		
		message : '',
		messageEn : '',
		url : '',
		maxAge : '',
		minAge : '',
		organization : '',
		gender : '',
		beforeDate : ''
	},
	  
	created : function() {
		getOrganization();
//		var ops = {
//		          dayNames: ["星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"],
//		          dayNamesMin: ["日", "一", "二", "三", "四", "五", "六"],
//		          monthNames: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],
//		          monthNamesShort: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],
//		          prevText: "上月",
//		          nextText: "次月",
//		          weekHeader: "週",
//		          showMonthAfterYear: true,
//		          dateFormat: "yy-mm-dd"
//		        }
//		        $('#date1').datepicker(ops)		
		
		
	},
	methods : {
		 save : save
	}
});
