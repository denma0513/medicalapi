
var link = function() {

	var strURL = window.location.toString(); //取得URL
	var getPara, ParaVal; 
	var aryPara = [];
	var aryParaContent = [];
	//取得URL參數
	if (strURL.indexOf("?") != -1){
			var getSearch = strURL.split("?");
			getPara = getSearch[1].split("&");
			for (i=0 ; i < getPara.length; i++){
				ParaVal = getPara[i].split("=");
				aryPara.push(ParaVal[0]);
				aryParaContent.push(ParaVal[1]);		
			}
	}
	
	var u = navigator.userAgent;
	var isAndroid = u.indexOf('Android') > -1 || u.indexOf('Adr') > -1; //判斷是否為Android
	var isiOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/); //判斷是否為ios
	//var isWindowsPhone = !!u.match(/Windows Phone/);//判斷是否為Windows phone
	
	UrlPara = "";
	for (i=0 ; i < aryPara.length; i++){
		UrlPara = UrlPara + aryPara[i]+ "=" + aryParaContent[i] + "&";
	}
	UrlPara = UrlPara.substr(0,UrlPara.length -1);

	//判斷IOS
	
	if (isiOS) {
		window.location.href = "MednetApp://"+UrlPara; //開啟app的協議，優先執行此URL	
		window.setTimeout(function(){
			window.location.href = "https://apps.apple.com/tw/app/%E6%99%BA%E6%85%A7%E5%81%A5%E8%A8%BA/id1459458752"; //超過一定時間則執行此URL EX:下載app的地址
		},2000);
	}
	
	//判斷android
	else if (isAndroid) {
		window.location.href = "mednetapp://shareapp?"+UrlPara; //開啟app的協議，優先執行此URL
		window.setTimeout(function(){
			window.location.href = "https://play.google.com/store/apps/details?id=com.mgear.mednetapp"; //超過一定時間則執行此URL EX:下載app的地址
		},2000);
	}
	//兩者皆不是的話
	else{
		var LinkUrl = "https://med-net.com/";
    	window.location.href =  LinkUrl;
	}
	
};

var changelink = new Vue({
	el : '#changelink',

	created : function() {
		console.log('created');
		link();
	}

});
