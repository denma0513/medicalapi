/*
var getOrganization = function() {
	$.ajax({
		url : '/MedicalApi/OrganizationList', // url位置
		type : 'post', // post/get
		data : {}, // 輸入的資料
		success : function(response) {
			if (response.result == 'success') { 
				pushNotification.organizationList = [];
				for (var i = 0; i < response.data.length; i++) {
					var obj = {}
					obj.Name = response.data[i].Name;
					pushNotification.organizationList.push(obj);	
				}
			}
		},// 成功後要執行的函數
		error : function(e) {
			alert(JSON.stringify*(e))
		} // 錯誤後執行的函數
	});

	// this.$http.post("http://survey.med-net.com:8080/MedicalApi/GetAppVersion",{}).then(
	// function (response) {
	// console.log(response);
	// }).catch(function (error) {
	// console.log(error);
	//    
	// });

};
*/
var save = function() {

	var data = {
			pushToken : pushTokenSave.pushToken,
			device : pushTokenSave.device,
			customerId : pushTokenSave.customerId
	}
	$.ajax({
		url : '/MedicalApi/RegisterPushTokenSave', // url位置
		type : 'post', // post/get
		contentType : 'application/json; charset=utf-8',
	    dataType: "json",
		data : JSON.stringify(data), // 輸入的資料
		success : function(response) {
			alert('True');
		},// 成功後要執行的函數
		error : function(e) {
			alert('False ',JSON.stringify(e));
		} // 錯誤後執行的函數
	});
}

var load = function() {

	var data = {
			customerId : pushTokenSave.loadcustomerId
	}
	$.ajax({
		url : '/MedicalApi/sendMessageWithCustomerId', // url位置
		type : 'post', // post/get
		contentType : 'application/json; charset=utf-8',
	    dataType: "json",
		data : JSON.stringify(data), // 輸入的資料
		success : function(response) {
			alert('True');
		},// 成功後要執行的函數
		error : function(e) {
			alert('False ',JSON.stringify(e));
		} // 錯誤後執行的函數
	});
}

var pushTokenSave = new Vue({
	el : '#pushTokenSave',
	data : {
		pushToken : 'pushtoken',
		device : 'device',
		customerId : 'customerid',
			
		loadpushToken : 'loadpushtoken',
		loaddevice : 'loaddevice',
		loadcustomerId : 'customerid'
		
	},
	  
	created : function() {
	},
	methods : {
		 save : save
	},
	methods : {
		 load : load
	}
});
