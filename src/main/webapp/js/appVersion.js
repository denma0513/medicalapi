//var VueResource = require('vue-resource');
//Vue.use(VueResource);
console.log(444)

var check = function() {
	console.log(app.check)
	app.check = !(app.check);
}

var getVersion = function() {
	$.ajax({
		// url: 'http://survey.med-net.com:8080/MedicalApi/GetAppVersion', //
		url : '/MedicalApi/GetAppVersion', // url位置
		type : 'post', // post/get
		data : {}, // 輸入的資料
		success : function(response) {
			console.log(response);
			if (response.result == 'success') {
				appVersion.iosVersion = response.data.ios;
				appVersion.androidVersion = response.data.android;

			}
		},// 成功後要執行的函數
		error : function(e) {
		} // 錯誤後執行的函數
	});

	// this.$http.post("http://survey.med-net.com:8080/MedicalApi/GetAppVersion",{}).then(
	// function (response) {
	// console.log(response);
	// }).catch(function (error) {
	// console.log(error);
	//    
	// });

};

var save = function() {

	var data = {
		ios : appVersion.iosVersion,
		android : appVersion.androidVersion
	}

	console.log(data);
	$.ajax({
		// url: 'http://survey.med-net.com:8080/MedicalApi/GetAppVersion', //
		url : '/MedicalApi/SetAppVersion', // url位置
		type : 'post', // post/get
		contentType : 'application/json; charset=utf-8',
	    dataType: "json",
		data : JSON.stringify(data), // 輸入的資料
		success : function(response) {
			getVersion();
		},// 成功後要執行的函數
		error : function(e) {
		} // 錯誤後執行的函數
	});
}

var appVersion = new Vue({
	el : '#appVersion',
	data : {
		androidVersion : '1.0.0',
		iosVersion : '1.0.0'
	},

	created : function() {
		console.log('created');
		getVersion();
	},
	methods : {
		save : save
	}
});
