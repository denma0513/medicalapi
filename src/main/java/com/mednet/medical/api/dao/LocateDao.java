package com.mednet.medical.api.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mednet.medical.api.base.baseService;
import com.mednet.medical.api.utils.DbUtils;
import com.mednet.medical.api.utils.config.ConstManager;

@Service
public class LocateDao extends baseService {
	private @Autowired HttpServletRequest request;

	public List<HashMap<String, Object>> queryOrganizationList(String organizationID) {
		StringBuffer sql = new StringBuffer();
		ArrayList<String> columnType = new ArrayList<String>();
		ArrayList<String> columnValue = new ArrayList<String>();

		// columnType.add("String");
		// columnValue.add("APP_MEDNET");

		sql.append("SELECT * FROM " + ConstManager.getMednetSchema(request) + ".DBO.ORGANIZATION \n");
		sql.append("WHERE ID = ? \n");

		columnType.add("String");
		columnValue.add(organizationID);

		List<HashMap<String, Object>> result = DbUtils.getInstance().excuteQueryL(sql.toString(), columnType,
				columnValue);
		return result;
	}

	public int createLocate(String guid, String organizationID, String name, String nameEn, String type) {
		StringBuffer sql = new StringBuffer();
		ArrayList<String> columnType = new ArrayList<String>();
		ArrayList<String> columnValue = new ArrayList<String>();

		sql.append("INSERT INTO APP_MEDNET.DBO.LOCATE   		\n");
		sql.append("(ID, ORGANIZATIONID, NAME, NAMEEN, type) 	\n");
		sql.append("VALUES (?, ?, ?, ?, ?) 						\n");

		columnType.add("String");
		columnValue.add(guid);
		columnType.add("String");
		columnValue.add(organizationID);
		columnType.add("String");
		columnValue.add(name);
		columnType.add("String");
		columnValue.add(nameEn);
		columnType.add("String");
		columnValue.add(type);

		int result = DbUtils.getInstance().updateExcute(sql.toString(), columnType, columnValue);
		return result;
	}

}
