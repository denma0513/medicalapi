package com.mednet.medical.api.dao;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mednet.medical.api.base.baseService;
import com.mednet.medical.api.utils.CommonUtils;
import com.mednet.medical.api.utils.DbUtils;
import com.mednet.medical.api.utils.config.ConstManager;

@Service
public class AIDoctorDao extends baseService {

	private @Autowired HttpServletRequest request;
	
	/**
	 *
	 * @param pushToken
	 * @param device
	 * @param customerid

	 * */
	//public int createPushToken(String pushToken, String organization, String name, String age ,String gender,String phoneNumber,String englishVersion) {
	public List<HashMap<String, Object>> getCustomerFromMednet(String customerID)
	{
		StringBuffer sql = new StringBuffer(); 
		ArrayList<String> columnType = new ArrayList<String>();
		ArrayList<String> columnValue = new ArrayList<String>();

		sql.append("SELECT *FROM [med-net.com].dbo.firstnet_customers where id = ?	\n");
//		sql.append("WHERE customer_app_Id	= ?														    \n");
//		 
		columnType.add("int");
		columnValue.add(customerID);
		List<HashMap<String, Object>>  list;
		list = DbUtils.getInstance().excuteExpertQAQueryL(sql.toString(), columnType, columnValue);
		return list;
	}
	
	public List<HashMap<String, Object>> saveResultData(String customerID,String symptoms,String urgentQuestion,String generalQuestion,String MergeQuestion,String targetId,String customerInput,String age,String gender,String date,String updateTime)
	{
		StringBuffer sql = new StringBuffer(); 
		ArrayList<String> columnType = new ArrayList<String>();
		ArrayList<String> columnValue = new ArrayList<String>();

		sql.append("INSERT INTO MedNet_AIdoctor.dbo.resultData " + 
				"OUTPUT INSERTED.result_id " + 
				"VALUES(?,?,?,?,?,?,?,?,?,?,'0',?)"
				+ "\n");

		columnType.add("String");
		columnValue.add(customerInput);
		columnType.add("String");
		columnValue.add(symptoms);
		columnType.add("String");
		columnValue.add(urgentQuestion);
		columnType.add("String");
		columnValue.add(generalQuestion);
		columnType.add("String");
		columnValue.add(MergeQuestion);
		columnType.add("String");
		columnValue.add(targetId);
		columnType.add("String");
		columnValue.add(customerID);
		columnType.add("String");
		columnValue.add(age);
		columnType.add("int");
		columnValue.add(gender);
		columnType.add("String");
		columnValue.add(date);
		columnType.add("String");
		columnValue.add(date);
		List<HashMap<String, Object>>  list;
		list = DbUtils.getInstance().excuteQueryL(sql.toString(), columnType, columnValue);
		return list;
	}
	
	public int updateResultData(String resultId,String symptoms,String urgentQuestion,String generalQuestion,String MergeQuestion,String targetId,String customerInput,String updateTime)
	{
		StringBuffer sql = new StringBuffer(); 
		ArrayList<String> columnType = new ArrayList<String>();
		ArrayList<String> columnValue = new ArrayList<String>();

		sql.append("UPDATE MedNet_AIdoctor.dbo.resultData SET [enable] = 0 \n");

		if(!CommonUtils.isBlank(symptoms)) {
			sql.append(",symptoms = ?\n");
			columnType.add("String");
			columnValue.add(symptoms);
		}
		if(!CommonUtils.isBlank(urgentQuestion)) {
			sql.append(",urgentQuestion = ?\n");
			columnType.add("String");
			columnValue.add(urgentQuestion);
		}
		if(!CommonUtils.isBlank(generalQuestion)) {
			sql.append(",generalQuestion = ?\n");
			columnType.add("String");
			columnValue.add(generalQuestion);
		}
		if(!CommonUtils.isBlank(MergeQuestion)) {
			sql.append(",MergeQuestion = ?\n");
			columnType.add("String");
			columnValue.add(MergeQuestion);
		}
		if(!CommonUtils.isBlank(targetId)) {
			sql.append(",targetId = ?\n");
			columnType.add("String");
			columnValue.add(targetId);
		}
		if(!CommonUtils.isBlank(updateTime)) {
			sql.append(",updateTime = ?\n");
			columnType.add("String");
			columnValue.add(updateTime);
		}
		if(!CommonUtils.isBlank(customerInput)) {
			sql.append(",customer_input = ?\n");
			columnType.add("String");
			columnValue.add(customerInput);
		}
		sql.append("WHERE result_id = ?");
		columnType.add("String");
		columnValue.add(resultId);
		return DbUtils.getInstance().updateExcute(sql.toString(), columnType, columnValue);
	}
	
	public int setResultToEnable(String resultId)
	{
		StringBuffer sql = new StringBuffer(); 
		ArrayList<String> columnType = new ArrayList<String>();
		ArrayList<String> columnValue = new ArrayList<String>();

		sql.append("UPDATE MedNet_AIdoctor.dbo.resultData SET [enable] = 1 WHERE result_id = ?");

		columnType.add("int");
		columnValue.add(resultId);
		return DbUtils.getInstance().updateExcute(sql.toString(), columnType, columnValue);
	}
	
	public List<HashMap<String, Object>> getResultList(String customerId)
	{
		StringBuffer sql = new StringBuffer(); 
		ArrayList<String> columnType = new ArrayList<String>();
		ArrayList<String> columnValue = new ArrayList<String>();
		sql.append("Select *From MedNet_AIdoctor.dbo.resultData WHERE customerId = ? and [enable] = 1 ORDER BY [date] DESC");

		columnType.add("String");
		columnValue.add(customerId);
		List<HashMap<String, Object>>  list = DbUtils.getInstance().excuteQueryL(sql.toString(), columnType, columnValue);
		return list;
	}
	
	public List<HashMap<String, Object>>  getSymptomName(List<String> symptomIdList)
	{
		StringBuffer sql = new StringBuffer(); 
		ArrayList<String> columnType = new ArrayList<String>();
		ArrayList<String> columnValue = new ArrayList<String>();
		sql.append("Select *From MedNet_AIdoctor.dbo.symptom\n");
		for(int i = 0 ; i < symptomIdList.size(); i++) {
			if(i == 0) {
				sql.append("WHERE symptom_id = ?\n");
			}
			else {
				sql.append("OR symptom_id = ?\n");
			}
			columnType.add("String");
			columnValue.add(symptomIdList.get(i));
		}
		List<HashMap<String, Object>>  list = DbUtils.getInstance().excuteQueryL(sql.toString(), columnType, columnValue);
		return list;
	}
	
	public HashMap<String, Object>  getResult(String resultId)
	{
		StringBuffer sql = new StringBuffer(); 
		ArrayList<String> columnType = new ArrayList<String>();
		ArrayList<String> columnValue = new ArrayList<String>();
		sql.append("SELECT * FROM MedNet_AIdoctor.dbo.resultData a,MedNet_AIdoctor.dbo.[target] b \n" + 
				"WHERE a.targetId = b.target_id and a.result_id = ? \n");
		columnType.add("String");
		columnValue.add(resultId);
		List<HashMap<String, Object>>  list = DbUtils.getInstance().excuteQueryL(sql.toString(), columnType, columnValue);
		if (list.size() == 1) {
			HashMap<String, Object> object = list.get(0);
			return object;
		}
		return null;
		
	}
	
	public List<HashMap<String, Object>>   getSymptom(List<String> symptomsList)
	{
		StringBuffer sql = new StringBuffer(); 
		ArrayList<String> columnType = new ArrayList<String>();
		ArrayList<String> columnValue = new ArrayList<String>();
		sql.append("select a.symptom_id,a.symptom_content,b.question_id\n" + 
				"from MedNet_AIdoctor.dbo.symptom a ,MedNet_AIdoctor.dbo.urgent_question b\n" + 
				" where a.[enable] = 1 and b.first_question = 1 and a.symptom_id = b.symptom_id and (");
		for (int i = 0 ; i < symptomsList.size() ; i++) {
			if (i == 0) {
				sql.append("a.symptom_content = ? ");
			}
			else {
				sql.append("or a.symptom_content = ? ");
			}
			
			columnType.add("String");
			columnValue.add(symptomsList.get(i));
		}
		sql.append(" )");
		return DbUtils.getInstance().excuteQueryL(sql.toString(), columnType, columnValue);
		
	}
	
	public HashMap<String, Object>   getQuestionNameAndOptionName(String optionId,String questionId)
	{
		StringBuffer sql = new StringBuffer(); 
		ArrayList<String> columnType = new ArrayList<String>();
		ArrayList<String> columnValue = new ArrayList<String>();
		sql.append("select a.option_content,b.question_content\n" + 
				"from MedNet_AIdoctor.dbo.general_option a, MedNet_AIdoctor.dbo.general_question b\n" + 
				"where  a.option_id = ? and b.question_id = ?");
		columnType.add("String");
		columnValue.add(optionId);
		columnType.add("String");
		columnValue.add(questionId);
		List<HashMap<String, Object>> result = DbUtils.getInstance().excuteQueryL(sql.toString(), columnType, columnValue);
		if (result.size() > 0) {
			return result.get(0);
		}
		else {
			return null;
		}
	}
	
	public List<HashMap<String, Object>>   getDivisionId(String[] divisionName)
	{
		StringBuffer sql = new StringBuffer(); 
		ArrayList<String> columnType = new ArrayList<String>();
		ArrayList<String> columnValue = new ArrayList<String>();
		sql.append("select *from dbMedNet_ExpertQA.dbo.tbl_Division where \n");
		for (int i = 0 ; i < divisionName.length; i++) {
			if (i == 0) {
				sql.append("[Name] = ? \n");
			}
			else {
				sql.append("or [Name] = ? \n");
			}
			columnType.add("String");
			columnValue.add(divisionName[i]);
		}
		return DbUtils.getInstance().excuteExpertQAQueryL(sql.toString(), columnType, columnValue);
	}
	
	public List<HashMap<String, Object>>   getRegionCountry()
	{
		StringBuffer sql = new StringBuffer(); 
		ArrayList<String> columnType = new ArrayList<String>();
		ArrayList<String> columnValue = new ArrayList<String>();
		sql.append("\n" + 
				"select  b.RegionID,b.RegionName,a.CodeName\n" + 
				"from [dbMedNet_ExpertQA].[dbo].tbl_SystemConfig a, [dbMedNet_ExpertQA].[dbo].map_RegionCountry b\n" + 
				"where a.[Value] = b.CountryID ");
		return DbUtils.getInstance().excuteExpertQAQueryL(sql.toString(), columnType, columnValue);
	}
	
	public List<HashMap<String, Object>>   getFuntionId(String[] funtionNameList)
	{
		StringBuffer sql = new StringBuffer(); 
		ArrayList<String> columnType = new ArrayList<String>();
		ArrayList<String> columnValue = new ArrayList<String>();
		sql.append("SELECT *\n" + 
				"  FROM [med-net.com].[dbo].[ProductFunctionItem] where [Enable] = 1 and ( \n");
		for (int i = 0 ; i < funtionNameList.length; i++) {
			if (i == 0) {
				sql.append("[NameTW] = ? \n");
			}
			else {
				sql.append("or [NameTW] = ? \n");
			}
			columnType.add("String");
			columnValue.add(funtionNameList[i]);
		}
		sql.append(")");
		return DbUtils.getInstance().excuteExpertQAQueryL(sql.toString(), columnType, columnValue);
	}
	
	public List<HashMap<String, Object>>   getDivisionDescription(String divisionName)
	{
		StringBuffer sql = new StringBuffer(); 
		ArrayList<String> columnType = new ArrayList<String>();
		ArrayList<String> columnValue = new ArrayList<String>();
		sql.append("select c.[Name]\n" + 
				"from dbMedNet_ExpertQA.dbo.tbl_Division a ,[dbMedNet_ExpertQA].[dbo].[map_DivisionExpertItem] b,[dbMedNet_ExpertQA].[dbo].tbl_ExpertItem c\n" + 
				"where a.[Name] = ? and b.DivisionID = a.[ID] and b.ExpertItemID = c.[ID] order by b.Seq");
		columnType.add("String");
		columnValue.add(divisionName);
		return DbUtils.getInstance().excuteExpertQAQueryL(sql.toString(), columnType, columnValue);
	}
	
	public List<HashMap<String, Object>>   getCityGuid(List<String> cityNamelist)
	{
		StringBuffer sql = new StringBuffer(); 
		ArrayList<String> columnType = new ArrayList<String>();
		ArrayList<String> columnValue = new ArrayList<String>();
		sql.append("select *from [med-net.com].[dbo].[ChannelCity] where ");
		for (int i = 0 ; i < cityNamelist.size(); i++) {
			if (i == 0) {
				sql.append(" [Name] = ? ");
			}
			else {
				sql.append(" or [Name] = ? ");
			}
			columnType.add("String");
			columnValue.add(cityNamelist.get(i).toString());
		}
		
		return DbUtils.getInstance().excuteExpertQAQueryL(sql.toString(), columnType, columnValue);
	}
	
	public List<HashMap<String, Object>>   getDoctorCityId(List<String> cityNamelist)
	{
		StringBuffer sql = new StringBuffer(); 
		ArrayList<String> columnType = new ArrayList<String>();
		ArrayList<String> columnValue = new ArrayList<String>();
		sql.append("select *from [dbMedNet_ExpertQA].[dbo].[tbl_SystemConfig] where TypeName = N'地區' and ( ");
		for (int i = 0 ; i < cityNamelist.size(); i++) {
			if (i == 0) {
				sql.append(" [CodeName] = ? ");
			}
			else {
				sql.append(" or [CodeName] = ? ");
			}
			columnType.add("String");
			columnValue.add(cityNamelist.get(i).toString());
		}
		sql.append(")");
		return DbUtils.getInstance().excuteExpertQAQueryL(sql.toString(), columnType, columnValue);
	}
}