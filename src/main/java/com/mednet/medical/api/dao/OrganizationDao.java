package com.mednet.medical.api.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mednet.medical.api.base.baseService;
import com.mednet.medical.api.utils.DbUtils;
import com.mednet.medical.api.utils.config.ConstManager;

@Service
public class OrganizationDao extends baseService {
	private @Autowired HttpServletRequest request;

	public List<HashMap<String, Object>> queryOrganizationList(String organizationID) {
		StringBuffer sql = new StringBuffer();
		ArrayList<String> columnType = new ArrayList<String>();
		ArrayList<String> columnValue = new ArrayList<String>();

		sql.append("SELECT * FROM " + ConstManager.getMednetSchema(request) + ".DBO.ORGANIZATION \n");
		sql.append("WHERE ID = ? \n");

		columnType.add("String");
		columnValue.add(organizationID);
		List<HashMap<String, Object>> result = DbUtils.getInstance().excuteQueryL(sql.toString(), columnType,
				columnValue);
		return result;
	}

}
