package com.mednet.medical.api.dao;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mednet.medical.api.base.baseService;
import com.mednet.medical.api.utils.CommonUtils;
import com.mednet.medical.api.utils.DbUtils;
import com.mednet.medical.api.utils.config.ConstManager;

@Service
public class TestDao extends baseService {

	private @Autowired HttpServletRequest request;
	
	/**
	 *
	 * @param pushToken
	 * @param device
	 * @param customerid

	 * */
	//public int createPushToken(String pushToken, String organization, String name, String age ,String gender,String phoneNumber,String englishVersion) {
	public List<HashMap<String, Object>> getQuestionList()
	{
		StringBuffer sql = new StringBuffer(); 
		ArrayList<String> columnType = new ArrayList<String>();
		ArrayList<String> columnValue = new ArrayList<String>();

		sql.append("SELECT *FROM dbMedNet_ExpertQA.dbo.tbl_Question 	\n");
//		sql.append("WHERE customer_app_Id	= ?														    \n");
//		 
//		columnType.add("int");
//		columnValue.add("9287");
		List<HashMap<String, Object>>  list;
		list = DbUtils.getInstance().excuteExpertQAQueryL(sql.toString(), columnType, columnValue);
		return list;
	}
	
}