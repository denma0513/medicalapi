package com.mednet.medical.api.dao;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mednet.medical.api.base.baseService;
import com.mednet.medical.api.utils.CommonUtils;
import com.mednet.medical.api.utils.DbUtils;
import com.mednet.medical.api.utils.config.ConstManager;


@Service
public class FixEmailDao extends baseService{
	
	private @Autowired HttpServletRequest request;
	
	
	public List<HashMap<String, Object>> getCustomerInformation(String customerAppId, String Email) {		
		StringBuffer sql = new StringBuffer(); 
		ArrayList<String> columnType = new ArrayList<String>();
		ArrayList<String> columnValue = new ArrayList<String>();
		SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

		sql.append("SELECT   oAuth_Type,oAuth_id FROM	med_db.dbo.Med_customer_informations_oAuth		\n");
		sql.append("WHERE customer_app_Id	= ?														    \n");
		 
		columnType.add("String");
		columnValue.add(customerAppId);
		List<HashMap<String, Object>>  list;
		list = DbUtils.getInstance().excuteExpertQAQueryL(sql.toString(), columnType, columnValue);
		return list;
	}
	
	public void fixAppCustomerId(String customerAppId, String customerId , String email) {

		StringBuffer sql = new StringBuffer(); 
		ArrayList<String> columnType = new ArrayList<String>();
		ArrayList<String> columnValue = new ArrayList<String>();
		
		//tabl 1
		sql.append("UPDATE med_db.dbo.Med_appRefresh_token		    	\n");
		sql.append("SET													\n");
		sql.append("customer_id = ?										\n");
		sql.append("WHERE customer_app_id = ?							\n");

		columnType.add("String");
		columnValue.add(customerId);
		columnType.add("String");
		columnValue.add(customerAppId);
		DbUtils.getInstance().updateExpertQAExcute(sql.toString(), columnType, columnValue);
		
		
		sql = new StringBuffer(); 
		columnType = new ArrayList<String>();
		columnValue = new ArrayList<String>();
		
		//tabl 2
		sql.append("UPDATE med_db.dbo.Med_community_friend		    	\n");
		sql.append("SET													\n");
		sql.append("customer_id = ?										\n");
		sql.append("WHERE customer_app_id = ?							\n");

		columnType.add("String");
		columnValue.add(customerId);
		columnType.add("String");
		columnValue.add(customerAppId);
		DbUtils.getInstance().updateExpertQAExcute(sql.toString(), columnType, columnValue);
		
		
		sql = new StringBuffer(); 
		columnType = new ArrayList<String>();
		columnValue = new ArrayList<String>();
		//tabl 3
		sql.append("UPDATE med_db.dbo.Med_community_friend_0520			\n");
		sql.append("SET													\n");
		sql.append("customer_id = ?										\n");
		sql.append("WHERE customer_app_id = ?							\n");

		columnType.add("String");
		columnValue.add(customerId);
		columnType.add("String");
		columnValue.add(customerAppId);
		DbUtils.getInstance().updateExpertQAExcute(sql.toString(), columnType, columnValue);
		
		
		
		
		sql = new StringBuffer(); 
		columnType = new ArrayList<String>();
		columnValue = new ArrayList<String>();
		//tabl 4
		sql.append("UPDATE med_db.dbo.Med_custom_body_mass_weight		\n");
		sql.append("SET													\n");
		sql.append("customer_id = ?										\n");
		sql.append("WHERE customer_app_id = ?							\n");

		columnType.add("String");
		columnValue.add(customerId);
		columnType.add("String");
		columnValue.add(customerAppId);
		DbUtils.getInstance().updateExpertQAExcute(sql.toString(), columnType, columnValue);
		
		
		sql = new StringBuffer(); 
		columnType = new ArrayList<String>();
		columnValue = new ArrayList<String>();
		//tabl 5
		sql.append("UPDATE med_db.dbo.Med_custom_circulatory_system_blood_pressure			\n");
		sql.append("SET													\n");
		sql.append("customer_id = ?										\n");
		sql.append("WHERE customer_app_id = ?							\n");

		columnType.add("String");
		columnValue.add(customerId);
		columnType.add("String");
		columnValue.add(customerAppId);
		DbUtils.getInstance().updateExpertQAExcute(sql.toString(), columnType, columnValue);
		
		
		sql = new StringBuffer(); 
		columnType = new ArrayList<String>();
		columnValue = new ArrayList<String>();
		//tabl 6
		sql.append("UPDATE med_db.dbo.Med_custom_circulatory_system_blood_sugar			\n");
		sql.append("SET													\n");
		sql.append("customer_id = ?										\n");
		sql.append("WHERE customer_app_id = ?							\n");

		columnType.add("String");
		columnValue.add(customerId);
		columnType.add("String");
		columnValue.add(customerAppId);
		DbUtils.getInstance().updateExpertQAExcute(sql.toString(), columnType, columnValue);
		
		
		sql = new StringBuffer(); 
		columnType = new ArrayList<String>();
		columnValue = new ArrayList<String>();
		//tabl 7
		sql.append("UPDATE med_db.dbo.Med_custom_drinking			\n");
		sql.append("SET													\n");
		sql.append("customer_id = ?										\n");
		sql.append("WHERE customer_app_id = ?							\n");

		columnType.add("String");
		columnValue.add(customerId);
		columnType.add("String");
		columnValue.add(customerAppId);
		DbUtils.getInstance().updateExpertQAExcute(sql.toString(), columnType, columnValue);
		
		
		sql = new StringBuffer(); 
		columnType = new ArrayList<String>();
		columnValue = new ArrayList<String>();
		//tabl 8
		sql.append("UPDATE med_db.dbo.Med_custom_sleeping			\n");
		sql.append("SET													\n");
		sql.append("customer_id = ?										\n");
		sql.append("WHERE customer_app_id = ?							\n");

		columnType.add("String");
		columnValue.add(customerId);
		columnType.add("String");
		columnValue.add(customerAppId);
		DbUtils.getInstance().updateExpertQAExcute(sql.toString(), columnType, columnValue);
		
		
		sql = new StringBuffer(); 
		columnType = new ArrayList<String>();
		columnValue = new ArrayList<String>();
		//tabl 9
		sql.append("UPDATE med_db.dbo.Med_custom_step_count			\n");
		sql.append("SET													\n");
		sql.append("customer_id = ?										\n");
		sql.append("WHERE customer_app_id = ?							\n");

		columnType.add("String");
		columnValue.add(customerId);
		columnType.add("String");
		columnValue.add(customerAppId);
		DbUtils.getInstance().updateExpertQAExcute(sql.toString(), columnType, columnValue);
		
		
		sql = new StringBuffer(); 
		columnType = new ArrayList<String>();
		columnValue = new ArrayList<String>();
		//tabl 10
		sql.append("UPDATE med_db.dbo.Med_custom_vital_signs_blood_oxygen			\n");
		sql.append("SET													\n");
		sql.append("customer_id = ?										\n");
		sql.append("WHERE customer_app_id = ?							\n");

		columnType.add("String");
		columnValue.add(customerId);
		columnType.add("String");
		columnValue.add(customerAppId);
		DbUtils.getInstance().updateExpertQAExcute(sql.toString(), columnType, columnValue);
		
		
		sql = new StringBuffer(); 
		columnType = new ArrayList<String>();
		columnValue = new ArrayList<String>();
		//tabl 11
		sql.append("UPDATE med_db.dbo.Med_customer_informations_oAuth			\n");
		sql.append("SET													\n");
		sql.append("customer_id = ?										\n");
		sql.append("WHERE customer_app_id = ?							\n");

		columnType.add("String");
		columnValue.add(customerId);
		columnType.add("String");
		columnValue.add(customerAppId);
		DbUtils.getInstance().updateExpertQAExcute(sql.toString(), columnType, columnValue);
		
		
		sql = new StringBuffer(); 
		columnType = new ArrayList<String>();
		columnValue = new ArrayList<String>();
		//tabl 12
		sql.append("UPDATE med_db.dbo.Med_customers						\n");
		sql.append("SET													\n");
		sql.append("customer_id = ?	, 	email = ?						\n");
		sql.append("WHERE customer_app_id = ?							\n");

		columnType.add("String");
		columnValue.add(customerId);
		columnType.add("String");
		columnValue.add(email);
		columnType.add("String");
		columnValue.add(customerAppId);
		DbUtils.getInstance().updateExpertQAExcute(sql.toString(), columnType, columnValue);
		
		
		sql = new StringBuffer(); 
		columnType = new ArrayList<String>();
		columnValue = new ArrayList<String>();
		//tabl 13
		sql.append("UPDATE med_db.dbo.Med_phone_registered			\n");
		sql.append("SET													\n");
		sql.append("customer_id = ?										\n");
		sql.append("WHERE customer_app_id = ?							\n");

		columnType.add("String");
		columnValue.add(customerId);
		columnType.add("String");
		columnValue.add(customerAppId);
		DbUtils.getInstance().updateExpertQAExcute(sql.toString(), columnType, columnValue);
		
		
		sql = new StringBuffer(); 
		columnType = new ArrayList<String>();
		columnValue = new ArrayList<String>();
		//tabl 14
		sql.append("UPDATE med_db.dbo.Med_push_topic_member			\n");
		sql.append("SET													\n");
		sql.append("customer_id = ?										\n");
		sql.append("WHERE customer_app_id = ?							\n");

		columnType.add("String");
		columnValue.add(customerId);
		columnType.add("String");
		columnValue.add(customerAppId);
		DbUtils.getInstance().updateExpertQAExcute(sql.toString(), columnType, columnValue);
		
		
		sql = new StringBuffer(); 
		columnType = new ArrayList<String>();
		columnValue = new ArrayList<String>();
		//tabl 15
		sql.append("UPDATE med_db.dbo.Med_sms_code			\n");
		sql.append("SET													\n");
		sql.append("customer_id = ?										\n");
		sql.append("WHERE customer_app_id = ?							\n");

		columnType.add("String");
		columnValue.add(customerId);
		columnType.add("String");
		columnValue.add(customerAppId);
		DbUtils.getInstance().updateExpertQAExcute(sql.toString(), columnType, columnValue);
	
	}
}
