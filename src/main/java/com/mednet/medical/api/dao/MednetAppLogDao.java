package com.mednet.medical.api.dao;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mednet.medical.api.base.baseService;
import com.mednet.medical.api.utils.CommonUtils;
import com.mednet.medical.api.utils.DbUtils;
import com.mednet.medical.api.utils.config.ConstManager;


@Service
public class MednetAppLogDao extends baseService{
	
	private @Autowired HttpServletRequest request;
	
	
	public int createMednetAppLogEven(String customerId, String customerAppId, String event , String value , String insertUser , String insertAddress , String device ) {		
		StringBuffer sql = new StringBuffer(); 
		ArrayList<String> columnType = new ArrayList<String>();
		ArrayList<String> columnValue = new ArrayList<String>();
		SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

		sql.append("INSERT INTO Medical_HQ.dbo.mednetapp_log_event  				 	\n");
		sql.append("(																    \n");
		sql.append("customer_id,customer_app_id,event,value								\n");
		sql.append(",insert_time,insert_user,insert_address,device						\n");
		sql.append(")																	\n");
		sql.append("VALUES (?,?,?,?,?,?,?,?)											\n");
		 
		columnType.add("String");
		columnValue.add(customerId);
		columnType.add("String");
		columnValue.add(customerAppId);
		columnType.add("String");
		columnValue.add(event);
		columnType.add("String");
		columnValue.add(value);
		columnType.add("String");
		columnValue.add(formatter1.format(new Date()));
		columnType.add("String");
		columnValue.add(insertUser);
		columnType.add("String");
		columnValue.add(insertAddress);
		columnType.add("String");
		columnValue.add(device);
		
		
		return DbUtils.getInstance().updateExcute(sql.toString(), columnType, columnValue);
		
	}
	


	public int createMednetAppLogNotice(String customerId, String customerAppId, String healthType , String noticeTime , String action , String insertUser , String insertAddress , String device) {		
		StringBuffer sql = new StringBuffer(); 
		ArrayList<String> columnType = new ArrayList<String>();
		ArrayList<String> columnValue = new ArrayList<String>();
		SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

		sql.append("INSERT INTO Medical_HQ.dbo.mednetapp_log_notice  				 	\n");
		sql.append("(																    \n");
		sql.append("customer_id,customer_app_id,health_type,notice_time					\n");
		sql.append(",action,insert_time,insert_user,insert_address,device				\n");
		sql.append(")																	\n");
		sql.append("VALUES (?,?,?,?,?,?,?,?,?) 											\n");
		 
		columnType.add("String");
		columnValue.add(customerId);
		columnType.add("String");
		columnValue.add(customerAppId);
		columnType.add("String");
		columnValue.add(healthType);
		columnType.add("String");
		columnValue.add(noticeTime);
		columnType.add("String");
		columnValue.add(action);
		columnType.add("String");
		columnValue.add(formatter1.format(new Date()));
		columnType.add("String");
		columnValue.add(insertUser);
		columnType.add("String");
		columnValue.add(insertAddress);
		columnType.add("String");
		columnValue.add(device);
		
		
		return DbUtils.getInstance().updateExcute(sql.toString(), columnType, columnValue);
		
	}	

	
	public int registerMednetAppLogHealthOrder(String customerId, String customerAppId, String healthType , String healthOrder , String insertUser , String insertAddress , String device) {		
		StringBuffer sql = new StringBuffer(); 
		ArrayList<String> columnType = new ArrayList<String>();
		ArrayList<String> columnValue = new ArrayList<String>();
		SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	
		sql.append("UPDATE Medical_HQ.dbo.mednetapp_log_health_order										\n");
		sql.append("SET														 								\n");
		sql.append("health_order = ?, modify_time = ?, modify_user = ?, modify_address = ?					\n");
		sql.append("WHERE customer_id = ? and customer_app_id = ? and health_type = ? and device = ?		\n");

		
		columnType.add("String");
		columnValue.add(healthOrder);
		columnType.add("String");
		columnValue.add(formatter1.format(new Date()));
		columnType.add("String");
		columnValue.add(insertUser);
		columnType.add("String");
		columnValue.add(insertAddress);		
		
		columnType.add("String");
		columnValue.add(customerId);
		columnType.add("String");
		columnValue.add(customerAppId);
		columnType.add("String");
		columnValue.add(healthType);
		columnType.add("String");
		columnValue.add(device);
		
		int result = DbUtils.getInstance().updateExcute(sql.toString(), columnType, columnValue);
		if (result > 0)
		{
			return 0;
		}
		else 
		{
			sql.append("INSERT INTO Medical_HQ.dbo.mednetapp_log_health_order			 	\n");
			sql.append("(																    \n");
			sql.append("customer_id,customer_app_id,health_type,health_order,				\n");
			sql.append("insert_time,insert_user,insert_address,device						\n");
			sql.append(")																	\n");
			sql.append("VALUES (?,?,?,?,?,?,?,?)											\n");
			 
			columnType.add("String");
			columnValue.add(customerId);
			columnType.add("String");
			columnValue.add(customerAppId);
			columnType.add("String");
			columnValue.add(healthType);
			columnType.add("String");
			columnValue.add(healthOrder);
			columnType.add("String");
			columnValue.add(formatter1.format(new Date()));
			columnType.add("String");
			columnValue.add(insertUser);
			columnType.add("String");
			columnValue.add(insertAddress);
			columnType.add("String");
			columnValue.add(device);

			return DbUtils.getInstance().updateExcute(sql.toString(), columnType, columnValue);
		}
	}
	
	
	
	public int registerMednetAppLogHealthOrderV2(
			String customerId,
			String customerAppId ,
			String insertUser ,
			String insertAddress,
			String blood_sugar,
			String blood_pressure,
			String blood_oxygen,
			String drinking,
			String sleeping,
			String step_count,
			String body_mass_weight,
			String device) 
	
	{		
		StringBuffer sql = new StringBuffer(); 
		ArrayList<String> columnType = new ArrayList<String>();
		ArrayList<String> columnValue = new ArrayList<String>();
		SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		sql.append("INSERT INTO Medical_HQ.dbo.mednetapp_log_health_orderV2			 	\n");
		sql.append("(																    \n");
		sql.append("customer_id,customer_app_id,insert_time,insert_user,				\n");
		sql.append("insert_address,blood_sugar,blood_pressure,blood_oxygen,				\n");
		sql.append("drinking,sleeping,step_count,body_mass_weight,device				\n");
		sql.append(")																	\n");
		sql.append("VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)									\n");
		 
		columnType.add("String");
		columnValue.add(customerId);
		columnType.add("String");
		columnValue.add(customerAppId);
		columnType.add("String");
		columnValue.add(formatter1.format(new Date()));
		columnType.add("String");
		columnValue.add(insertUser);
		columnType.add("String");
		columnValue.add(insertAddress);
		
		columnType.add("String");
		columnValue.add(blood_sugar);
		columnType.add("String");
		columnValue.add(blood_pressure);
		columnType.add("String");
		columnValue.add(blood_oxygen);
		columnType.add("String");
		columnValue.add(drinking);
		columnType.add("String");
		columnValue.add(sleeping);
		columnType.add("String");
		columnValue.add(step_count);
		columnType.add("String");
		columnValue.add(body_mass_weight);
		
		
		columnType.add("String");
		columnValue.add(device);

		return DbUtils.getInstance().updateExcute(sql.toString(), columnType, columnValue);
		
	}
	
}
