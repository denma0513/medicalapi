package com.mednet.medical.api.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mednet.medical.api.base.baseService;
import com.mednet.medical.api.entity.Customer;
import com.mednet.medical.api.utils.CommonUtils;
import com.mednet.medical.api.utils.DbUtils;
import com.mednet.medical.api.utils.TimerUtils;
import com.mednet.medical.api.utils.config.ConstManager;

@Service
public class CustomerDao extends baseService {
	private @Autowired HttpServletRequest request;
	/**
	 * 新增健診客戶資料
	 * @author dennis
	 * */
	public int createCustomer(Customer customer){
		StringBuffer sql = new StringBuffer(); 
		ArrayList<String> columnType = new ArrayList<String>();
		ArrayList<String> columnValue = new ArrayList<String>();
		
		int result;
		if ((result = updateCustomer(customer)) > 0) {
			return result;
		}

		sql.append(" INSERT INTO "+ConstManager.getEmsSchema(request)+".DBO.EMS_CONTACT 			 	\n");
		sql.append(" (			 										\n");
		sql.append(" 	CONTACTID,										\n");
		sql.append(" 	DISPLAYNAME,								 		\n");
		sql.append(" 	GOVERNMENTID,			 				 		\n");
		sql.append(" 	GENDER,			 				 				\n");
		sql.append(" 	BIRTHDAY,			 			 				\n");
		sql.append(" 	PASSPORTID,			 						 	\n");
		sql.append(" 	RESIDENCEPERMITID,					 		 	\n");
		sql.append(" 	QUESTIONWEBURL,						 		 	\n");
		sql.append(" 	EMAIL,						 	 				\n");
		sql.append(" 	EMAIL2,						 	 				\n");
		sql.append(" 	PHONENUMBERCOUNTRY,				 			 	\n");
		sql.append(" 	PHONENUMBERAREA,				 			 		\n");
		sql.append(" 	PHONENUMBER,					 		 			\n");
		sql.append(" 	PHONEEXT,					 		 			\n");
		sql.append(" 	MOBILEPHONE,				 			 			\n");
		sql.append(" 	NEWBARCODEID,				 				 	\n");
		sql.append(" 	REPORTID,					 				 	\n");
		sql.append(" 	PGNAME,									 	 	\n");
		sql.append(" 	LEVEL,									 	 	\n");
		sql.append(" 	EXTNOTES,							 		 	\n");
		sql.append(" 	ADDRESS,							 		 		\n");
		sql.append(" 	LOCKERID,							 		 	\n");
		sql.append(" 	LOCKERTYPE,								 	 	\n");			 	
		sql.append(" 	HEALTHCHECKDATE				 			 		\n");	
		sql.append(" )			 				 				 		\n");
		sql.append("VALUES( ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,    \n");
		sql.append("  		?,?,?,?) 								    \n");

		columnType.add("String");
		columnValue.add(customer.getContactID());
		columnType.add("String");
		columnValue.add(customer.getDisplayName());
		columnType.add("String");
		columnValue.add(customer.getGovernmentID());
		columnType.add("String");
		columnValue.add(customer.getGender());
		columnType.add("String");
		columnValue.add(customer.getBirthday());
		columnType.add("String");
		columnValue.add(customer.getPassportID());
		columnType.add("String");
		columnValue.add(customer.getResidencePermitID());
		columnType.add("String");
		columnValue.add(customer.getQuestionWebUrl());
		columnType.add("String");
		columnValue.add(customer.getEmail());
		columnType.add("String");
		columnValue.add(customer.getEmail2());
		columnType.add("String");
		columnValue.add(customer.getPhoneNumberCountry());
		columnType.add("String");
		columnValue.add(customer.getPhoneNumberArea());
		columnType.add("String");
		columnValue.add(customer.getPhoneNumber());
		columnType.add("String");
		columnValue.add(customer.getPhoneExt());
		columnType.add("String");
		columnValue.add(customer.getMobilePhone());
		columnType.add("String");
		columnValue.add(customer.getNewBarCodeID());
		columnType.add("String");
		columnValue.add(customer.getReportID());
		columnType.add("String");
		columnValue.add(customer.getPgName());
		columnType.add("String");
		columnValue.add(customer.getLevel());
		columnType.add("String");
		columnValue.add(customer.getExtNotes());
		columnType.add("String");
		columnValue.add(customer.getAddress());
		columnType.add("String");
		columnValue.add(customer.getLockerID());
		columnType.add("String");
		columnValue.add(customer.getLockerType());
		columnType.add("String");
		columnValue.add(customer.getHealthCheckDate());
		
		result = DbUtils.getInstance().updateExcute(sql.toString(), columnType, columnValue);
		return result;
	}
	
	/**
	 * 更新健診客戶資料
	 * @author dennis
	 * */
	public int updateCustomer(Customer customer){
		StringBuffer sql = new StringBuffer(); 
		ArrayList<String> columnType = new ArrayList<String>();
		ArrayList<String> columnValue = new ArrayList<String>();
		
		sql.append("UPDATE "+ConstManager.getEmsSchema(request)+".DBO.EMS_CONTACT 	\n");
		sql.append("SET														 		\n");
		sql.append("DISPLAYNAME = ?, GENDER = ?, BIRTHDAY = ?, 						\n");
		sql.append("EMAIL = ?, EMAIL2 = ?, PHONENUMBERAREA = ?,						\n");
		sql.append("PHONENUMBER = ?, PHONEEXT = ?, PGNAME = ?, [LEVEL] = ?,			\n");
		sql.append("EXTNOTES = ?, ADDRESS= ?, LOCKERID = ?, LOCKERTYPE = ?,			\n");
		sql.append("PHONENUMBERCOUNTRY = ?											\n");
		sql.append("WHERE CONTACTID = ? AND HEALTHCHECKDATE = ?						\n");
		
		columnType.add("String");
		columnValue.add(customer.getDisplayName());
		columnType.add("String");
		columnValue.add(customer.getGender());
		columnType.add("String");
		columnValue.add(customer.getBirthday());
		columnType.add("String");
		columnValue.add(customer.getEmail());
		columnType.add("String");
		columnValue.add(customer.getEmail2());
		columnType.add("String");
		columnValue.add(customer.getPhoneNumberArea());
		columnType.add("String");
		columnValue.add(customer.getPhoneNumber());
		columnType.add("String");
		columnValue.add(customer.getPhoneExt());
		columnType.add("String");
		columnValue.add(customer.getPgName());
		columnType.add("String");
		columnValue.add(customer.getLevel());
		columnType.add("String");
		columnValue.add(customer.getExtNotes());
		columnType.add("String");
		columnValue.add(customer.getAddress());
		columnType.add("String");
		columnValue.add(customer.getLockerID());
		columnType.add("String");
		columnValue.add(customer.getLockerType());
		columnType.add("String");
		columnValue.add(customer.getPhoneNumberCountry());
		columnType.add("String");
		columnValue.add(customer.getContactID());
		columnType.add("String");
		columnValue.add(customer.getHealthCheckDate());
		
		int result = DbUtils.getInstance().updateExcute(sql.toString(), columnType, columnValue);
		return result;
	}
	
	/**
	 * 清除EMS_CHECKITEMLIST
	 * @param contactCode
	 * @return int
	 * @author dennis
	 * */
	public int clearCheckitemlist(String contactCode){
		StringBuffer sql = new StringBuffer(); 
		ArrayList<String> columnType = new ArrayList<String>();
		ArrayList<String> columnValue = new ArrayList<String>();

		sql.append("DELETE FROM "+ConstManager.getEmsSchema(request)+".DBO.EMS_CHECKITEMLIST WHERE NEWBARCODEID = ? \n");
		
		columnType.add("String");
		columnValue.add(contactCode);
		
		int result = DbUtils.getInstance().updateExcute(sql.toString(), columnType, columnValue);
		return result;
	}
	

	/**
	 * 新增EMS_CHECKITEMLIST
	 * @param contactCode
	 * @return int
	 * @author dennis
	 * */
	public int insCheckitemlist(String contactCode, String examitenCode, String examitemName, String roomCode){
		StringBuffer sql = new StringBuffer(); 
		ArrayList<String> columnType = new ArrayList<String>();
		ArrayList<String> columnValue = new ArrayList<String>();

		sql.append("INSERT INTO "+ConstManager.getEmsSchema(request)+".DBO.EMS_CHECKITEMLIST   \n");
		sql.append("	( 										\n");
		sql.append("		NEWBARCODEID, 						\n");
		sql.append("		FLOWNO, 								\n");
		sql.append("		DETAILID, 							\n");
		sql.append("		ROOMID, 								\n");
		sql.append("		FUNITEMID, 							\n");
		sql.append("		FUNITEMNAME, 						\n");
		sql.append("		STATUS, 								\n");
		sql.append("		GROUPID, 							\n");
		sql.append("		TOTALOPTIONNUM, 						\n");
		sql.append("		CANCHOOSENUM, 						\n");
		sql.append("		ISCHOOSE 							\n");
		sql.append("	)  										\n");
		sql.append("VALUES (?,'','',?,?,?,'2','',1,1,0) 		\n");
		
		columnType.add("String");
		columnValue.add(contactCode);
		columnType.add("String");
		columnValue.add(roomCode);
		columnType.add("String");
		columnValue.add(examitenCode);
		columnType.add("String");
		columnValue.add(examitemName);
		
		int result = DbUtils.getInstance().updateExcute(sql.toString(), columnType, columnValue);
		return result;
	}
	
	
	/**
	 * 新增EMS_CHECKITEMLIST
	 * @param contactCode
	 * @return int
	 * @author dennis
	 * */
	public int insCheckitemlist(JSONArray insertList){
		StringBuffer sql = new StringBuffer();
		sql.append("INSERT INTO "+ConstManager.getEmsSchema(request)+".DBO.EMS_CHECKITEMLIST   \n");
		sql.append("	( 											\n");
		sql.append("		NEWBARCODEID, 							\n");
		sql.append("		FLOWNO, 									\n");
		sql.append("		DETAILID, 								\n");
		sql.append("		ROOMID, 									\n");
		sql.append("		FUNITEMID, 								\n");
		sql.append("		FUNITEMNAME, 							\n");
		sql.append("		STATUS, 									\n");
		sql.append("		GROUPID, 								\n");
		sql.append("		TOTALOPTIONNUM, 							\n");
		sql.append("		CANCHOOSENUM, 							\n");
		sql.append("		CHECKINTIME, 							\n");
		sql.append("		CHECKOUTTIME, 							\n");
		sql.append("		ISCHOOSE 								\n");
		sql.append("	)  											\n");
		sql.append("VALUES (?,'','',?,?,?,?,'',1,1,?,?,0) 		\n");
		DbUtils dbUtils = DbUtils.getInstance().getMultipleUpdateIntent().batchUpdateSql(sql.toString());
		
		int result = 0;
		for (int i = 0; i < insertList.length(); i++) {
			JSONObject insert = insertList.optJSONObject(i);
			ArrayList<String> columnType = new ArrayList<String>();
			ArrayList<String> columnValue = new ArrayList<String>();

			
			String contactCode = insert.optString("contactID");
			String examitemCode = insert.optString("examitemCode");
			String examitemName = insert.optString("examitemName");
			String roomCode = insert.optString("roomCode");
			String status = insert.optString("status","2");
			String checkInTime = insert.optString("checkInTime");
			String checkOutTime = insert.optString("checkOutTime");
			
			
			columnType.add("String");
			columnValue.add(contactCode);
			columnType.add("String");
			columnValue.add(roomCode);
			columnType.add("String");
			columnValue.add(examitemCode);
			columnType.add("String");
			columnValue.add(examitemName);
			columnType.add("String");
			columnValue.add(status);
			columnType.add("String");
			columnValue.add(checkInTime);
			columnType.add("String");
			columnValue.add(checkOutTime);
			if (null == dbUtils.batchUpdateColumn( columnType, columnValue)) {
				
				//if (null == dbUtils.getMultipleUpdateIntent().batchUpdateSql(sql.toString(), columnType, columnValue)) {
				result = 0;
			}
		}
		if (null == dbUtils.batchCommit()) {
			result = 0;
		}
		return result;
	}
	
	/**
	 * 紀錄客戶進出Log
	 * @param contactCode
	 * @return int
	 * @author dennis
	 * @param status 
	 * @param examitemName 
	 * @param examitemID 
	 * @param roomID 
	 * */
	public int createCustomerLog(String action, String contactCode, String roomID, String examitemID, String examitemName, String status, String time) {
		StringBuffer sql = new StringBuffer(); 
		ArrayList<String> columnType = new ArrayList<String>();
		ArrayList<String> columnValue = new ArrayList<String>();
		
		if (updateCustomerLog(action, contactCode, roomID, examitemID, examitemName, status, time) > 0) {
			return 1;
		} 
		
		sql.append("INSERT INTO MEDICAL_HQ.DBO.CUSTOMERREPORT  \n");
		sql.append("( 											 \n");
		sql.append("		NEWBARCODEID, 							 \n");
		sql.append("		FLOWNO, 									 \n");
		sql.append("		DETAILID, 								 \n");
		sql.append("		ROOMID, 									 \n");
		sql.append("		FUNITEMID, 								 \n");
		sql.append("		FUNITEMNAME, 							 \n");
		sql.append("		STATUS, 									 \n");
		sql.append("		CHECKINTIME ,							 \n");
		sql.append("		GROUPID, 							  	 \n");
		sql.append("		TOTALOPTIONNUM, 							 \n");
		sql.append("		CANCHOOSENUM, 							 \n");
		sql.append("		ISCHOOSE 								 \n");
		sql.append("	)	    										 \n");	
		sql.append("VALUES (?,'','',?,?,?,?,?,'',1,1,0)	 		 \n");

		columnType.add("String");
		columnValue.add(contactCode);
		columnType.add("String");
		columnValue.add(roomID);
		columnType.add("String");
		columnValue.add(examitemID);
		columnType.add("String");
		columnValue.add(examitemName);
		columnType.add("String");
		columnValue.add(status);
		columnType.add("String");
		columnValue.add(time);
		
		int result = DbUtils.getInstance().updateExcute(sql.toString(), columnType, columnValue);
		return result;
	}
	
	/**
	 * 紀錄客戶進出Log
	 * @param contactCode
	 * @param status 
	 * @param examitemName 
	 * @param examitemID 
	 * @param roomID 
	 * @return int
	 * @author dennis
	 * */
	public int updateCustomerLog(String action, String contactCode, String roomID, String examitemID, String examitemName, String status, String time){
		StringBuffer sql = new StringBuffer(); 
		ArrayList<String> columnType = new ArrayList<String>();
		ArrayList<String> columnValue = new ArrayList<String>();

		columnType.add("String");
		columnValue.add(status);
		
		
		sql.append("UPDATE  MEDICAL_HQ.DBO.CUSTOMERREPORT  		 	\n");
		
		if ("in".equals(action)) {
			sql.append("	   SET  STATUS = ?, 							\n");
			sql.append("	   	CHECKINTIME = ?			 				\n");
			columnType.add("String");
			columnValue.add(time);
			
		}else if ("out".equals(action)){
			sql.append("	   SET  STATUS = ?, 							\n");
			sql.append("	   	CHECKOUTTIME = ?			 				\n");
			columnType.add("String");
			columnValue.add(time);
			
		} else {
			sql.append("	   SET  STATUS = ? 							\n");	
		}
		sql.append("	 WHERE	NEWBARCODEID = ? 						\n");
		sql.append("	   AND  ROOMID = ? 								\n");
		sql.append("	   AND  FUNITEMID = ? 							\n");

		columnType.add("String");
		columnValue.add(contactCode);
		columnType.add("String");
		columnValue.add(roomID);
		columnType.add("String");
		columnValue.add(examitemID);
		
		int result = DbUtils.getInstance().updateExcute(sql.toString(), columnType, columnValue);
		return result;
	}
	
	/**
	 * 紀錄客戶進出Log
	 * @param contactCode
	 * @author dennis
	 * */
	public List<HashMap<String, Object>>  queryCustomerLogByContactCode(String contactCode){
		StringBuffer sql = new StringBuffer(); 
		ArrayList<String> columnType = new ArrayList<String>();
		ArrayList<String> columnValue = new ArrayList<String>();

		sql.append("SELECT * FROM MEDICAL_HQ.DBO.CUSTOMERREPORT  		 	\n");
		sql.append("	 WHERE NEWBARCODEID = ?	 							 	\n");
		columnType.add("String");
		columnValue.add(contactCode);
		
		 List<HashMap<String, Object>> result = DbUtils.getInstance().excuteQueryL(sql.toString(), columnType, columnValue);
		return result;
	}
	
	/**
	 * 客戶資料
	 * @param contactCode
	 * @author dennis
	 * */
	public HashMap<String, Object>  queryCustomerByContactCode(String contactCode){
		StringBuffer sql = new StringBuffer(); 
		ArrayList<String> columnType = new ArrayList<String>();
		ArrayList<String> columnValue = new ArrayList<String>();

//		sql.append("SELECT *	 		 																\n");
//		sql.append("  FROM APP_MEDNET.DBO.CUSTOMER C,  APP_MEDNET.DBO.CUSTOMERREPORT R	 		 	\n");
//		sql.append(" WHERE C.CONTACTCODE = ?	 														\n");
//		sql.append("   AND R.CUSTOMERID = C.ID	 		 											\n");
		
		sql.append("SELECT * FROM "+ConstManager.getMednetSchema(request)+".DBO.CUSTOMER		 		 	\n");
		sql.append("	 WHERE CONTACTCODE = ?	 							 	\n");
	
		columnType.add("String");
		columnValue.add(contactCode);
		
		 List<HashMap<String, Object>> result = DbUtils.getInstance().excuteQueryL(sql.toString(), columnType, columnValue);
		 if (result != null && result.size() > 0) {
			 return result.get(0);
		 }
		return null;
	}
}
