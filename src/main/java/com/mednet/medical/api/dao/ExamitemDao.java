package com.mednet.medical.api.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mednet.medical.api.base.baseService;
import com.mednet.medical.api.utils.CommonUtils;
import com.mednet.medical.api.utils.DbUtils;
import com.mednet.medical.api.utils.config.ConstManager;

@Service
public class ExamitemDao extends baseService {
	private @Autowired HttpServletRequest request;

	public int examitemExistsCount(ArrayList<String> relativeIDArr) {
		StringBuffer sql = new StringBuffer();
		ArrayList<String> columnType = new ArrayList<String>();
		ArrayList<String> columnValue = new ArrayList<String>();

		sql.append("SELECT COUNT(ID) COUNT FROM " + ConstManager.getMednetSchema(request) + ".DBO.EXAMITEMS \n");
		if (relativeIDArr.size() <= 1) {
			sql.append("where id = ?\n");
			columnType.add("String");
			columnValue.add(relativeIDArr.get(0));
		} else {
			sql.append("where id in (\n");
			for (int i = 0; i < relativeIDArr.size(); i++) {
				if (i == 0) {
					sql.append("?");
				} else {
					sql.append(", ?");
				}
				columnType.add("String");
				columnValue.add(relativeIDArr.get(i));
			}
			sql.append(")\n");
		}

		List<HashMap<String, Object>> result = DbUtils.getInstance().excuteQueryL(sql.toString(), columnType,
				columnValue);
		if (result != null && result.size() > 0) {
			HashMap<String, Object> obj = result.get(0);
			int count = Integer.parseInt(CommonUtils.chkValue(obj.get("COUNT"), "0"));
			return count;
		}

		return 0;
	}

	/**
	 * 撈出健診健診項目的數目
	 */
	public int examitemExistsCountByCode(ArrayList<String> relativeIDArr) {
		StringBuffer sql = new StringBuffer();
		ArrayList<String> columnType = new ArrayList<String>();
		ArrayList<String> columnValue = new ArrayList<String>();

		sql.append("SELECT COUNT(ID) COUNT FROM " + ConstManager.getMednetSchema(request) + ".DBO.EXAMITEMS \n");
		if (relativeIDArr.size() <= 1) {
			sql.append("WHERE CODE = ?\n");
			columnType.add("String");
			columnValue.add(relativeIDArr.get(0));
		} else {
			sql.append("WHERE CODE IN (\n");
			for (int i = 0; i < relativeIDArr.size(); i++) {
				if (i == 0) {
					sql.append("?");
				} else {
					sql.append(", ?");
				}
				columnType.add("String");
				columnValue.add(relativeIDArr.get(i));
			}
			sql.append(")\n");
		}

		List<HashMap<String, Object>> result = DbUtils.getInstance().excuteQueryL(sql.toString(), columnType,
				columnValue);
		if (result != null && result.size() > 0) {
			HashMap<String, Object> obj = result.get(0);
			int count = Integer.parseInt(CommonUtils.chkValue(obj.get("COUNT"), "0"));
			return count;
		}

		return 0;
	}

	/**
	 * 新增健診項目
	 */
	public int createExamitem(String guid, String organizationID, String code, JSONObject examData) {
		String name = examData.optString("name");
		String nameEn = examData.optString("nameEn");
		String parentid = "".equals(examData.optString("parentid")) ? null : examData.optString("parentid");
		String description = examData.optString("description");
		String descriptionen = examData.optString("descriptionen");
		String spendseconds = examData.optString("spendseconds","0");
		String priorityrule = examData.optString("priorityrule","0");
		String timeruleseconds = examData.optString("timeruleseconds","0");
		String timerulebefore = examData.optString("timerulebefore","0");
		String doubleruleseconds = examData.optString("doubleruleseconds","0");
		JSONArray rulesDataList = examData.optJSONArray("rulesData");

		StringBuffer sql = new StringBuffer();
		ArrayList<String> columnType = new ArrayList<String>();
		ArrayList<String> columnValue = new ArrayList<String>();

		columnValue.add(name);
		columnType.add("String");
		columnValue.add(nameEn);
		columnType.add("String");
		columnValue.add(spendseconds);
		columnType.add("int");
		columnValue.add(timeruleseconds);
		columnType.add("int");
		columnValue.add(timerulebefore);
		columnType.add("boolean");
		columnValue.add(doubleruleseconds);
		columnType.add("int");
		columnValue.add(priorityrule);
		columnType.add("int");
		columnValue.add(parentid);
		columnType.add("String");
		columnValue.add(description);
		columnType.add("String");
		columnValue.add(descriptionen);
		columnType.add("String");
		columnValue.add(code);
		columnType.add("String");

		sql.append("UPDATE " + ConstManager.getMednetSchema(request) + ".DBO.EXAMITEMS   					\n");
		sql.append("SET NAME = ?, NAMEEN = ?, SPENDSECONDS = ?, 												\n");
		sql.append("TIMERULESECONDS = ?, TIMERULEBEFORE = ?, DOUBLERULESECONDS = ?, PRIORITYRULE = ?, 		\n");
		sql.append("PARENTID = ?, DESCRIPTION = ?, DESCRIPTIONEN = ?											\n");
		sql.append("WHERE code = ?																			\n");
		int result = DbUtils.getInstance().updateExcute(sql.toString(), columnType, columnValue);

		if (result > 0) {
			return result;
		}

		sql.delete(0, sql.length());
		columnType.clear();
		columnValue.clear();

		columnType.add("String");
		columnValue.add(guid);
		columnType.add("String");
		columnValue.add(organizationID);
		columnType.add("String");
		columnValue.add(name);
		columnType.add("String");
		columnValue.add(nameEn);
		columnType.add("String");
		columnValue.add(spendseconds);
		columnType.add("String");
		columnValue.add(timeruleseconds);
		columnType.add("String");
		columnValue.add(timerulebefore);
		columnType.add("String");
		columnValue.add(doubleruleseconds);
		columnType.add("String");
		columnValue.add(priorityrule);
		columnType.add("String");
		columnValue.add(parentid);
		columnType.add("String");
		columnValue.add(description);
		columnType.add("String");
		columnValue.add(descriptionen);
		columnType.add("String");
		columnValue.add(code);

		sql.append("INSERT INTO " + ConstManager.getMednetSchema(request) + ".DBO.EXAMITEMS \n");
		sql.append("( 												\n");
		sql.append("ID, 							 					\n");
		sql.append("ORGANIZATIONID, 									\n");
		sql.append("NAME, 											\n");
		sql.append("NAMEEN, 											\n");
		sql.append("SPENDSECONDS, 									\n");
		sql.append("TIMERULESECONDS, 								\n");
		sql.append("TIMERULEBEFORE, 									\n");
		sql.append("DOUBLERULESECONDS, 								\n");
		sql.append("PRIORITYRULE, 									\n");
		sql.append("PARENTID, 										\n");
		sql.append("DESCRIPTION, 									\n");
		sql.append("DESCRIPTIONEN, 									\n");
		sql.append("CODE 											\n");
		sql.append(") 												\n");
		sql.append("VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)  \n");

		result = DbUtils.getInstance().updateExcute(sql.toString(), columnType, columnValue);

		if (result <= 0) {
			// 新增失敗
			return 0;
		}

		int ruleResult = 0;
		// 先新增rule
		for (int i = 0; i < rulesDataList.length(); i++) {
			JSONObject rulesData = rulesDataList.optJSONObject(i);
			String relativeID = rulesData.optString("relativeID");
			String type = rulesData.optString("type");
			String afterWait = rulesData.optString("afterWait","0");
			ruleResult += createExamitem2Rule(guid, relativeID, type, afterWait);
		}

		// 有rule新增失敗return
		if (ruleResult != rulesDataList.length()) {
			return -1;
		}

		return result;
	}

	/**
	 * 新增健診項目與規則的關聯資料
	 */
	public int createExamitem2Rule(String guid, String relativeID, String type, String afterWait) {
		StringBuffer sql = new StringBuffer();
		ArrayList<String> columnType = new ArrayList<String>();
		ArrayList<String> columnValue = new ArrayList<String>();
		columnType.add("String");
		columnValue.add(type);
		columnType.add("String");
		columnValue.add(afterWait);
		columnType.add("String");
		columnValue.add(guid);
		columnType.add("String");
		columnValue.add(relativeID);

		sql.append("UPDATE " + ConstManager.getMednetSchema(request) + ".DBO.EXAMITEMS2RULE   	\n");
		sql.append("SET TYPE = ?, AFTERWAIT = ? 													\n");
		sql.append("WHERE  EXAMITEMID= ? AND RELATIVEID = ?										\n");
		int result = DbUtils.getInstance().updateExcute(sql.toString(), columnType, columnValue);
		if (result > 0) {
			return result;
		}
		sql.delete(0, sql.length());
		sql.append("INSERT INTO " + ConstManager.getMednetSchema(request) + ".DBO.EXAMITEMS2RULE   		\n");
		sql.append("(TYPE, AFTERWAIT, EXAMITEMID, RELATIVEID) 											\n");
		sql.append("VALUES (?, ?, ?, ?) 																	\n");
		result = DbUtils.getInstance().updateExcute(sql.toString(), columnType, columnValue);
		return result;
	}

	/**
	 * 撈出健診項目id by code
	 */
	public String checkExamitem(String code) {
		StringBuffer sql = new StringBuffer();
		ArrayList<String> columnType = new ArrayList<String>();
		ArrayList<String> columnValue = new ArrayList<String>();

		sql.append("SELECT ID FROM " + ConstManager.getMednetSchema(request) + ".DBO.EXAMITEMS WHERE CODE = ? \n");
		columnType.add("String");
		columnValue.add(code);

		List<HashMap<String, Object>> result = DbUtils.getInstance().excuteQueryL(sql.toString(), columnType,
				columnValue);
		if (result != null && result.size() > 0) {
			HashMap<String, Object> obj = result.get(0);
			return CommonUtils.chkValue(obj.get("ID"), "");
		}
		return "";
	}

	/**
	 * 用examitemid 查詢診間
	 * 
	 * @param examitenID
	 * @author dennis
	 */
	public HashMap<String, Object> queryExamitemRoomByID(String examitenID, String parentID) {
		StringBuffer sql = new StringBuffer();
		ArrayList<String> columnType = new ArrayList<String>();
		ArrayList<String> columnValue = new ArrayList<String>();

		sql.append("SELECT R.CODE, R.ID, R.NAME  												\n");
		sql.append("  FROM " + ConstManager.getMednetSchema(request) + ".DBO.ROOM2EXAMITEM RE, 	\n");
		sql.append("  " + ConstManager.getMednetSchema(request) + ".DBO.ROOM R 					\n");
		sql.append(" WHERE (EXAMITEMID = ? OR EXAMITEMID = ?) 									\n");
		sql.append("   AND RE.ROOMID = R.ID  													\n");

		columnType.add("String");
		columnValue.add(examitenID);
		columnType.add("String");
		columnValue.add(parentID);

		List<HashMap<String, Object>> result = DbUtils.getInstance().excuteQueryL(sql.toString(), columnType,
				columnValue);
		if (result != null && result.size() > 0) {
			return result.get(0);
		}
		return null;
	}

	/**
	 * 用examitemid 查詢診間
	 * 
	 * @param examitenID
	 * @author dennis
	 */
	public HashMap<String, Object> queryExamitemRoomByID(String examitenCode) {
		StringBuffer sql = new StringBuffer();
		ArrayList<String> columnType = new ArrayList<String>();
		ArrayList<String> columnValue = new ArrayList<String>();

		sql.append("SELECT E.CODE, E.ID, E.NAME 												\n");
		sql.append("  FROM " + ConstManager.getMednetSchema(request) + ".DBO.examitems E 	\n");
		sql.append("  WHERE E.CODE = ?														\n");

		columnType.add("String");
		columnValue.add(examitenCode);

		List<HashMap<String, Object>> result = DbUtils.getInstance().excuteQueryL(sql.toString(), columnType,
				columnValue);
		if (result != null && result.size() > 0) {
			return result.get(0);
		}
		return null;
	}
}
