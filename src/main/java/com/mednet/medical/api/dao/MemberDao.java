package com.mednet.medical.api.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mednet.medical.api.base.baseService;
import com.mednet.medical.api.entity.Customer;
import com.mednet.medical.api.utils.CommonUtils;
import com.mednet.medical.api.utils.DbUtils;
import com.mednet.medical.api.utils.TimerUtils;
import com.mednet.medical.api.utils.config.ConstManager;

@Service
public class MemberDao extends baseService {
	private @Autowired HttpServletRequest request;
	/**
	 * 新增健診客戶資料
	 * @author dennis
	 * */
	public HashMap<String, Object>  getAtuhType(String userID){
		StringBuffer sql = new StringBuffer(); 
		ArrayList<String> columnType = new ArrayList<String>();
		ArrayList<String> columnValue = new ArrayList<String>();
		
		List<HashMap<String, Object>>  list;
		HashMap<String, Object> result = null;
		
//		sql.append(" Select Expert.UserId,Expert.isExpert,Article.Release,Article.NotRelease  						 \n");
		
		
		sql.append("Declare @User_id int\n");
		sql.append("set @User_id = ?\n");

		sql.append("Select Expert.CustomerID, Expert.UserId as ExpertId,Expert.isExpert,ISNULL(Article.Release,0) as Release,ISNULL(Article.NotRelease,0) as NotRelease FROM (\n");
		sql.append("Select @User_id as CustomerID, UserID as UserId, count(case when (UserID is not null) then 1 else 0 end )as isExpert FROM dbMedNet_ExpertQA.[dbo].[tbl_ExpertProfile] \n");
		sql.append("				Where UserID = (Select ID FROM dbMedNet_ExpertQA.[dbo].[tbl_User] Where MedNetID = @User_id ) and ApproveStatus =1 Group BY UserID ) Expert Left join\n");
		sql.append("(Select UserId,\n");
		sql.append("count(case when (IsRelease =1) then 1 else null end) as Release,\n");
		sql.append("count(case when (IsRelease =0) then 1 else null end) as NotRelease\n");
		sql.append("FROM dbMedNet_ExpertQA.[dbo].[tbl_ExpertArticle] Where IsEnable=1 and UserId = (Select ID FROM dbMedNet_ExpertQA.[dbo].[tbl_User] Where MedNetID = @User_id)\n"); 
		sql.append("GROUP BY UserId) Article  on Expert.UserId = Article.UserId\n");


		
//		sql.append(" Select CONVERT(varchar(10), Expert.UserId) UserId ,					 							 \n");
//		sql.append(" 		CONVERT(varchar(10), Expert.isExpert) isExpert ,					 						 \n");
//		sql.append(" 		CONVERT(varchar(10), Article.Release) Release ,					 						 \n");
//		sql.append(" 		CONVERT(varchar(10), Article.NotRelease) NotRelease					 					 \n");
//		sql.append(" 	FROM (                       																 \n");
//		sql.append(" 			Select UserId, count(case when (UserID is not null) then 1 else 0 end )as isExpert   \n");
//		sql.append(" 			FROM [DBMEDNET_EXPERTQA].[dbo].[tbl_ExpertProfile]								     \n");
//		sql.append(" 			Where UserID = ?   																	 \n");
//		sql.append(" 			group by UserId 																		 \n");
//		sql.append(" 		) Expert Left join 																		 \n");
//		sql.append(" 	(Select UserId, 																				 \n");
//		sql.append(" 			count(case when (IsRelease =1) then 1 else null end) as Release,						 \n");
//		sql.append(" 			count(case when (IsRelease =0) then 1 else null end) as NotRelease				     \n");
//		sql.append("    	   FROM [DBMEDNET_EXPERTQA].[dbo].[tbl_ExpertArticle] 										 \n");
//		sql.append("      Where IsEnable=1  																			 \n");
//		sql.append("      group by UserId) Article  on Expert.UserId = Article.UserId								 \n");

		columnType.add("int");
		columnValue.add(userID);
		list = DbUtils.getInstance().excuteExpertQAQueryL(sql.toString(), columnType, columnValue);
		
		if (list != null && list.size() >= 1) {
			result =new HashMap<String, Object>();
			HashMap<String, Object> obj = list.get(0);
	        result.put("isExpert", obj.get("isExpert")+"");
	        result.put("drID", obj.get("ExpertId")+"");
	        result.put("NotRelease", obj.get("NotRelease")+"");
	        result.put("CustomerID", obj.get("CustomerID")+"");
	        result.put("Release", obj.get("Release")+"");
			//result = list.get(0);
		}

		return result;
	}

}
