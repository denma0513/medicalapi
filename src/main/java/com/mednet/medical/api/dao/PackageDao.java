package com.mednet.medical.api.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mednet.medical.api.base.baseService;
import com.mednet.medical.api.utils.CommonUtils;
import com.mednet.medical.api.utils.DbUtils;
import com.mednet.medical.api.utils.config.ConstManager;

@Service
public class PackageDao extends baseService {
	private @Autowired HttpServletRequest request;

	/**
	 * 新增套組
	 * @param guid
	 * @param organizationID
	 * @param name
	 * @param examitemIDArr
	 * @return result
	 * @author dennis
	 * */
	public int createPackage(String guid, String organizationID, String name, ArrayList<String> examitemIDArr) {
		StringBuffer sql = new StringBuffer();
		ArrayList<String> columnType = new ArrayList<String>();
		ArrayList<String> columnValue = new ArrayList<String>();

		sql.append("INSERT INTO " + ConstManager.getMednetSchema(request) + ".DBO.PACKAGE   	\n");
		sql.append("(ID, ORGANIZATIONID, NAME ) 												\n");
		sql.append("VALUES (?, ?, ?) 														\n");

		columnType.add("String");
		columnValue.add(guid);
		columnType.add("String");
		columnValue.add(organizationID);
		columnType.add("String");
		columnValue.add(name);

		DbUtils dbUtils = DbUtils.getInstance();

		if (null == dbUtils.getMultipleUpdateIntent().updateMultipleSql(sql.toString(), columnType, columnValue)) {
			return 0;
		}

		int result = 1;
		for (int i = 0; i < examitemIDArr.size(); i++) {
			sql.delete(0, sql.length());
			columnType.clear();
			columnValue.clear();

			sql.append("INSERT INTO " + ConstManager.getMednetSchema(request) + ".DBO.PACKAGE2EXAMITEM   	\n");
			sql.append("(PACKAGEID, EXAMITEMID) 																\n");
			sql.append("VALUES (?, ?)	 																	\n");

			columnType.add("String");
			columnValue.add(guid);
			columnType.add("String");
			columnValue.add(examitemIDArr.get(i));

			if (null == dbUtils.getMultipleUpdateIntent().updateMultipleSql(sql.toString(), columnType, columnValue)) {
				result = 0;
			}
		}

		if (null == dbUtils.commit()) {
			result = 0;
		}

		return result;
	}

	/**
	 * 確認是否有該名稱之套組
	 * @param packageName
	 * @return result
	 * @author dennis
	 * */
	public int packageExistsCount(String packageName) {
		StringBuffer sql = new StringBuffer();
		ArrayList<String> columnType = new ArrayList<String>();
		ArrayList<String> columnValue = new ArrayList<String>();

		sql.append("SELECT COUNT(1) FROM " + ConstManager.getMednetSchema(request) + ".DBO.PACKAGE	\n");
		sql.append("WHERE NAME = ? 																	\n");

		columnType.add("String");
		columnValue.add(packageName);
		int result = DbUtils.getInstance().excuteQueryCount(sql.toString(), columnType,
				columnValue);
//		if (result != null && result.size() > 0) {
//			HashMap<String, Object> obj = result.get(0);
//			System.out.println(result);
//			System.out.println(obj);
//			int count = Integer.parseInt(CommonUtils.chkValue(obj.get("COUNT"), "0"));
//			System.out.println(count);
//			return count;
//		}
		return result;
	}
	
	/**
	 * 查詢套組中的所有健診項目
	 * @param packageName
	 * @return result
	 * @author dennis
	 * */
	public List<HashMap<String, Object>> queryPackageExamitems(String packageName) {
		StringBuffer sql = new StringBuffer();
		ArrayList<String> columnType = new ArrayList<String>();
		ArrayList<String> columnValue = new ArrayList<String>();

		sql.append("SELECT E.ID ,E.PARENTID, E.NAME, E.CODE														\n");
		sql.append("FROM " + ConstManager.getMednetSchema(request) + ".DBO.PACKAGE P, 							\n");
		sql.append(" " + ConstManager.getMednetSchema(request) + ".DBO.PACKAGE2EXAMITEM PE, 						\n");
		sql.append(" " + ConstManager.getMednetSchema(request) + ".DBO.EXAMITEMS E,								\n");
		sql.append("WHERE P.NAME = ?																				\n");
		sql.append("AND P.ID = PE.PACKAGEID 																		\n");
		sql.append("AND PE.EXAMITEMID = E.ID																		\n");
		columnType.add("String");
		columnValue.add(packageName);
		List<HashMap<String, Object>> result = DbUtils.getInstance().excuteQueryL(sql.toString(), columnType, columnValue);
		return result;
	}
	
	/**
	 * 查詢套組中的所有健診項目及對應診間
	 * @param packageName
	 * @return result
	 * @author dennis
	 * */
	public List<HashMap<String, Object>> queryPackageExamitemsRoom(String packageName) {
		StringBuffer sql = new StringBuffer();
		ArrayList<String> columnType = new ArrayList<String>();
		ArrayList<String> columnValue = new ArrayList<String>();
		
		sql.append(" SELECT A.CODE ECODE, A.NAME ENAME, B.CODE RCODE, B.NAME RNAME FROM ( 										\n");
		sql.append(" 		SELECT E.ID ,E.PARENTID, E.NAME, E.CODE																\n");
		sql.append(" 		  FROM " + ConstManager.getMednetSchema(request) + ".DBO.EXAMITEMS E, 								\n");
		sql.append(" 		  	" + ConstManager.getMednetSchema(request) + ".DBO.PACKAGE P, 									\n");
		sql.append(" 		  	" + ConstManager.getMednetSchema(request) + ".DBO.PACKAGE2EXAMITEM PE							\n");
		sql.append(" 		 WHERE PE.EXAMITEMID = E.ID 																			\n");
		sql.append(" 		   AND P.ID = PE.PACKAGEID 																			\n");
		sql.append(" 		   AND P.NAME = ?																					\n");
		sql.append(" 	) A LEFT JOIN (																							\n");
		sql.append(" 		SELECT R.CODE, R.ID, R.NAME, RE.EXAMITEMID															\n");
		sql.append(" 			FROM " + ConstManager.getMednetSchema(request) + ".DBO.ROOM2EXAMITEM RE, 						\n");
		sql.append(" 			" + ConstManager.getMednetSchema(request) + ".DBO.ROOM R											\n");
		sql.append(" 			WHERE RE.ROOMID = R.ID																			\n");
		sql.append(" 	) B ON  (B.EXAMITEMID = A.ID  OR B.EXAMITEMID = A.PARENTID)												\n");

		columnType.add("String");
		columnValue.add(packageName);
		List<HashMap<String, Object>> result = DbUtils.getInstance().excuteQueryL(sql.toString(), columnType, columnValue);
		return result;
	}
	
	
	
	/**
	 * 查詢健診項目及對應診間
	 * @param packageName
	 * @return result
	 * @author dennis
	 * */
	public List<HashMap<String, Object>> queryExamitemsRoom(JSONArray codeArr) {
		StringBuffer sql = new StringBuffer();
		ArrayList<String> columnType = new ArrayList<String>();
		ArrayList<String> columnValue = new ArrayList<String>();

		sql.append(" SELECT A.CODE RCODE, A.NAME RNAME, B.CODE ECODE, B.NAME ENAME FROM (			\n");
		sql.append(" 	SELECT RE.EXAMITEMID, RE.ROOMID, R.CODE, R.NAME 								\n");
		sql.append(" 	  FROM " + ConstManager.getMednetSchema(request) + ".DBO.ROOM2EXAMITEM RE, 	\n");
		sql.append(" 	  	" + ConstManager.getMednetSchema(request) + ".DBO.ROOM R					\n");
		sql.append("     WHERE RE.ROOMID = R.ID														\n");
		sql.append(" 	) A 																			\n");
		sql.append("		RIGHT JOIN " + ConstManager.getMednetSchema(request) + ".DBO.EXAMITEMS B 	\n");
		sql.append("				ON (A.EXAMITEMID = B.ID OR A.EXAMITEMID = B.PARENTID)				\n");
		if (codeArr.length() <= 1) {
			sql.append("WHERE B.CODE = ?\n");
			columnType.add("String");
			columnValue.add(codeArr.optString(0));
		} else {
			sql.append("WHERE B.CODE IN (\n");
			for (int i = 0; i < codeArr.length(); i++) {
				if (i == 0) {
					sql.append("?");
				} else {
					sql.append(", ?");
				}
				columnType.add("String");
				columnValue.add(codeArr.optString(i));
			}
			sql.append(")\n");
		}
		List<HashMap<String, Object>> result = DbUtils.getInstance().excuteQueryL(sql.toString(), columnType, columnValue);
		return result;
	}
	
	
	

}
