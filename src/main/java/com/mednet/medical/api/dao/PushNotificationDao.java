package com.mednet.medical.api.dao;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mednet.medical.api.base.baseService;
import com.mednet.medical.api.utils.CommonUtils;
import com.mednet.medical.api.utils.DbUtils;
import com.mednet.medical.api.utils.config.ConstManager;

@Service
public class PushNotificationDao extends baseService {

	private @Autowired HttpServletRequest request;
	
	/**
	 * 新增推播token
	 * @param pushToken
	 * @param organization
	 * @param name
	 * @param age
	 * @param gender
	 * @param phoneNumber
	 * @param englishVersion
	 * @return result
	 * @author Ray
	 * */
	public int createPushToken(String pushToken, String organization, String name, String age ,String gender,String phoneNumber,String englishVersion) {
		StringBuffer sql = new StringBuffer(); 
		ArrayList<String> columnType = new ArrayList<String>();
		ArrayList<String> columnValue = new ArrayList<String>();
		
		sql.append("UPDATE Medical_HQ.DBO.TOKEN_DATA 											\n");
		sql.append("SET														 					\n");
		sql.append("push_token = ?, gender = ?, name = ?, 										\n");
		sql.append("organization = ?, english_version = ?, phone_number = ?,age = ?,date = ?	\n");
		sql.append("WHERE push_token = ?  														\n");
		SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd");
		columnType.add("String");
		columnValue.add(pushToken);
		columnType.add("String");
		columnValue.add(gender);
		columnType.add("String");
		columnValue.add(name);
		columnType.add("String");
		columnValue.add(organization);
		columnType.add("String");
		columnValue.add(englishVersion);
		columnType.add("String");
		columnValue.add(phoneNumber);
		columnType.add("String");
		columnValue.add(age);
		columnType.add("String");
		columnValue.add(formatter1.format(new Date()));
		columnType.add("String");
		columnValue.add(pushToken);
		
		int result = DbUtils.getInstance().updateExcute(sql.toString(), columnType, columnValue);
		if (result > 0) {
			return result;
		}
		
		sql.append("INSERT INTO Medical_HQ.DBO.TOKEN_DATA 	\n");
		sql.append("(														    \n");
		sql.append("push_token , gender , name , organization,					\n");
		sql.append(" english_version , phone_number , age,	date )					\n");
		sql.append("VALUES (?,?,?,?,?,?,?,?) 			\n");
		 
		columnType.add("String");
		columnValue.add(pushToken);
		columnType.add("String");
		columnValue.add(gender);
		columnType.add("String");
		columnValue.add(name);
		columnType.add("String");
		columnValue.add(organization);
		columnType.add("String");
		columnValue.add(englishVersion);
		columnType.add("String");
		columnValue.add(phoneNumber);
		columnType.add("String");
		columnValue.add(age);
		columnType.add("String");
		columnValue.add(formatter1.format(new Date()));
		
		return DbUtils.getInstance().updateExcute(sql.toString(), columnType, columnValue);
		
	}
	
	public List<HashMap<String, Object>> queryPushTokenWithCondition(String maxAge,String minAge, String organization, String gender , boolean beforeDate)
	{
		StringBuffer sql = new StringBuffer();
		ArrayList<String> columnType = new ArrayList<String>();
		ArrayList<String> columnValue = new ArrayList<String>();
		
		sql.append("SELECT * FROM Medical_HQ.dbo.TOKEN_DATA "
				+ "WHERE ");
		
		if (!CommonUtils.isBlank(gender)) {
			sql.append("and gender = ? ");
			columnType.add("String");
			columnValue.add(gender);
		}
		
		if (!CommonUtils.isBlank(maxAge) && !maxAge.startsWith("0")) {
			sql.append("and age <= ? ");
			columnType.add("String");
			columnValue.add(maxAge);
		}
		
		if (!CommonUtils.isBlank(minAge) && !minAge.startsWith("0")) {
			sql.append("and age >= ? ");
			columnType.add("String");
			columnValue.add(minAge);
		}
		
		if (!CommonUtils.isBlank(organization)) {
			sql.append("and organization = ? ");
			columnType.add("String");
			columnValue.add(organization);
		}
		
		if (!beforeDate) {
			sql.append("and date >= ? ");
			SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd");
			columnType.add("String");
			columnValue.add(formatter1.format(new Date()));
		}
		
		String sqlString = sql.toString();
		
		if (sqlString.endsWith("WHERE ")) {
			sqlString = sqlString.replaceAll("WHERE ", "");
		}
		else {
			sqlString = sqlString.replaceAll("WHERE and", "WHERE ");
		}
		
		List<HashMap<String, Object>> result = DbUtils.getInstance().excuteQueryL(sqlString, columnType, columnValue);
		
		return result;
		
	}
	
	
	public List<HashMap<String, Object>> queryNotificationNameList() {
		StringBuffer sql = new StringBuffer();
		
		sql.append(" SELECT L.MESSAGEID, M.TITLE,M.CONTENT,M.TARGETURL,L.CUSTOMERID, L.PUSHTOKEN  \n ");
		sql.append("   FROM   \n ");
		sql.append(" 		MEDICAL_HQ.DBO.MEDNETAPP_NOTIFICATION_LIST  L,  \n ");
		sql.append(" 		MEDICAL_HQ.DBO.MEDNETAPP_NOTIFICATION_MESSAGE M  \n ");
		sql.append("  WHERE L.MESSAGEID = M.MESSAGEID  \n ");
		
		String sqlString = sql.toString();
		List<HashMap<String, Object>> result = DbUtils.getInstance().excuteQueryL(sqlString);
		return result;
		
	}
	
	/**
	 *
	 * @param pushToken
	 * @param device
	 * @param customerid

	 * */
	//public int createPushToken(String pushToken, String organization, String name, String age ,String gender,String phoneNumber,String englishVersion) {
	public int deleteScheduleByMsgidCusid(String messageId, String customerId ) {		
		StringBuffer sql = new StringBuffer(); 
		ArrayList<String> columnType = new ArrayList<String>();
		ArrayList<String> columnValue = new ArrayList<String>();
		SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		
		sql.append("DELETE FROM MEDICAL_HQ.DBO.MEDNETAPP_NOTIFICATION_LIST							    \n");
		sql.append("WHERE MESSAGEID = ? AND CUSTOMERID = ?												\n");
		columnType.add("String");
		columnValue.add(messageId);
		columnType.add("String");
		columnValue.add(customerId);
		//System.out.println("sql = "+sql);
		int result = DbUtils.getInstance().updateExcute(sql.toString(), columnType, columnValue);
		return result;
	}
}
