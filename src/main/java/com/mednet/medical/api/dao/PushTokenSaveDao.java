package com.mednet.medical.api.dao;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mednet.medical.api.base.baseService;
import com.mednet.medical.api.utils.CommonUtils;
import com.mednet.medical.api.utils.DbUtils;
import com.mednet.medical.api.utils.config.ConstManager;

@Service
public class PushTokenSaveDao extends baseService {

	private @Autowired HttpServletRequest request;
	
	/**
	 *
	 * @param pushToken
	 * @param device
	 * @param customerid

	 * */
	//public int createPushToken(String pushToken, String organization, String name, String age ,String gender,String phoneNumber,String englishVersion) {
	public int createPushToken(String pushToken, String device, String customerId ) {		
		
		
		StringBuffer sql = new StringBuffer(); 
		ArrayList<String> columnType = new ArrayList<String>();
		ArrayList<String> columnValue = new ArrayList<String>();
		SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		
		sql.append("UPDATE Medical_HQ.dbo.pushtoken 													\n");
		sql.append("SET																 					\n");
		sql.append("pushtoken = ?, device = ?, customerid = ?, ModifyDate = ?							\n");
		sql.append("WHERE pushtoken = ?  																\n");
		columnType.add("String");
		columnValue.add(pushToken);
		columnType.add("String");
		columnValue.add(device);
		columnType.add("String");
		columnValue.add(customerId);
		columnType.add("String");
		columnValue.add(formatter1.format(new Date()));
		columnType.add("String");
		columnValue.add(pushToken);
		
		int result = DbUtils.getInstance().updateExcute(sql.toString(), columnType, columnValue);
		if (result > 0) {
			return result;
		}
		
		
		sql.append("INSERT INTO Medical_HQ.dbo.pushtoken  	\n");
		sql.append("(																				    \n");
		sql.append("pushtoken , device , customerid , InserDate, ModifyDate , Enable )					\n");
		sql.append("VALUES (?,?,?,?,?,?) 			\n");
		 
		columnType.add("String");
		columnValue.add(pushToken);
		columnType.add("String");
		columnValue.add(device);
		columnType.add("String");
		columnValue.add(customerId);
		columnType.add("String");
		columnValue.add(formatter1.format(new Date()));
		columnType.add("String");
		columnValue.add(formatter1.format(new Date()));
		columnType.add("String");
		columnValue.add("1");
		
		return DbUtils.getInstance().updateExcute(sql.toString(), columnType, columnValue);
		
	}
	
	public List<HashMap<String, Object>> queryPushTokenWithCustomerId(String customerId)
	{
		StringBuffer sql = new StringBuffer();
		ArrayList<String> columnType = new ArrayList<String>();
		ArrayList<String> columnValue = new ArrayList<String>();
		
		sql.append("SELECT * FROM Medical_HQ.dbo.pushtoken WHERE customerid = ?");
		columnType.add("String");
		columnValue.add(customerId);				

				
		String sqlString = sql.toString();
		List<HashMap<String, Object>> result = DbUtils.getInstance().excuteQueryL(sqlString, columnType, columnValue);
		return result;
		
	}

	
	public int rejectAddFriendWithCustomerIdAndAppid(String customerId, String appId) {		
		StringBuffer sql = new StringBuffer(); 
		ArrayList<String> columnType = new ArrayList<String>();
		ArrayList<String> columnValue = new ArrayList<String>();
		

		sql.append("use med_db										\n");
		sql.append("delete FROM Med_community_friend WHERE 			\n");
		sql.append(" customer_id =?  and active = ? and app_id = ?	\n");
		
		columnType.add("int");
		columnValue.add(customerId);				
		columnType.add("int");
		columnValue.add("0");
		columnType.add("String");
		columnValue.add(appId);
		String sqlString = sql.toString();
		int result = DbUtils.getInstance().excuteExpertQAQuery(sql.toString(), columnType, columnValue);
		return result;

	}
	
	public List<HashMap<String, Object>> queryPushToken() {
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT * FROM MEDICAL_HQ.DBO.PUSHTOKEN WHERE ENABLE = '1' ");
		String sqlString = sql.toString();
		List<HashMap<String, Object>> result = DbUtils.getInstance().excuteQueryL(sqlString);
		return result;
		
	}
	
	
	
}