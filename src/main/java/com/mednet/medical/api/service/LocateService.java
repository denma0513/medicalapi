package com.mednet.medical.api.service;

import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mednet.medical.api.dao.LocateDao;
import com.mednet.medical.api.utils.CommonUtils;
import com.mednet.medical.api.utils.RandomGuid;

@Service
public class LocateService {

	@Autowired
	LocateDao locateDao;
	/**
	 * 新增地點
	 * @param json : JSONObject
	 * @author dennis
	 * @return 
	 * */
	public JSONObject createLocate(JSONObject json) {
		String organizationID = json.optString("organizationID");
		 JSONArray locateData = json.optJSONArray("locateData");
		List<HashMap<String, Object>> queryResult = locateDao.queryOrganizationList(organizationID);
		if (queryResult == null || queryResult.isEmpty()) {
			return CommonUtils.setResponse(false, "No organization", "502", "");
		}
		
		for (int i = 0; i < locateData.length(); i++) {
			String guid = new RandomGuid().RandomGUID(true).toString();
			JSONObject obj = locateData.optJSONObject(i);
			String name = obj.optString("name");
			String nameEn = obj.optString("nameEn");
			String type = obj.optString("type");
			obj.put("status", locateDao.createLocate(guid, organizationID, name, nameEn, type));
		}
		
		return CommonUtils.setResponse(true, "success", "200", "", json);
	}
}
