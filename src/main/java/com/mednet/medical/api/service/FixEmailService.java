package com.mednet.medical.api.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONObject;
import org.json.JSONArray;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import com.mednet.medical.api.utils.CommonUtils;
import com.turo.pushy.apns.ApnsClient;
import com.turo.pushy.apns.ApnsClientBuilder;
import com.turo.pushy.apns.util.ApnsPayloadBuilder;
import com.turo.pushy.apns.util.SimpleApnsPushNotification;
import com.turo.pushy.apns.util.TokenUtil;



@Service
public class FixEmailService {
	private Log log = LogFactory.getLog("FixEmail");
	static String fcmUrl = "https://appapi.med-net.com/api/createUser";
	/**
	 * 修正建促app帳號缺少e-mail問題
	 * @param customerAppId 
	 * @param email 
	 * @param oAuth_Type 
	 * @param oAuth_id
	 * 
	 * */
	public String mednetCreateUser (String customerAppId, String email, String  oAuth_Type, String oAuth_id ){
		 
		HttpURLConnection conn;
        try {
            //建立查詢參數
            JSONObject params = new JSONObject();
            SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            String NoValue = null;
            //建立HTTP連線
            URL mURL = new URL(fcmUrl);
            conn = (HttpURLConnection) mURL.openConnection();
            conn.setRequestMethod("POST");
            //conn.setRequestProperty("Content-Length", length);
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(10000);
            conn.setDoOutput(true);
            //conn.setRequestProperty("Authorization", "key=AAAAfGFWx9A:APA91bE_0mVPrWBQ1kokq9_V7kVjcP5tD-9G-XNJE1t2WxfLYMNk0BUBXgDptwWU-kd1VJTHNe29oJgAH0l2-ZEDGbXhMkw9Wr4ZgiqSpilR4FeONfio3ZYPfgKYRuJbhkU09Wzk63BM");
            conn.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
            
            
            params.put("vendor_id", "Kx6gUu2A");
            params.put("vendor_token", "$2y$10$WtyKApAtHXdh23y9g1GHKuQJ5RDIu9tSV2hNmKeD9zufGxXpVITym");
            JSONObject profile = new JSONObject();
            
            profile.put("customer_app_id", customerAppId);
            profile.put("customer_id", "-1");
            profile.put("email", email);
            profile.put("password", "");
            profile.put("name", "");
            profile.put("first_name", "");
            profile.put("last_name", "");
            profile.put("id_number", "");
            profile.put("is_sms_check", false);
            profile.put("cellphone", "");
            profile.put("gender", "");
            profile.put("create_from", NoValue);
            profile.put("change_password_at", formatter1.format(new Date()));
            profile.put("login_at", formatter1.format(new Date()));
            profile.put("force_logout_at", NoValue);
            profile.put("self_introduction", "");
            profile.put("birthday", "");
            profile.put("profile_image", "");
            profile.put("created_at", formatter1.format(new Date()));
            profile.put("updated_at", formatter1.format(new Date()));
            
            params.put("profile", profile);
            
            JSONObject oauth = new JSONObject();

            oauth.put("type", Integer.parseInt(oAuth_Type));
            oauth.put("social_id", oAuth_id);
            JSONArray oauthA = new JSONArray();
            oauthA.put(0,oauth);
            params.put("oauth", oauthA);
            
            conn.setRequestProperty("Content-Length", params.toString());
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
            outputStreamWriter.write(params.toString());
            outputStreamWriter.flush();
            
            int status = conn.getResponseCode();
            log.info("status = ["+status+"]");
            if (status == HttpsURLConnection.HTTP_OK) {

                BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();
                JSONObject state = new JSONObject(response.toString());
                JSONObject Mednet_data = new JSONObject(state.optString("data"));
                JSONObject Mednet_profile = new JSONObject(Mednet_data.optString("profile"));
                String temp = Mednet_profile.optString("customer_id");
                return temp;
            }
        } catch (Exception ignored) {
            ignored.printStackTrace();
            return "ignored";
        }
		return "Else";
	}
	
}