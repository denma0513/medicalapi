package com.mednet.medical.api.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mednet.medical.api.dao.ExamitemDao;
import com.mednet.medical.api.dao.LocateDao;
import com.mednet.medical.api.dao.OrganizationDao;
import com.mednet.medical.api.utils.CommonUtils;
import com.mednet.medical.api.utils.Message;
import com.mednet.medical.api.utils.RandomGuid;

@Service
public class ExamitemService {

	@Autowired
	OrganizationDao organizationDao;

	@Autowired
	ExamitemDao examitemDao;

	/**
	 * 新增地點
	 * 
	 * @param json
	 *            : JSONObject
	 * @author dennis
	 * @return
	 */
	public JSONObject createExamitem(JSONObject json) {
		String organizationID = json.optString("organizationID");
		JSONArray examitemData = json.optJSONArray("examitemData");
		JSONArray resultJsonArray = new JSONArray();
		List<HashMap<String, Object>> queryResult = organizationDao.queryOrganizationList(organizationID);
		if (queryResult == null || queryResult.isEmpty()) {
			return CommonUtils.setResponse(false, Message.APIMSG502, "502", "");
		}

		for (int i = 0; i < examitemData.length(); i++) {
			JSONObject resultData =  new JSONObject();
			JSONObject examData = examitemData.optJSONObject(i);
			String code = examData.optString("code");
			JSONArray rulesDataList = examData.optJSONArray("rulesData");
			String checkID = examitemDao.checkExamitem(code);
			String guid = CommonUtils.isBlank(checkID)?new RandomGuid().RandomGUID(true).toString():checkID;
			resultData.put("code", code);
			// 判斷有沒有rule
			if (rulesDataList.length() > 0) {
				ArrayList<String> relativeIDArr = new ArrayList<String>();
				int num = rulesDataList.length();
				for (int j = 0; j < num; j++) {
					JSONObject rulesData = rulesDataList.optJSONObject(j);
					relativeIDArr.add(rulesData.optString("relativeID"));
				}

				// 判斷rule中的檢查項目是否完整
				if (num != examitemDao.examitemExistsCount(relativeIDArr)) {
					resultData.put("status", "0");
					resultData.put("statusMessage", Message.RESULTMSG500);
					resultJsonArray.put(resultData);
					continue;
				}
			}

			// 開始新增資料
			int result = examitemDao.createExamitem(guid, organizationID, code, examData);

			switch (result) {
			case 1:
				resultData.put("status", "1");
				resultData.put("statusMessage", Message.RESULTMSG200);
				break;
			case 0:
				resultData.put("status", "0");
				resultData.put("statusMessage", Message.RESULTMSG501);
				break;
			case -1:
				resultData.put("status", "0");
				resultData.put("statusMessage", Message.RESULTMSG501);
				break;
			}
			resultJsonArray.put(resultData);

		}

		return CommonUtils.setResponse(true, Message.RESULTMSG200, "200", "", resultJsonArray);
	}
}
