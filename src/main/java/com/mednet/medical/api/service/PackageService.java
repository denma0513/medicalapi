package com.mednet.medical.api.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mednet.medical.api.dao.ExamitemDao;
import com.mednet.medical.api.dao.LocateDao;
import com.mednet.medical.api.dao.PackageDao;
import com.mednet.medical.api.utils.CommonUtils;
import com.mednet.medical.api.utils.Message;
import com.mednet.medical.api.utils.RandomGuid;

@Service
public class PackageService {

	@Autowired
	LocateDao locateDao;

	@Autowired
	ExamitemDao examitemDao;

	@Autowired
	PackageDao packageDao;

	private Log log = LogFactory.getLog("file");

	/**
	 * 新增套組
	 * 
	 * @param json
	 * @author dennis
	 * @return
	 */
	public JSONObject createPackage(JSONObject json) {
		String organizationID = json.optString("organizationID");
		JSONArray packageData = json.optJSONArray("packageData");
		JSONArray resultJsonArray = new JSONArray();
		
		// check Organization
		List<HashMap<String, Object>> queryResult = locateDao.queryOrganizationList(organizationID);
		if (queryResult == null || queryResult.isEmpty()) {
			return CommonUtils.setResponse(false, Message.APIMSG502, "502", "");
		}

		for (int i = 0; i < packageData.length(); i++) {
			String guid = new RandomGuid().RandomGUID(true).toString();
			JSONObject resultData =  new JSONObject();
			JSONObject obj = packageData.optJSONObject(i);
			String name = obj.optString("name");
			JSONArray examitemIDList = obj.optJSONArray("examitemID");
			ArrayList<String> examitemIDArr = new ArrayList<String>();
			resultData.put("name", name);
			if (examitemIDList.length() <= 0) {
				resultData.put("status", "0");
				resultData.put("statusMessage", Message.RESULTMSG502);
				resultJsonArray.put(resultData);
				continue;
			}
			int num = examitemIDList.length();
			for (int j = 0; j < num; j++) {
				examitemIDArr.add(examitemIDList.optString(j));
			}
			//ArrayList<String> examitemIDArr = obj.get("examitemID") != null ? (ArrayList<String>) obj.get("examitemID") : new ArrayList<String>();
			// 判斷套組中的檢查項目是否完整
			if (num != examitemDao.examitemExistsCount(examitemIDArr)) {
				resultData.put("status", "0");
				resultData.put("statusMessage", Message.RESULTMSG500);
				resultJsonArray.put(resultData);
				continue;
			}
			int result = packageDao.createPackage(guid, organizationID, name, examitemIDArr);
			if (result <= 0 ) {
				resultData.put("status", "0");
				resultData.put("statusMessage", Message.RESULTMSG501);
				resultJsonArray.put(resultData);
			}else {
				resultData.put("status", "1");
				resultData.put("statusMessage", Message.RESULTMSG200);
				resultJsonArray.put(resultData);				
			}

		}

		return CommonUtils.setResponse(true, Message.APIMSG200, "200", "", resultJsonArray);
	}
}
