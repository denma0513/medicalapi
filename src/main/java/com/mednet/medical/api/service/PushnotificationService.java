package com.mednet.medical.api.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONObject;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import com.mednet.medical.api.utils.CommonUtils;
import com.turo.pushy.apns.ApnsClient;
import com.turo.pushy.apns.ApnsClientBuilder;
import com.turo.pushy.apns.util.ApnsPayloadBuilder;
import com.turo.pushy.apns.util.SimpleApnsPushNotification;
import com.turo.pushy.apns.util.TokenUtil;

@Service
public class PushnotificationService {
	private Log log = LogFactory.getLog("PushNotification");
	static String fcmUrl = "https://fcm.googleapis.com/fcm/send";
	/**
	 * 發送andoird 推播
	 * @param content 
	 * @param title 
	 * @param pushToken 
	 * 
	 * 
	 * */
	public void sendAndroidNotification (String pushToken, String title, String content, boolean checkInTypeSound , String url){
		 
		HttpURLConnection conn;
        try {
            //建立查詢參數
            JSONObject params = new JSONObject();

            //建立HTTP連線
            URL mURL = new URL(fcmUrl);
            conn = (HttpURLConnection) mURL.openConnection();
            conn.setRequestMethod("POST");
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(10000);
            conn.setDoOutput(true);
            conn.setRequestProperty("Authorization", "key=AAAAfGFWx9A:APA91bE_0mVPrWBQ1kokq9_V7kVjcP5tD-9G-XNJE1t2WxfLYMNk0BUBXgDptwWU-kd1VJTHNe29oJgAH0l2-ZEDGbXhMkw9Wr4ZgiqSpilR4FeONfio3ZYPfgKYRuJbhkU09Wzk63BM");
            conn.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
            
            params.put("to", pushToken);
            JSONObject data = new JSONObject(); 
            data.put("title", title);
            data.put("content", content);
            if (checkInTypeSound) {
            	data.put("sound", "event_notice");
            }
            if (!CommonUtils.isBlank(url)) {
            	data.put("url", url);
            }
   
            params.put("data", data);
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
            outputStreamWriter.write(params.toString());
            outputStreamWriter.flush();
            log.info("send notification params = ["+params+"]");
			
            int status = conn.getResponseCode();
            log.info("status = ["+status+"]");
			
            if (status == HttpsURLConnection.HTTP_OK) {

                BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();
                JSONObject state = new JSONObject(response.toString());
            }
        } catch (Exception ignored) {
            ignored.printStackTrace();
        }
		
		
	}
	
	public void sendiOSNotification (String pushToken, String title, String content, boolean checkInTypeSound , String url){
		
		try {
			//ios
			Resource resource = new ClassPathResource("TestApns.p12");
		
			ApnsClient apnsClient;
			apnsClient = new ApnsClientBuilder()
					.setApnsServer(ApnsClientBuilder.PRODUCTION_APNS_HOST)
				    .setClientCredentials(resource.getFile(), "")
				    .build();
			final ApnsPayloadBuilder payloadBuilder = new ApnsPayloadBuilder();
		    payloadBuilder.setAlertBody(content);
		    payloadBuilder.setAlertTitle(title);
		    if (checkInTypeSound) {
		    	payloadBuilder.setSound("notification_notice.aiff");
		    }
		    else {
		    	payloadBuilder.setSound("default");
		    }
		    if (!CommonUtils.isBlank(url)) {
		    	payloadBuilder.addCustomProperty("url", url);
		    }

		    final String payload = payloadBuilder.buildWithDefaultMaximumLength();
		    final String token = TokenUtil.sanitizeTokenString(pushToken);

		    SimpleApnsPushNotification pushNotification = new SimpleApnsPushNotification(token, "com.MediCrowd.test", payload);
			
		    apnsClient.sendNotification(pushNotification);
		} catch (SSLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
