package com.mednet.medical.api.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import com.mednet.medical.api.utils.CommonUtils;
import com.turo.pushy.apns.ApnsClient;
import com.turo.pushy.apns.ApnsClientBuilder;
import com.turo.pushy.apns.util.ApnsPayloadBuilder;
import com.turo.pushy.apns.util.SimpleApnsPushNotification;
import com.turo.pushy.apns.util.TokenUtil;

@Service
public class AIDoctorService {
	String mednetUrlPrefix = "https://appapi.med-net.com/api/";
	String mednetStageOriginUrlPrefix = "https://stageapi.med-net.com/api/";
	String mednetStageUrlPrefix = "https://stage.med-net.com/api/";
	private Log log = LogFactory.getLog("Controller");
	public JSONObject MednetApiGetUserProfileByCID (String customerId){
		HttpURLConnection conn;
        try {
            //建立查詢參數
            JSONObject params = new JSONObject();

            //建立HTTP連線
            URL mURL = new URL(mednetStageOriginUrlPrefix+"getUserProfileByCID");
            conn = (HttpURLConnection) mURL.openConnection();
            conn.setRequestMethod("POST");
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(10000);
            conn.setDoOutput(true);
            conn.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
            
            params.put("vendor_id", "8Ac6XBGf");
            params.put("vendor_token","$2y$10$93ADzjFvqDjSU0bHNinhjOpcLkhoW/EcbUurRw9ZiJnWwjmNAhO5u");
            params.put("customer_id",customerId);
 
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
            outputStreamWriter.write(params.toString());
            outputStreamWriter.flush();
			
            int status = conn.getResponseCode();
   
			
            if (status == HttpsURLConnection.HTTP_OK) {

                BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();
                return new JSONObject(response.toString());
            }
            else {
            	return null;
            }
        } catch (Exception ignored) {
            ignored.printStackTrace();
            return null;
        }
	}
	
	public JSONObject getContentForSymptoms (String keywords){
		HttpURLConnection conn;
        try {
            //建立查詢參數
            JSONObject params = new JSONObject();
            
            //建立HTTP連線
            URL mURL = new URL(mednetStageUrlPrefix+"ContentForSymptoms");
            conn = (HttpURLConnection) mURL.openConnection();
            conn.setRequestMethod("POST");
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(10000);
            conn.setDoOutput(true);
            conn.setRequestProperty("Content-Type", "application/json");
            
            params.put("vendor_id", "8Ac6XBGf");
            params.put("vendor_token","$2y$10$93ADzjFvqDjSU0bHNinhjOpcLkhoW/EcbUurRw9ZiJnWwjmNAhO5u");
            params.put("text_content",keywords);
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
            outputStreamWriter.write(params.toString());
            outputStreamWriter.flush();
			
            int status = conn.getResponseCode();
   
			
            if (status == HttpsURLConnection.HTTP_OK) {

                BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();
                return new JSONObject(response.toString());
            }
            else {
            	return null;
            }
        } catch (Exception ignored) {
            ignored.printStackTrace();
            return null;
        }
	}
	
	
	public JSONObject getDoctors (String divisionId , JSONArray countryList, String orderType){
		HttpURLConnection conn;
        try {
            //建立查詢參數
            JSONObject params = new JSONObject();

            //建立HTTP連線
            URL mURL = new URL(mednetStageUrlPrefix+"DivisionForDoctors");
            conn = (HttpURLConnection) mURL.openConnection();
            conn.setRequestMethod("POST");
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(10000);
            conn.setDoOutput(true);
            conn.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
            
            params.put("vendor_id", "8Ac6XBGf");
            params.put("vendor_token","$2y$10$93ADzjFvqDjSU0bHNinhjOpcLkhoW/EcbUurRw9ZiJnWwjmNAhO5u");
            params.put("division",divisionId);
            params.put("country_list",countryList);
            params.put("orderType",orderType);
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
            outputStreamWriter.write(params.toString());
            outputStreamWriter.flush();
			
            int status = conn.getResponseCode();
   
			
            if (status == HttpsURLConnection.HTTP_OK) {

                BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();
                return new JSONObject(response.toString());
            }
            else {
            	return null;
            }
        } catch (Exception ignored) {
            ignored.printStackTrace();
            return null;
        }
	}
	
	public JSONObject getProductss (JSONArray functionIdList , JSONArray cityList, String lowerPrice , String upper_price,String page , String orderType){
		HttpURLConnection conn;
        try {
            //建立查詢參數
            JSONObject params = new JSONObject();

            //建立HTTP連線
            URL mURL = new URL(mednetStageUrlPrefix+"FunctionItemForProducts");
            conn = (HttpURLConnection) mURL.openConnection();
            conn.setRequestMethod("POST");
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(10000);
            conn.setDoOutput(true);
            conn.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
            JSONArray test = new JSONArray("[\n" + 
            		"        {\n" + 
            		"            \"exam_funtion_id\": \"42C7F1F7-62AA-4416-AB07-FFF1E874DB83\"\n" + 
            		"        },\n" + 
            		"        {\n" + 
            		"            \"exam_function_id\": \"3180330F-D58E-45C3-9EDC-FE85CD164245\"\n" + 
            		"        }\n" + 
            		"    ]");
            
            params.put("vendor_id", "8Ac6XBGf");
            params.put("vendor_token","$2y$10$93ADzjFvqDjSU0bHNinhjOpcLkhoW/EcbUurRw9ZiJnWwjmNAhO5u");
            params.put("data", functionIdList);
            params.put("city_sort_list", cityList);
            params.put("lowerPrice", lowerPrice);
            params.put("upper_price", upper_price);
            params.put("page", page);
            params.put("orderType", orderType);
            
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
            outputStreamWriter.write(params.toString());
            outputStreamWriter.flush();
			
            int status = conn.getResponseCode();
   
			
            if (status == HttpsURLConnection.HTTP_OK) {

                BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();
                return new JSONObject(response.toString());
            }
            else {
            	return null;
            }
        } catch (Exception ignored) {
            ignored.printStackTrace();
            return null;
        }
	}
}
