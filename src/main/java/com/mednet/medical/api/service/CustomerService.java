package com.mednet.medical.api.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mednet.medical.api.base.baseService;
import com.mednet.medical.api.dao.CustomerDao;
import com.mednet.medical.api.dao.ExamitemDao;
import com.mednet.medical.api.dao.OrganizationDao;
import com.mednet.medical.api.dao.PackageDao;
import com.mednet.medical.api.entity.Customer;
import com.mednet.medical.api.utils.CommonUtils;
import com.mednet.medical.api.utils.Message;
import com.mednet.medical.api.utils.TimerUtils;

@Service
//extends baseService
public class CustomerService {

	@Autowired
	OrganizationDao organizationDao;

	@Autowired
	PackageDao packageDao;

	@Autowired
	ExamitemDao examitemDao;

	@Autowired
	CustomerDao customerDao;

	TimerUtils timer = new TimerUtils();

	private @Autowired HttpServletRequest request;
	
	/**
	 * 新增客戶資料
	 * 
	 * @param json
	 * @author dennis
	 * @return
	 */
	public JSONObject createCustomer(JSONObject json) {
		String organizationID = json.optString("organizationID");
		JSONArray customerWaitData = json.optJSONArray("customerWaitData");
		JSONArray resultJsonArray = new JSONArray();
		List<HashMap<String, Object>> queryResult = organizationDao.queryOrganizationList(organizationID);
		if (queryResult == null || queryResult.isEmpty()) {
			return CommonUtils.setResponse(false, Message.APIMSG502, "502", "");
		}

		// 依照匯入資料新增客戶
		for (int i = 0; i < customerWaitData.length(); i++) {
			JSONObject resultData = new JSONObject();
			Customer customer = new Customer();
			JSONObject customerData = customerWaitData.optJSONObject(i);
			customer.setEntityData(customerData);

			// 判斷是匯入套組名稱或是匯入健診項目
			if (!CommonUtils.isBlank(customer.getPgName())) {
				// 匯入套組
				if (packageDao.packageExistsCount(customer.getPgName()) <= 0) {
					customer.setValid(false);
					customer.setErrorMessage(Message.RESULTMSG507);
				}
			} else if (customer.getExamitemCode() != null && customer.getExamitemCode().length() > 0) {
				// 匯入健診項目
				int num = customer.getExamitemCode().length();
				ArrayList<String> examitemCodeArr = new ArrayList<String>();
				for (int j = 0; j < num; j++) {
					examitemCodeArr.add(customer.getExamitemCode().optString(j));
				}
				// 判斷檢查項目是否完整
				if (num != examitemDao.examitemExistsCountByCode(examitemCodeArr)) {
					customer.setValid(false);
					customer.setErrorMessage(Message.RESULTMSG500);
				}
			}

			resultData.put("governmentID", customer.getGovernmentID());
			resultData.put("passportID", customer.getPassportID());
			resultData.put("residencePermetID", customer.getResidencePermitID());

			// 無效客戶
			if (!customer.isValid()) {
				resultData.put("status", "0");
				resultData.put("message", customer.getErrorMessage());
				resultJsonArray.put(resultData);
				continue;
			}
			int result = customerDao.createCustomer(customer);
			if (result <= 0) {
				resultData.put("status", "0");
				resultData.put("message", customer.getErrorMessage());
				resultJsonArray.put(resultData);
				continue;
			}

			resultData.put("status", "1");
			resultData.put("message", Message.RESULTMSG200);
			resultJsonArray.put(resultData);

			// 如套組不為空則建立檢查項目
			timer.setTitle("examitemIDList").startTimer();
			List<HashMap<String, Object>> examitemIDList = new ArrayList<HashMap<String, Object>>();
			if (!CommonUtils.isBlank(customer.getPgName())) {
				examitemIDList = packageDao.queryPackageExamitemsRoom(customer.getPgName());
			} else if (customer.getExamitemCode() != null && customer.getExamitemCode().length() > 0) {
				examitemIDList = packageDao.queryExamitemsRoom(customer.getExamitemCode());
			}
			timer.endTimer();

			if (examitemIDList.size() > 0) {
				examitemIDList = firstOrDefault(examitemIDList, "ECODE");
				// 清除Check item list
				customerDao.clearCheckitemlist(customer.getContactID());
				JSONArray insertList = new JSONArray();
				// 批次匯入check item list
				for (int j = 0; j < examitemIDList.size(); j++) {
					JSONObject insertObj = new JSONObject();
					HashMap<String, Object> examitemIDData = examitemIDList.get(j);
					String examitemName = CommonUtils.chkValue(examitemIDData.get("ENAME"));
					String examitemCode = CommonUtils.chkValue(examitemIDData.get("ECODE"));
					String roomCode = CommonUtils.chkValue(examitemIDData.get("RCODE"));
					insertObj.put("examitemName", examitemName);
					insertObj.put("examitemCode", examitemCode);
					insertObj.put("roomCode", roomCode);
					insertObj.put("contactID", customer.getContactID());
					insertList.put(insertObj);
				}
				customerDao.insCheckitemlist(insertList);
			}

		}

		return CommonUtils.setResponse(true, Message.RESULTMSG200, "200", "", resultJsonArray);
	}

	/**
	 * 記錄客戶進出診間資料
	 * 
	 * @param json
	 * @author dennis
	 * @return
	 */
	public JSONObject setUserLog(JSONObject json) {
		JSONObject resultObj = new JSONObject();
		// { NewBarCodeID: code, RoomID: roomcode, Status: status, FunItemIDs:
		// funitem },
		String action = json.optString("Action");
		String newBarCodeID = json.optString("NewBarCodeID");
		String roomID = json.optString("RoomID");
		String status = json.optString("Status");
		String[] funItemIDs = json.optString("FunItemIDs").split(",");
		Date date = new Date();

		// SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
		// SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

		String dateStr = formatter.format(date);

		// 依照funItemIDs逐筆寫入紀錄
		for (int i = 0; i < funItemIDs.length; i++) {
			HashMap<String, Object> examData = examitemDao.queryExamitemRoomByID(funItemIDs[i]);
			if ("in".equals(action)) {
				customerDao.createCustomerLog(action, newBarCodeID, roomID, funItemIDs[i],
						CommonUtils.chkValue(examData.get("NAME")), status, dateStr);
			} else if ("out".equals(action)) {
				customerDao.updateCustomerLog(action, newBarCodeID, roomID, funItemIDs[i],
						CommonUtils.chkValue(examData.get("NAME")), status, dateStr);
			} else {
				customerDao.updateCustomerLog(action, newBarCodeID, roomID, funItemIDs[i],
						CommonUtils.chkValue(examData.get("NAME")), status, "");
			}
		}

		return CommonUtils.setResponse(true, Message.RESULTMSG200, "200", "");
	}

	/**
	 * 記錄客戶進出診間資料
	 * 
	 * @param json
	 * @author dennis
	 * @return
	 */
	public JSONObject updateUserStatus(JSONObject json) {
		JSONObject resultObj = new JSONObject();
		String organizationID = json.optString("organizationID");
		String contactCode = json.optString("NewBarCodeID");
		JSONArray examitemCode = json.optJSONArray("examitemCode");
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		SimpleDateFormat formatter2 = new SimpleDateFormat("yyyy-MM-dd");
		String dateStr = formatter.format(date);

		List<HashMap<String, Object>> organizationResult = organizationDao.queryOrganizationList(organizationID);
		if (organizationResult == null || organizationResult.isEmpty()) {
			return CommonUtils.setResponse(false, Message.APIMSG502, "502", "");
		}

		// 取出客戶進出紀錄（暫存）
		List<HashMap<String, Object>> customerLogList = customerDao.queryCustomerLogByContactCode(contactCode);
		// 取客戶資料
		HashMap<String, Object> customerResult = customerDao.queryCustomerByContactCode(contactCode);
		if (customerResult == null) {
			return CommonUtils.setResponse(true, Message.APIMSG503, "503", "");
		}
		
		//set customer
		Customer customer = new Customer();
		customer.setContactID(contactCode);
		customer.setNewBarCodeID(contactCode);
		customer.setDisplayName(CommonUtils.chkValue(customerResult.get("Name")));
		customer.setGovernmentID(CommonUtils.chkValue(customerResult.get("IdentityID")));
		customer.setGender(CommonUtils.chkValue(customerResult.get("Gender")));
		customer.setBirthday(CommonUtils.chkValue(customerResult.get("Birthday")));
		customer.setPassportID(CommonUtils.chkValue(customerResult.get("PassportID")));
		customer.setEmail(CommonUtils.chkValue(customerResult.get("Email")));
		customer.setEmail2(CommonUtils.chkValue(customerResult.get("Email2")));
		customer.setPhoneNumberArea(CommonUtils.chkValue(customerResult.get("PhoneArea")));
		customer.setPhoneNumber(CommonUtils.chkValue(customerResult.get("PhoneNumber")));
		customer.setPhoneExt(CommonUtils.chkValue(customerResult.get("PhoneExt")));
		customer.setMobilePhone(CommonUtils.chkValue(customerResult.get("Mobile")));
		customer.setQuestionWebUrl(CommonUtils.chkValue(customerResult.get("QuestionLink")));
		customer.setLevel(CommonUtils.chkValue(customerResult.get("Level")));
		customer.setExtNotes(CommonUtils.chkValue(customerResult.get("ExtNotes")));
		customer.setHealthCheckDate(formatter2.format(date));
		int result = customerDao.createCustomer(customer);
		if (result <= 0) {
			return CommonUtils.setResponse(true, Message.APIMSG503, "503", "");
		}

		//將客戶進出資料轉成map
		HashMap<String, HashMap<String, Object>> checkItemListMap = new HashMap<String, HashMap<String, Object>>();
		for (int i = 0; i < customerLogList.size(); i++) {
			HashMap<String, Object> customerLog = customerLogList.get(i);
			String code = CommonUtils.chkValue(customerLog.get("FunItemID"));
			checkItemListMap.put(code, customerLog);
		}

		List<HashMap<String, Object>> examitemIDList = packageDao.queryExamitemsRoom(examitemCode);
		if (examitemIDList.size() > 0) {
			examitemIDList = firstOrDefault(examitemIDList, "ECODE");
			// 清除Check item list
			customerDao.clearCheckitemlist(contactCode);
			JSONArray insertList = new JSONArray();
			// 批次匯入check item list
			for (int j = 0; j < examitemIDList.size(); j++) {
				JSONObject insertObj = new JSONObject();
				HashMap<String, Object> examitemIDData = examitemIDList.get(j);
				String examName = CommonUtils.chkValue(examitemIDData.get("ENAME"));
				String examCode = CommonUtils.chkValue(examitemIDData.get("ECODE"));
				String roomCode = CommonUtils.chkValue(examitemIDData.get("RCODE"));
				if (null != checkItemListMap.get(examCode)) {
					HashMap<String, Object> checkItem = checkItemListMap.get(examCode);
					String status = CommonUtils.chkValue(checkItem.get("Status"));
					String checkInTime = CommonUtils.chkValue(checkItem.get("CheckInTime"));
					String checkOutTime = CommonUtils.chkValue(checkItem.get("CheckOutTime"));
					insertObj.put("status", status);
					insertObj.put("checkInTime", checkInTime);
					insertObj.put("checkOutTime", checkOutTime);
				}
				insertObj.put("examitemName", examName);
				insertObj.put("examitemCode", examCode);
				insertObj.put("roomCode", roomCode);
				insertObj.put("contactID", contactCode);
				insertList.put(insertObj);
			}
			customerDao.insCheckitemlist(insertList);
		}

		return CommonUtils.setResponse(true, Message.RESULTMSG200, "200", "");
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ArrayList<HashMap<String, Object>> firstOrDefault(List<HashMap<String, Object>> examitemIDList,
			String filter) {
		HashMap tempMap = new HashMap();
		ArrayList<HashMap<String, Object>> retList = new ArrayList<HashMap<String, Object>>();
		for (int i = 0; i < examitemIDList.size(); i++) {
			HashMap<String, Object> map = examitemIDList.get(i);
			if (tempMap.get(map.get(filter)) != null) {
				continue;
			}
			tempMap.put(map.get(filter), "1");
			retList.add(map);
		}
		return retList;
	}

}
