package com.mednet.medical.api.entity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import com.mednet.medical.api.utils.CommonUtils;
import com.mednet.medical.api.utils.Message;

public class Customer {
	// table column
	private String contactID;
	private String displayName;
	private String governmentID;
	private String gender;
	private String birthday;
	private String passportID;
	private String residencePermitID;
	private String email;
	private String email2;
	private String phoneNumberCountry;
	private String phoneNumberArea;
	private String phoneNumber;
	private String phoneExt;
	private String mobilePhone;
	private String questionWebUrl;
	private String newBarCodeID;
	private String reportID;
	private String pgName;
	private String level;
	private String extNotes;
	private String address;
	private String lockerID;
	private String lockerType;
	private String healthCheckDate;
	private String reportDelivery;

	// extra values
	private JSONArray examitemCode;

	// message
	private boolean isValid = true;
	private String errorMessage;

	public Customer() {
		this.contactID = "";
		this.displayName = "";
		this.governmentID = "";
		this.gender = "";
		this.birthday = "";
		this.passportID = "";
		this.residencePermitID = "";
		this.email = "";
		this.email2 = "";
		this.phoneNumberCountry = "";
		this.phoneNumberArea = "";
		this.phoneNumber = "";
		this.phoneExt = "";
		this.mobilePhone = "";
		this.questionWebUrl = "";
		this.newBarCodeID = "";
		this.reportID = "";
		this.pgName = "";
		this.level = "";
		this.extNotes = "";
		this.address = "";
		this.lockerID = "";
		this.lockerType = "";
		this.healthCheckDate = "";
		this.reportDelivery = "";
		this.examitemCode = new JSONArray();
	}

	public String getContactID() {
		return contactID;
	}

	public void setContactID(String contactID) {
		this.contactID = contactID;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getGovernmentID() {
		return governmentID;
	}

	public void setGovernmentID(String governmentID) {
		this.governmentID = governmentID;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getPassportID() {
		return passportID;
	}

	public void setPassportID(String passportID) {
		this.passportID = passportID;
	}

	public String getResidencePermitID() {
		return residencePermitID;
	}

	public void setResidencePermitID(String residencePermitID) {
		this.residencePermitID = residencePermitID;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEmail2() {
		return email2;
	}

	public void setEmail2(String email2) {
		this.email2 = email2;
	}

	public String getPhoneNumberCountry() {
		return phoneNumberCountry;
	}

	public void setPhoneNumberCountry(String phoneNumberCountry) {
		this.phoneNumberCountry = phoneNumberCountry;
	}

	public String getPhoneNumberArea() {
		return phoneNumberArea;
	}

	public void setPhoneNumberArea(String phoneNumberArea) {
		this.phoneNumberArea = phoneNumberArea;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getPhoneExt() {
		return phoneExt;
	}

	public void setPhoneExt(String phoneExt) {
		this.phoneExt = phoneExt;
	}

	public String getMobilePhone() {
		return mobilePhone;
	}

	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}

	public String getQuestionWebUrl() {
		return questionWebUrl;
	}

	public void setQuestionWebUrl(String questionWebUrl) {
		this.questionWebUrl = questionWebUrl;
	}

	public String getNewBarCodeID() {
		return newBarCodeID;
	}

	public void setNewBarCodeID(String newBarCodeID) {
		this.newBarCodeID = newBarCodeID;
	}

	public String getReportID() {
		return reportID;
	}

	public void setReportID(String reportID) {
		this.reportID = reportID;
	}

	public String getPgName() {
		return pgName;
	}

	public void setPgName(String pgName) {
		this.pgName = pgName;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getExtNotes() {
		return extNotes;
	}

	public void setExtNotes(String extNotes) {
		this.extNotes = extNotes;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getLockerID() {
		return lockerID;
	}

	public void setLockerID(String lockerID) {
		this.lockerID = lockerID;
	}

	public String getLockerType() {
		return lockerType;
	}

	public void setLockerType(String lockerType) {
		this.lockerType = lockerType;
	}

	public String getHealthCheckDate() {
		return healthCheckDate;
	}

	public void setHealthCheckDate(String healthCheckDate) {
		this.healthCheckDate = healthCheckDate;
	}

	public String getReportDelivery() {
		return reportDelivery;
	}

	public void setReportDelivery(String reportDelivery) {
		this.reportDelivery = reportDelivery;
	}

	public boolean isValid() {
		return isValid;
	}

	public void setValid(boolean isValid) {
		this.isValid = isValid;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public JSONArray getExamitemCode() {
		return examitemCode;
	}

	public void setExamitemCode(JSONArray examitemCode) {
		this.examitemCode = examitemCode;
	}

	public Customer setEntityData(JSONObject json) {
		this.displayName = json.optString("displayName");
		this.governmentID = json.optString("governmentID");
		this.gender = json.optString("gender");
		this.birthday = json.optString("birthday");
		this.passportID = json.optString("passportID");
		this.residencePermitID = json.optString("residencePermitID");
		this.email = json.optString("email");
		this.email2 = json.optString("email2");
		this.phoneNumberCountry = json.optString("phoneNumberCountry");
		this.phoneNumberArea = json.optString("phoneNumberArea");
		this.phoneNumber = json.optString("phoneNumber");
		this.phoneExt = json.optString("phoneExt");
		this.mobilePhone = json.optString("mobilePhone");
		this.pgName = json.optString("pgName");
		this.level = json.optString("level");
		this.extNotes = json.optString("extNotes");
		this.address = json.optString("address");
		this.lockerID = json.optString("lockerID");
		this.healthCheckDate = json.optString("healthCheckDate");
		this.examitemCode = json.optJSONArray("examitemCode");

		if (!checkEntity()) {
			return this;
		}

		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
		SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat formatter2 = new SimpleDateFormat("yyyy/MM/dd");

		try {
			this.healthCheckDate = formatter1.format(formatter.parse(this.healthCheckDate));
			if (!CommonUtils.isBlank(this.birthday)) {
				this.birthday = formatter2.format(formatter.parse(this.birthday));
			}
		} catch (ParseException e) {
			this.isValid = false;
			this.errorMessage = Message.RESULTMSG505;
			return this;
		}

		try {
			if (!CommonUtils.isBlank(this.governmentID)) {
				this.contactID = this.governmentID;
			} else if (!CommonUtils.isBlank(this.passportID)) {
				this.contactID = this.passportID;
			} else if (!CommonUtils.isBlank(this.residencePermitID)) {
				this.contactID = this.residencePermitID;
			}

			this.newBarCodeID = this.contactID;
		} catch (Exception e) {
			this.isValid = false;
			this.errorMessage = Message.RESULTMSG506;
			return this;
		}

		if (!CommonUtils.isBlank(this.lockerID))
			this.lockerType = this.lockerID.substring(0, 1);
		return this;
	}

	private boolean checkEntity() {
		if (CommonUtils.isBlank(this.displayName)) {
			this.isValid = false;
			this.errorMessage = String.format(Message.RESULTMSG503, "displayName");
			return false;
		}
		if (CommonUtils.isBlank(this.governmentID) && CommonUtils.isBlank(this.passportID)
				&& CommonUtils.isBlank(this.residencePermitID)) {
			this.isValid = false;
			this.errorMessage = Message.RESULTMSG504;
			return false;
		}
		if (CommonUtils.isBlank(this.gender)) {
			this.isValid = false;
			this.errorMessage = String.format(Message.RESULTMSG503, "gender");
			return false;
		}
		if (CommonUtils.isBlank(this.pgName) && (this.examitemCode == null || this.examitemCode.length() <= 0)) {
			this.isValid = false;
			this.errorMessage = String.format(Message.RESULTMSG508, "pgName", "examitemCode");
			return false;
		}
		if (CommonUtils.isBlank(this.healthCheckDate)) {
			this.isValid = false;
			this.errorMessage = String.format(Message.RESULTMSG503, "healthCheckDate");
			return false;
		}
		return true;
	}

}
