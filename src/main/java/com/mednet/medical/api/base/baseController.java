package com.mednet.medical.api.base;

import javax.ws.rs.core.MediaType;

import org.json.JSONObject;

import com.mednet.medical.api.utils.CommonUtils;
import com.mednet.medical.api.utils.Message;

public class baseController {
	protected JSONObject retJson;
	protected String CONTENTTYPE_JSON_UTF8 = MediaType.APPLICATION_JSON + ";charset=UTF-8";
	protected JSONObject reqJson;
	
	protected void init () {
		retJson = null;
		reqJson = null;
	}
	
	protected void returnMessage (String message, String code) {
		//retJSON.op
	}
	
//	protected void setResponse (boolean result, String message, String code, String exception){
//		retJson = new JSONObject();
//		retJson.put("result", result?"success":"fail");
//		retJson.put("code", code);
//		retJson.put("message", message);
//		retJson.put("exception", exception);
//	}
	
	protected void checkJSONPara (String jsonString) throws Exception{
		if (CommonUtils.isBlank(jsonString)) {
			retJson = CommonUtils.setResponse(false, Message.APIMSG501, "501", "");
			throw new Exception("request para error"); 
		}
		try {
			reqJson = new JSONObject(jsonString);
		}catch (Exception e) {
			retJson = CommonUtils.setResponse(false, Message.APIMSG501, "501", "");
			throw new Exception("request para format error");
		}
	}

}
