package com.mednet.medical.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.File;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.MediaType;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.mednet.medical.api.base.baseController;
import com.mednet.medical.api.utils.CommonUtils;
import com.mednet.medical.api.utils.Message;
import com.turo.pushy.apns.ApnsClient;
import com.turo.pushy.apns.ApnsClientBuilder;
import com.turo.pushy.apns.util.ApnsPayloadBuilder;
import com.turo.pushy.apns.util.SimpleApnsPushNotification;
import com.turo.pushy.apns.util.TokenUtil;
import com.mednet.medical.api.dao.PushNotificationDao;
import com.mednet.medical.api.dao.PushTokenSaveDao;
import com.mednet.medical.api.service.PushnotificationService;

@Controller
public class PushNotificationController extends baseController {
	@Autowired
	PushNotificationDao pushNotificationDao;
	@Autowired
	PushnotificationService pushnotificationService;
	@Autowired
	PushTokenSaveDao pushTokenSaveDao;
	private Log log = LogFactory.getLog("PushNotification");

	@RequestMapping(value = "/PushNotification", method = RequestMethod.POST ,produces = MediaType.APPLICATION_JSON + ";charset=UTF-8")
	@ResponseBody
	public String PushNotification(@RequestBody String jsonString, HttpServletRequest request) throws JSONException {
		try {
			init () ;
			log.info("PushNotification request = ["+jsonString+"]");
			checkJSONPara(jsonString);
			String title = "智慧健診";
			String pushToken = reqJson.optString("pushToken");
			String content =  reqJson.optString("content");
			if (pushToken.length() > 100) {
				//android
				pushnotificationService.sendAndroidNotification(pushToken, title, content,true,"");
			} else {
				//ios
				pushnotificationService.sendiOSNotification(pushToken, title, content, true,"");
			    
			}
		    JSONObject retJson;
			retJson = CommonUtils.setResponse(true,  Message.APIMSG200, "200", "");
			return retJson.toString();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			if (retJson == null || retJson.isNull("result")) {
				retJson = CommonUtils.setResponse(false, "call api fail!", "500", e.getMessage());
			}
			return retJson.toString();
		}
		
	}
	
	@RequestMapping(value = "/RegisterPushNotificationToken", produces = MediaType.APPLICATION_JSON + ";charset=UTF-8")
	@ResponseBody
	public String customerNotificationData(@RequestBody String jsonString) throws JSONException {
		try {
			init () ;
			checkJSONPara(jsonString);
			String message = "";
			if (CommonUtils.isBlank(reqJson.optString("pushToken"))) {
				message = "pushToken can't be null";
			}
			if (CommonUtils.isBlank(reqJson.optString("organization"))) {
				message = "organization can't be null";
			}
			if (CommonUtils.isBlank(reqJson.optString("name"))) {
				message = "name can't be null";
			}
			if (CommonUtils.isBlank(reqJson.optString("age"))) {
				message = "age can't be null";
			}
			if (CommonUtils.isBlank(reqJson.optString("gender"))) {
				message = "gender can't be null";
			}
			if (CommonUtils.isBlank(reqJson.optString("englishVersion"))) {
				message = "englishVersion can't be null";
			}
			if (CommonUtils.isBlank(message)) {
				Thread thread1 = new Thread(new Runnable() {
	                public void run() {
	                	pushNotificationDao.createPushToken(reqJson.optString("pushToken"), 
	        					reqJson.optString("organization"), 
	        					reqJson.optString("name"), 
	        					reqJson.optString("age"),
	        					reqJson.optString("gender"),
	        					reqJson.optString("phoneNumber"),
	        					reqJson.optString("englishVersion"));
	                }
	            });
				thread1.start();
				JSONObject retJson;
				retJson = CommonUtils.setResponse(true,  Message.APIMSG200, "200", "");
				return retJson.toString();
			}
			else {
				retJson = CommonUtils.setResponse(false, "call api fail!", "500", message);
				return retJson.toString();
			}
		} catch (Exception e) {
			if (retJson == null || retJson.isNull("result")) {
				retJson = CommonUtils.setResponse(false, "call api fail!", "500", e.getMessage());
			}
			return retJson.toString();
		}
	}
	
	@RequestMapping(value = "/SendAdvertisementNotification", produces = MediaType.APPLICATION_JSON + ";charset=UTF-8")
	@ResponseBody
	public String sendAdvertisementNotification(@RequestBody String jsonString) throws JSONException {
		try {
			init () ;
			checkJSONPara(jsonString);
			String message = "";
			if (CommonUtils.isBlank(reqJson.optString("message"))) {
				message = "message can't be null";
			}
			if (CommonUtils.isBlank(reqJson.optString("messageEn"))) {
				message = "messageEn can't be null";
			}
			if (CommonUtils.isBlank(reqJson.optString("url"))) {
				message = "url can't be null";
			}
			if (CommonUtils.isBlank(message)) {
				final String messageText = reqJson.optString("message");
				final String messageTextEn = reqJson.optString("messageEn");
				Thread thread1 = new Thread(new Runnable() {
	                public void run() {
	                	List<HashMap<String, Object>> result = pushNotificationDao.queryPushTokenWithCondition(reqJson.optString("maxAge"), reqJson.optString("minAge"),reqJson.optString("organization"), reqJson.optString("gender"),reqJson.optString("beforeDate").equals("1"));
	                	for (int i = 0 ; i < result.size() ; i++) {
	            			HashMap<String, Object> obj = result.get(i);
	            			String title = "智慧健診";
	            			if (obj.get("english_version").equals("1")) {
	            				if (obj.get("push_token").toString().length() > 100) {
	            					//android
	            					pushnotificationService.sendAndroidNotification(obj.get("push_token").toString(), title, messageTextEn,false,reqJson.optString("url"));
	            				} else {
	            					//ios
	            					pushnotificationService.sendiOSNotification(obj.get("push_token").toString(), title, messageTextEn, false,reqJson.optString("url"));
	            				    
	            				}
	            			}
	            			else {
	            				if (obj.get("push_token").toString().length() > 100) {
	            					//android
	            					pushnotificationService.sendAndroidNotification(obj.get("push_token").toString(), title, messageText,false,reqJson.optString("url"));
	            				} else {
	            					//ios
	            					pushnotificationService.sendiOSNotification(obj.get("push_token").toString(), title, messageText, false,reqJson.optString("url"));
	            				}
	            			}
	            		}
	                } 
	            });
				thread1.start();
				JSONObject retJson;
				retJson = CommonUtils.setResponse(true,  Message.APIMSG200, "200", "");
				return retJson.toString();
			}
			else {
				retJson = CommonUtils.setResponse(false, "call api fail!", "500", message);
				return retJson.toString();
			}
		} catch (Exception e) {
			if (retJson == null || retJson.isNull("result")) {
				retJson = CommonUtils.setResponse(false, "call api fail!", "500", e.getMessage());
			}
			return retJson.toString();
		}
	}
	
	
	@RequestMapping(value = "/SendMutipleAdvertisementNotification", produces = MediaType.APPLICATION_JSON + ";charset=UTF-8")
	@ResponseBody
	public String sendMutipleAdvertisementNotification(@RequestBody String jsonString) throws JSONException {
		try {
			init () ;
			checkJSONPara(jsonString);
			String message = "";
			if (CommonUtils.isBlank(reqJson.optString("message"))) {
				message = "message can't be null";
			}
			if (CommonUtils.isBlank(reqJson.optString("messageEn"))) {
				message = "messageEn can't be null";
			}
			if (CommonUtils.isBlank(reqJson.optString("url"))) {
				message = "url can't be null";
			}
			
			JSONArray customerArray = reqJson.optJSONArray("customer");
			
			if (customerArray == null || customerArray.length() == 0) {
				message = "customer can't be null or empty";
			}
			
			if (CommonUtils.isBlank(message)) {
				final String messageText = reqJson.optString("message");
				final String messageTextEn = reqJson.optString("messageEn");
				Thread thread1 = new Thread(new Runnable() {
	                public void run() {
	                	for (int i = 0 ; i < customerArray.length() ; i++) {
	            			JSONObject obj = customerArray.optJSONObject(i);
	            			String title = "智慧健診";
	     
	            			if (obj.optString("english_version").equals("1")) {
	            				if (obj.optString("push_token").toString().length() > 100) {
	            					//android
	            					pushnotificationService.sendAndroidNotification(obj.optString("push_token").toString(), title, messageTextEn,false,reqJson.optString("url"));
	            				} else {
	            					//ios
	            					pushnotificationService.sendiOSNotification(obj.optString("push_token").toString(), title, messageTextEn, false,reqJson.optString("url"));
	            				    
	            				}
	            			}
	            			else {
	            				if (obj.optString("push_token").toString().length() > 100) {
	            					//android
	            					pushnotificationService.sendAndroidNotification(obj.optString("push_token").toString(), title, messageText,false,reqJson.optString("url"));
	            				} else {
	            					//ios
	            					pushnotificationService.sendiOSNotification(obj.optString("push_token").toString(), title, messageText, false,reqJson.optString("url"));
	            				}
	            			}
	            		}
	                } 
	            });
				thread1.start();
				JSONObject retJson;
				retJson = CommonUtils.setResponse(true,  Message.APIMSG200, "200", "");
				return retJson.toString();
			}
			else {
				retJson = CommonUtils.setResponse(false, "call api fail!", "500", message);
				return retJson.toString();
			}
		} catch (Exception e) {
			if (retJson == null || retJson.isNull("result")) {
				retJson = CommonUtils.setResponse(false, "call api fail!", "500", e.getMessage());
			}
			return retJson.toString();
		}
	}
	
	
	/**
	 * 發送單人推播，主站用
	 * 
	 * */
	@RequestMapping(value = "/sendNotification", produces = MediaType.APPLICATION_JSON + ";charset=UTF-8")
	@ResponseBody
	public String sendNotification(@RequestBody String jsonString) throws JSONException {
		try {
			init () ;
			checkJSONPara(jsonString);
			String retMsg = "";
			if (CommonUtils.isBlank(reqJson.optString("customerID"))) {
				retMsg = "customerID can't be null";
			}
			if (CommonUtils.isBlank(reqJson.optString("message"))) {
				retMsg = "message can't be null";
			}
			final String customerID = reqJson.optString("customerID");
			final String title = reqJson.optString("title","醫聯網");
			final String message = reqJson.optString("message");
			final String url = reqJson.optString("url","");	                
			if (CommonUtils.isBlank(retMsg)) {
				sendMsg(customerID, title, message, url);
				JSONObject retJson;
				retJson = CommonUtils.setResponse(true,  Message.APIMSG200, "200", "");
				return retJson.toString();
			} else {
				retJson = CommonUtils.setResponse(false, "call api fail!", "500", retMsg);
				return retJson.toString();
			}
		} catch (Exception e) {
			if (retJson == null || retJson.isNull("result")) {
				retJson = CommonUtils.setResponse(false, "call api fail!", "500", e.getMessage());
			}
			return retJson.toString();
		}
	}
	
	/**
	 * 發送全員推播，主站用
	 * 
	 * */
	@RequestMapping(value = "/sendNotificationAllCustomer", produces = MediaType.APPLICATION_JSON + ";charset=UTF-8")
	@ResponseBody
	public String sendNotificationAllCustomer(@RequestBody String jsonString) throws JSONException {
		try {
			init () ;
			checkJSONPara(jsonString);
			String retMsg = "";
			if (CommonUtils.isBlank(reqJson.optString("message"))) {
				retMsg = "message can't be null";
			}
			final String title = reqJson.optString("title","醫聯網");
			final String message = reqJson.optString("message");
			final String url = reqJson.optString("url","");	                
			if (CommonUtils.isBlank(retMsg)) {
				sendMsg(title, message, url);
				JSONObject retJson;
				retJson = CommonUtils.setResponse(true,  Message.APIMSG200, "200", "");
				return retJson.toString();
			} else {
				retJson = CommonUtils.setResponse(false, "call api fail!", "500", retMsg);
				return retJson.toString();
			}
		} catch (Exception e) {
			if (retJson == null || retJson.isNull("result")) {
				retJson = CommonUtils.setResponse(false, "call api fail!", "500", e.getMessage());
			}
			return retJson.toString();
		}
	}
	
	/**
	 * 排程發送推播，主站用
	 * 
	 * */
	@RequestMapping(value = "/sendNotificationSchedule", produces = MediaType.APPLICATION_JSON + ";charset=UTF-8")
	@ResponseBody
	public String sendNotificationSchedule() throws JSONException {
		try {
			init () ;
			//checkJSONPara(jsonString);
			String retMsg = "";
			//System.out.println("sendNotificationSchedule");
			if (CommonUtils.isBlank(retMsg)) {
				Thread thread1 = new Thread(new Runnable() {
		            public void run() {
		            	List<HashMap<String, Object>> result = null;
		            	result = pushNotificationDao.queryNotificationNameList();
		            	//System.out.println("result = "+result);
		            	for (int i = 0 ; i < result.size() ; i++) {
		        			
		            		HashMap<String, Object> obj = result.get(i);
		            		String messageid = obj.get("MESSAGEID").toString();
		            		String customerId = obj.get("CUSTOMERID").toString();

		            		String pushtoken = obj.get("PUSHTOKEN").toString();
		        			String title = obj.get("TITLE").toString();
		        			String message = obj.get("CONTENT").toString();
		        			String url = obj.get("TARGETURL").toString();
		        			//System.out.println("messageid = "+messageid+" customerId = "+customerId);
		        			if (pushtoken.length() > 100) {
		    					//android
		    					pushnotificationService.sendAndroidNotification(pushtoken, title, message,false,url);
		    				} else {
		    					//ios
		    					pushnotificationService.sendiOSNotification(pushtoken, title, message, false,url);
		    				}
		        			//發完清資料
		        			pushNotificationDao.deleteScheduleByMsgidCusid(messageid, customerId);
		            	}
		            } 
		        });
				thread1.start();
				
				
				
				JSONObject retJson;
				retJson = CommonUtils.setResponse(true,  Message.APIMSG200, "200", "");
				return retJson.toString();
			} else {
				retJson = CommonUtils.setResponse(false, "call api fail!", "500", retMsg);
				return retJson.toString();
			}
		} catch (Exception e) {
			if (retJson == null || retJson.isNull("result")) {
				retJson = CommonUtils.setResponse(false, "call api fail!", "500", e.getMessage());
			}
			return retJson.toString();
		}
	}
	
	public void sendMsg(String title,String message,String url){
		sendMsg(null,title,message,url);
	}
		
	public void sendMsg(String customerID,String title,String message,String url){
		Thread thread1 = new Thread(new Runnable() {
            public void run() {
            	List<HashMap<String, Object>> result = null;
            	if (CommonUtils.isBlank(customerID)) {
            		result = pushTokenSaveDao.queryPushToken();
            	}else{
            		result = pushTokenSaveDao.queryPushTokenWithCustomerId(customerID);
            	}
            
            	for (int i = 0 ; i < result.size() ; i++) {
        			HashMap<String, Object> obj = result.get(i);
        			String pushtoken = obj.get("pushtoken").toString();
        			if (pushtoken.length() > 100) {
    					//android
    					pushnotificationService.sendAndroidNotification(pushtoken, title, message,false,url);
    				} else {
    					//ios
    					pushnotificationService.sendiOSNotification(pushtoken, title, message, false,url);
    				    
    				}
            	}
            } 
        });
		thread1.start();
	}
	
	
}
