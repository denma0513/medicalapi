package com.mednet.medical.api.controller;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStreamReader;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.MediaType;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mednet.medical.api.utils.CommonUtils;
import com.mednet.medical.api.utils.Message;

import com.mednet.medical.api.base.baseController;

@Controller
public class TestApiController extends baseController {
	@RequestMapping(value = "/TestApi", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON
			+ ";charset=UTF-8")
	@ResponseBody
	public String testApi(@RequestBody String jsonString, HttpServletRequest request) throws JSONException {
		try {
			init();
			checkJSONPara(jsonString);
			String path = reqJson.optString("path");

			if (CommonUtils.isBlank(path)) {
				retJson = CommonUtils.setResponse(false, Message.APIMSG500, "500", "file path empty");
			} else {
				// Resource resource = new ClassPathResource("MainPage.txt");
				// File filename = resource.getFile();
				String pathname = "C://usr/local/data/testApi/" + path + ".txt"; // 絕對路徑或相對路徑都可以，這裡是絕對路徑，寫入檔案時演示相對路徑
				File filename = new File(pathname); // 要讀取以上路徑的input。txt檔案
				InputStreamReader reader = new InputStreamReader(new FileInputStream(filename), "UTF-8"); // 建立一個輸入流物件reader
				BufferedReader br = new BufferedReader(reader); // 建立一個物件，它把檔案內容轉成計算機能讀懂的語言
				String line = "";
				String jsonStr = "";
				try {
					StringBuilder sb = new StringBuilder();
					line = br.readLine();

					while (line != null) {
						sb.append(line);

						line = br.readLine();
					}
					jsonStr = sb.toString();
				} finally {
					reader.close();
				}
				jsonStr = jsonStr.replaceAll(" ", "");

				JSONObject jsonObj = CommonUtils.parseJson(jsonStr);

				retJson = jsonObj;
			}

		} catch (Exception e) {
			if (retJson == null || retJson.isNull("result")) {
				retJson = CommonUtils.setResponse(false, Message.APIMSG500, "500", e.getMessage());
			}
			e.printStackTrace();
			return retJson.toString();
		}
		return retJson.toString();
	}

	@RequestMapping(value = "/AndroidKeyHashApi", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON
			+ ";charset=UTF-8")
	@ResponseBody
	public String AndroidKeyHashApi(@RequestBody String jsonString, HttpServletRequest request) throws JSONException {
		try {
			init();
			checkJSONPara(jsonString);
			String sha1 = reqJson.optString("sha1");
			String keyHash = reqJson.optString("keyHash");

			if (CommonUtils.isBlank(keyHash)) {
				retJson = CommonUtils.setResponse(false, Message.APIMSG500, "500", "file path empty");
			} else {
				// Resource resource = new ClassPathResource("MainPage.txt");
				// File filename = resource.getFile();
				String pathname = "C://usr/local/data/testApi/AndroidKeyHash.txt"; // 絕對路徑或相對路徑都可以，這裡是絕對路徑，寫入檔案時演示相對路徑
				// String pathname =
				// "/usr/local/data/testApi/AndroidKeyHash.txt"; //
				// 絕對路徑或相對路徑都可以，這裡是絕對路徑，寫入檔案時演示相對路徑
				File filename = new File(pathname); // 要讀取以上路徑的input。txt檔案
				InputStreamReader reader = new InputStreamReader(new FileInputStream(filename), "UTF-8"); // 建立一個輸入流物件reader
				BufferedReader br = new BufferedReader(reader); // 建立一個物件，它把檔案內容轉成計算機能讀懂的語言
				String line = "";
				String jsonStr = "";
				try {
					filename.createNewFile(); // 建立新檔案
					BufferedWriter out = new BufferedWriter(new FileWriter(filename));
					out.write(sha1 + "     " + keyHash); // \r\n即為換行
					out.flush(); // 把快取區內容壓入檔案
					out.close(); // 最後記得關閉檔案
				} catch (Exception e) {
					if (retJson == null || retJson.isNull("result")) {
						retJson = CommonUtils.setResponse(false, Message.APIMSG500, "500", e.getMessage());
					}
					e.printStackTrace();
					return "error";
				}
				return "success";
			}

		} catch (Exception e) {
			if (retJson == null || retJson.isNull("result")) {
				retJson = CommonUtils.setResponse(false, Message.APIMSG500, "500", e.getMessage());
			}
			e.printStackTrace();
			return retJson.toString();
		}
		return retJson.toString();
	}

	@RequestMapping(value = "/HealthCareLogApi", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON
			+ ";charset=UTF-8")
	@ResponseBody
	public String HealthCareLogApi(@RequestBody String jsonString, HttpServletRequest request) throws JSONException {
		try {
			init();
			checkJSONPara(jsonString);
			String log = reqJson.optString("log");

			if (CommonUtils.isBlank(log)) {
				retJson = CommonUtils.setResponse(false, Message.APIMSG500, "500", "file path empty");
			} else {
				// Resource resource = new ClassPathResource("MainPage.txt");
				// File filename = resource.getFile();
				// String pathname =
				// "C://usr/local/data/testApi/HealthCareLog.txt"; //
				// 絕對路徑或相對路徑都可以，這裡是絕對路徑，寫入檔案時演示相對路徑
//				String pathname = "C://usr/local/data/testApi"; // 絕對路徑或相對路徑都可以，這裡是絕對路徑，寫入檔案時演示相對路徑
				String pathname = "/usr/local/data/testApi"; // 絕對路徑或相對路徑都可以，這裡是絕對路徑，寫入檔案時演示相對路徑
				String file = "HealthCareLog.txt"; // 絕對路徑或相對路徑都可以，這裡是絕對路徑，寫入檔案時演示相對路徑
				File filePath = new File(pathname); // 要讀取以上路徑的input。txt檔案

				if (!filePath.exists()) {
					filePath.mkdir();
				}
				File filename = new File(filePath + "/" + file); // 要讀取以上路徑的input。txt檔案
				if (!filename.exists()) {
					filename.createNewFile(); // 建立新檔案
				}
				InputStreamReader reader = new InputStreamReader(new FileInputStream(filename), "UTF-8"); // 建立一個輸入流物件reader
				BufferedReader br = new BufferedReader(reader); // 建立一個物件，它把檔案內容轉成計算機能讀懂的語言
				String line = "";
				String jsonStr = "";

				try {

					StringBuilder sb = new StringBuilder();
					line = br.readLine();

					while (line != null) {
						sb.append(line);
						line = br.readLine();
					}
					jsonStr = sb.toString();
					jsonStr += log+" \n" ;
					// filename.createNewFile(); // 建立新檔案
					BufferedWriter out = new BufferedWriter(new FileWriter(filename));
					out.write(jsonStr); // \r\n即為換行
					out.newLine();
					out.flush(); // 把快取區內容壓入檔案
					out.close(); // 最後記得關閉檔案
				} catch (Exception e) {
					if (retJson == null || retJson.isNull("result")) {
						retJson = CommonUtils.setResponse(false, Message.APIMSG500, "500", e.getMessage());
					}
					e.printStackTrace();
					return "error";
				}
				return "success";
			}

		} catch (Exception e) {
			if (retJson == null || retJson.isNull("result")) {
				retJson = CommonUtils.setResponse(false, Message.APIMSG500, "500", e.getMessage());
			}
			e.printStackTrace();
			return retJson.toString();
		}
		return retJson.toString();
	}
}
