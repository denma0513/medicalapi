package com.mednet.medical.api.controller;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.MediaType;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mednet.medical.api.utils.CommonUtils;
import com.mednet.medical.api.utils.Message;

import com.mednet.medical.api.base.baseController;
import com.mednet.medical.api.dao.AIDoctorDao;
import com.mednet.medical.api.service.CustomerService;
import com.mednet.medical.api.service.AIDoctorService;

@Controller
@RequestMapping("/AIdoctor")
public class AIDoctorController extends baseController {
	@Autowired
	AIDoctorDao AIDoctorDao;
	@Autowired
	AIDoctorService aIDoctorService;

	private Log log = LogFactory.getLog("Controller");
	@RequestMapping(value = "/getSymptoms", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON
			+ ";charset=UTF-8")
	@ResponseBody
	public String getSymptoms(@RequestBody String jsonString, HttpServletRequest request) throws JSONException {
		try {
			init();
			checkJSONPara(jsonString);
			if (CommonUtils.isBlank(reqJson.optString("words"))) {
				retJson = CommonUtils.setResponse(false, "call api fail!", "500", "words can't be null");
			} else {
				String message = "";
				String words = reqJson.optString("words");
				String resultId = reqJson.optString("resultId");
				String customerId = reqJson.optString("customerId");
				String age = reqJson.optString("age");
				String gender = reqJson.optString("gender");
				JSONObject apiRetrunObj = aIDoctorService.getContentForSymptoms(words);
				if (apiRetrunObj.optInt("code") != 200) {
					message = apiRetrunObj.optString("message");
					retJson = CommonUtils.setResponse(true, Message.APIMSG500, "500", apiRetrunObj.toString());
				} else if (apiRetrunObj.getJSONObject("data") == null) {
					message = "識別失敗";
					retJson = CommonUtils.setResponse(true, Message.APIMSG500, "500", message);
				} else {
					JSONObject apiReturnData = apiRetrunObj.getJSONObject("data");
					if (apiReturnData.getJSONArray("symptom_list") == null
							|| apiReturnData.getJSONArray("symptom_list").length() == 0) {
						message = "識別失敗";
					}
					JSONObject returnJson = new JSONObject();
					DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
					LocalDateTime now = LocalDateTime.now();
					String date = dtf.format(now);
					List<HashMap<String, Object>> list = AIDoctorDao.saveResultData(customerId, null, null, null, null,
							null, words, age, gender, date, date);
					HashMap<String, Object> object = list.get(0);
					resultId = object.get("result_id").toString();
					returnJson.put("resultId", resultId);

					if (CommonUtils.isBlank(message)) {
						JSONArray symptomJSONArray = apiReturnData.getJSONArray("symptom_list");
						List<String> symptomList = new ArrayList<String>();
						for (int i = 0; i < symptomJSONArray.length(); i++) {
							JSONObject symptom = symptomJSONArray.getJSONObject(i);
							symptomList.add(symptom.getString("Word"));
						}
						List<HashMap<String, Object>> quesyResultList = AIDoctorDao.getSymptom(symptomList);
						if (quesyResultList ==null || quesyResultList.size()<=0) {
							message = "識別失敗";
							retJson = CommonUtils.setResponse(false, Message.APIMSG500, "500", message);
							return retJson.toString();
						}
						JSONArray symptoms = new JSONArray();
						for (int i = 0; i < quesyResultList.size(); i++) {
							HashMap<String, Object> quesyResultObj = quesyResultList.get(i);
							JSONObject jsonAdd = new JSONObject();
							jsonAdd.put("symptom_id", quesyResultObj.get("symptom_id"));
							jsonAdd.put("symptom_content", quesyResultObj.get("symptom_content"));
							jsonAdd.put("question_id", quesyResultObj.get("question_id"));
							symptoms.put(jsonAdd);
						}
						returnJson.put("words", words);
						returnJson.put("symptoms", symptoms);
						retJson = CommonUtils.setResponse(true, Message.APIMSG200, "200", message, returnJson);
					} else {
						retJson = CommonUtils.setResponse(false, Message.APIMSG500, "500", message);
					}
				}
			}
		} catch (Exception e) {
			if (retJson == null || retJson.isNull("result")) {
				retJson = CommonUtils.setResponse(false, Message.APIMSG500, "500", e.getMessage());
			}
			e.printStackTrace();
			return retJson.toString();
		}
		return retJson.toString();
	}

	@RequestMapping(value = "/getCustomerData", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON
			+ ";charset=UTF-8")
	@ResponseBody
	public String getCustomerData(@RequestBody String jsonString, HttpServletRequest request) throws JSONException {
		try {
			init();
			checkJSONPara(jsonString);
			String customerId = reqJson.optString("customerId");
			JSONObject resultJson = aIDoctorService.MednetApiGetUserProfileByCID(customerId);
			if (resultJson != null) {
				JSONObject data = resultJson.getJSONObject("data");
				JSONObject profile = data.getJSONObject("profile");
				String image = profile.optString("profile_image");
				String birth = profile.optString("birthday");
				String name = profile.optString("name");
				String firstName = profile.optString("first_name");
				String lastName = profile.optString("last_name");
				int age = 30; //預設值
				if (!CommonUtils.isBlank(birth)) {
					String[] birthArray = birth.split("-");
					age = calculateAge(LocalDate.of(Integer.valueOf(birthArray[0]), Integer.valueOf(birthArray[1]),
							Integer.valueOf(birthArray[2])), LocalDate.now());
				}
				int gender = profile.optInt("gender",1);
				JSONObject returnJson = new JSONObject();
				returnJson.put("age", Integer.toString(age));
				returnJson.put("gender", Integer.toString(gender));
				if (image != null) {
					returnJson.put("photo", image);
				}
				else {
					returnJson.put("photo", "");
				}
				
				if (firstName != null && lastName != null && firstName != "" && lastName != "") {
					returnJson.put("name", lastName + firstName );
				}
				else if (name != null) {
					returnJson.put("name", name);
				}
				else {
					returnJson.put("name", "");
				}
				
				String message = "";
				if (age == 0) {
					message = "使用者未登錄生日";
				}
				retJson = CommonUtils.setResponse(true, Message.APIMSG200, "200", message, returnJson);
			} else {
				retJson = CommonUtils.setResponse(false, Message.APIMSG200, "200", "無法取得資料");
			}

		} catch (Exception e) {
			if (retJson == null || retJson.isNull("result")) {
				retJson = CommonUtils.setResponse(false, Message.APIMSG500, "500", e.getMessage());
			}
			e.printStackTrace();
			return retJson.toString();
		}
		return retJson.toString();
	}

	@RequestMapping(value = "/saveResultData", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON
			+ ";charset=UTF-8")
	@ResponseBody
	public String saveResultData(@RequestBody String jsonString, HttpServletRequest request) throws JSONException {
		try {
			init();
			checkJSONPara(jsonString);
			if (CommonUtils.isBlank(reqJson.optString("resultId"))) {
				retJson = CommonUtils.setResponse(false, "call api fail!", "500", "resultId can't be null");
			} else {
				String customerId = reqJson.optString("customerId");
				String resultId = reqJson.optString("resultId");
				String targetId = reqJson.optString("targetId");
				JSONArray symptoms = reqJson.optJSONArray("symptoms");
				String symptomsString = null;
				if (symptoms != null && symptoms.length() > 0) {
					symptomsString = symptoms.toString();
				}

				JSONArray urgentQuestion = reqJson.optJSONArray("urgentQuestion");
				String urgentQuestionString = null;
				if (urgentQuestion != null && urgentQuestion.length() > 0) {
					urgentQuestionString = urgentQuestion.toString();
				}
				
				JSONArray generalQuestion = reqJson.optJSONArray("generalQuestion");
				String generalQuestionString = null;
				if (generalQuestion != null && generalQuestion.length() > 0) {
					generalQuestionString = generalQuestion.toString();
				}
				
				JSONArray mergeQuestion = reqJson.optJSONArray("mergeQuestion");
				String mergeQuestionString = null;
				if (mergeQuestion != null && mergeQuestion.length() > 0) {
					mergeQuestionString = mergeQuestion.toString();
				}
				
				retJson = CommonUtils.setResponse(true, Message.APIMSG200, "200", "");
				DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
				LocalDateTime now = LocalDateTime.now();
				String date = dtf.format(now);
				int updateResult = AIDoctorDao.updateResultData(resultId, symptomsString, urgentQuestionString,
						generalQuestionString, mergeQuestionString, targetId, null, date);
				if (updateResult > 0) {
					retJson = CommonUtils.setResponse(true, Message.APIMSG200, "200", "儲存成功");
				} else {
					retJson = CommonUtils.setResponse(true, Message.APIMSG500, "500", "儲存失敗");
					log.error(reqJson);
				}
			}
		} catch (Exception e) {
			if (retJson == null || retJson.isNull("result")) {
				retJson = CommonUtils.setResponse(false, Message.APIMSG500, "500", e.getMessage());
			}
			e.printStackTrace();
			return retJson.toString();
		}
		return retJson.toString();
	}

	@RequestMapping(value = "/setResultToEnable", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON
			+ ";charset=UTF-8")
	@ResponseBody
	public String setResultToEnable(@RequestBody String jsonString, HttpServletRequest request) throws JSONException {
		try {
			init();
			checkJSONPara(jsonString);
			if (CommonUtils.isBlank(reqJson.optString("resultId"))) {
				retJson = CommonUtils.setResponse(false, "call api fail!", "500", "resultId can't be null");
			} else {
				String resultId = reqJson.optString("resultId");
				int result = AIDoctorDao.setResultToEnable(resultId);
				if (result > 0) {
					retJson = CommonUtils.setResponse(true, Message.APIMSG200, "200", "");
				} else {
					retJson = CommonUtils.setResponse(true, Message.APIMSG500, "500", "儲存失敗");
				}
			}
		} catch (Exception e) {
			if (retJson == null || retJson.isNull("result")) {
				retJson = CommonUtils.setResponse(false, Message.APIMSG500, "500", e.getMessage());
			}
			e.printStackTrace();
			return retJson.toString();
		}
		return retJson.toString();
	}

	@RequestMapping(value = "/getResultList", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON
			+ ";charset=UTF-8")
	@ResponseBody
	public String getResultList(@RequestBody String jsonString, HttpServletRequest request) throws JSONException {
		try {
			init();
			checkJSONPara(jsonString);
			if (CommonUtils.isBlank(reqJson.optString("customerId"))) {
				retJson = CommonUtils.setResponse(false, "call api fail!", "500", "customerId can't be null");
			} else {
				String customerId = reqJson.optString("customerId");
				List<HashMap<String, Object>> resultList = AIDoctorDao.getResultList(customerId);
				JSONArray resultArray = new JSONArray();
				for (int i = 0; i < resultList.size(); i++) {
					HashMap<String, Object> resultHashMap = resultList.get(i);
					JSONArray symptomArray = new JSONArray(resultHashMap.get("symptoms").toString());
					List<String> symptomIdList = new ArrayList<String>();
					for (int k = 0; k < symptomArray.length(); k++) {
						JSONObject symptom = symptomArray.getJSONObject(k);
						symptomIdList.add(symptom.getString("symptomsId"));
					}
					List<HashMap<String, Object>> symptomQueryresultList = AIDoctorDao.getSymptomName(symptomIdList);
					String symptomName = "";
					for (int k = 0; k < symptomQueryresultList.size(); k++) {
						HashMap<String, Object> object = symptomQueryresultList.get(k);
						if (symptomName == "") {
							symptomName += object.get("symptom_content");
						} else {
							symptomName += "," + object.get("symptom_content");
						}
					}
					String date = resultHashMap.get("date").toString();
					String resultId = resultHashMap.get("result_id").toString();
					JSONObject returnJson = new JSONObject();
					returnJson.put("symptomName", symptomName);
					returnJson.put("date", date);
					returnJson.put("resultId", resultId);
					resultArray.put(returnJson);
				}
				retJson = CommonUtils.setResponse(true, Message.APIMSG200, "200", "", resultArray);
			}

		} catch (Exception e) {
			if (retJson == null || retJson.isNull("result")) {
				retJson = CommonUtils.setResponse(false, Message.APIMSG500, "500", e.getMessage());
			}
			e.printStackTrace();
			return retJson.toString();
		}
		return retJson.toString();
	}

	@RequestMapping(value = "/getResult", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON
			+ ";charset=UTF-8")
	@ResponseBody
	public String getResult(@RequestBody String jsonString, HttpServletRequest request) throws JSONException {
		try {
			init();
			checkJSONPara(jsonString);
			if (CommonUtils.isBlank(reqJson.optString("customerId"))) {
				retJson = CommonUtils.setResponse(false, "call api fail!", "500", "customerId can't be null");
			} else if (CommonUtils.isBlank(reqJson.optString("resultId"))) {
				retJson = CommonUtils.setResponse(false, "call api fail!", "500", "resultId can't be null");
			} else {
				String customerId = reqJson.optString("customerId");
				String resultId = reqJson.optString("resultId");
				HashMap<String, Object> resultObj = AIDoctorDao.getResult(resultId);
				if (resultObj == null) {
					retJson = CommonUtils.setResponse(false, "call api fail!", "500", "resultId illegal");
				} else if (!customerId.equals(resultObj.get("customerId").toString())) {
					retJson = CommonUtils.setResponse(false, "call api fail!", "500", "customerId illegal");
				} else {
					JSONObject returnJson = new JSONObject();
					int resultType = 0;
					// 塞進去reultType 警示結果 = 1 ,正常結果 =2 ,建議結果 = 3
					if (resultObj.get("MergeQuestion") == null) {
						returnJson.put("resultType", "1");
						resultType = 1;
					} else if (Integer.valueOf(resultObj.get("end").toString()) == 1) {
						returnJson.put("resultType", "3");
						resultType = 3;
					} else {
						returnJson.put("resultType", "2");
						resultType = 2;
					}
					returnJson.put("suspectDisease", resultObj.get("target_title"));
					returnJson.put("diseaseDiscription", resultObj.get("target_content"));
					returnJson.put("diseaseSuggestion", resultObj.get("target_suggest"));
					returnJson.put("customerInput", resultObj.get("customer_input"));
					returnJson.put("updateTime", resultObj.get("updateTime"));
					returnJson.put("mergeContent", resultObj.get("merge_content"));
					// 撈症狀名稱
					JSONArray symptomArray = new JSONArray(resultObj.get("symptoms").toString());
					List<String> symptomIdList = new ArrayList<String>();
					for (int k = 0; k < symptomArray.length(); k++) {
						JSONObject symptom = symptomArray.getJSONObject(k);
						symptomIdList.add(symptom.get("symptomsId").toString());
					}
					symptomArray = new JSONArray();
					List<HashMap<String, Object>> symptomQueryresultList = AIDoctorDao.getSymptomName(symptomIdList);
					for (int i = 0; i < symptomQueryresultList.size(); i++) {
						HashMap<String, Object> object = symptomQueryresultList.get(i);
						JSONObject JSONObjectToAdd = new JSONObject();
						JSONObjectToAdd.put("symptomId", object.get("symptom_id"));
						JSONObjectToAdd.put("symptomName", object.get("symptom_content"));
						symptomArray.put(JSONObjectToAdd);
					}
					returnJson.put("symptoms", symptomArray);

					// 撈基礎問題
					if (resultType != 1) {
						JSONArray generalQuestionArray  = new JSONArray();
						if (resultObj.get("generalQuestion") != null) {
							generalQuestionArray  = new JSONArray(resultObj.get("generalQuestion").toString());
						}
						JSONArray resultGeneralQuestionArray = new JSONArray();
						for (int i = 0; i < generalQuestionArray.length(); i++) {
							JSONObject JSONObjGeneralQuestion = (JSONObject) generalQuestionArray.get(i);
							JSONArray generalQuestionDetailArray = new JSONArray(
									JSONObjGeneralQuestion.get("questionContent").toString());
							JSONArray resultGeneralQuestionDetailArray = new JSONArray();
							for (int k = 0; k < generalQuestionDetailArray.length(); k++) {
								JSONObject JSONObjGeneralQuestionDetail = (JSONObject) generalQuestionDetailArray
										.get(k);
								HashMap<String, Object> object = AIDoctorDao.getQuestionNameAndOptionName(
										JSONObjGeneralQuestionDetail.get("optionId").toString(),
										JSONObjGeneralQuestionDetail.get("questionId").toString());
								if (object != null) {
									JSONObjGeneralQuestionDetail.put("questionName", object.get("question_content"));
									JSONObjGeneralQuestionDetail.put("optionName", object.get("option_content"));
								}
								resultGeneralQuestionDetailArray.put(JSONObjGeneralQuestionDetail);
							}
							JSONObjGeneralQuestion.put("questionContent", resultGeneralQuestionDetailArray);
							resultGeneralQuestionArray.put(JSONObjGeneralQuestion);
						}
						returnJson.put("generalQuestionArray", resultGeneralQuestionArray);
					}

					//取得醫師
					String[] divisionData = resultObj.get("target_outpatient").toString().split(",");
//					System.out.print(resultObj.get("target_outpatient").toString());
					List<HashMap<String, Object>> divisionNameData = AIDoctorDao.getDivisionId(divisionData);
//					System.out.print(divisionNameData);
					List<String> divisionIdData = new ArrayList<String>();
					JSONArray  divisionIdDataJsonArray = new JSONArray();
					for (int i = 0; i < divisionData.length; i++) {
						for (int k = 0; k < divisionNameData.size(); k++) {
							if (divisionData[i].toString().equals(divisionNameData.get(k).get("Name").toString())) {
								divisionIdData.add(divisionNameData.get(k).get("ID").toString());
								JSONObject obj = new JSONObject();
								obj.put("divisionId", divisionNameData.get(k).get("ID").toString());
								obj.put("divisionName", divisionNameData.get(k).get("Name").toString());
								List<HashMap<String, Object>> divisionDesciptionData = AIDoctorDao.getDivisionDescription(divisionNameData.get(k).get("Name").toString());
								String des = "";
								for (int j = 0 ; j < divisionDesciptionData.size() ; j++) {
									HashMap<String, Object> hash = divisionDesciptionData.get(j);
									if (des == "") {
										des += hash.get("Name").toString();
									}
									else {
										des +=  ","+hash.get("Name").toString();
									}
									if (j == 2) {
										break;
									}
								}
								obj.put("description", des);
								divisionIdDataJsonArray.put(obj);
								break;
							}
						}
					}
					JSONObject doctors = getDoctors(divisionIdData, new JSONArray(), "1");
					returnJson.put("doctors", doctors);
					returnJson.put("division", divisionIdDataJsonArray);
					//取得套組
					String[] functionData = resultObj.get("target_examination").toString().split(",");
					List<HashMap<String, Object>> functionNameData = AIDoctorDao.getFuntionId(functionData);
					JSONArray functionIdJsonArray = new JSONArray();
					for (int i = 0 ; i < functionNameData.size() ; i++) {
						JSONObject obj = new JSONObject();
						obj.put("exam_function_id", functionNameData.get(i).get("GUID").toString());
						functionIdJsonArray.put(obj);
					}
					JSONObject functionRetuenObj = aIDoctorService.getProductss(functionIdJsonArray, new JSONArray(), "0", "1000000", "1", "2");
					if (functionRetuenObj.has("data")) {
						JSONObject functionReturnData  = functionRetuenObj.getJSONObject("data");
						if (functionReturnData.has("exam_list")) {
							returnJson.put("function", functionReturnData.getJSONArray("exam_list"));
							returnJson.put("functionIdList", functionIdJsonArray);
						}
					}
					
					retJson = CommonUtils.setResponse(true, Message.APIMSG200, "200", "", returnJson);
				}

			}

		} catch (Exception e) {
			if (retJson == null || retJson.isNull("result")) {
				retJson = CommonUtils.setResponse(false, Message.APIMSG500, "500", e.getMessage());
			}
			e.printStackTrace();
			return retJson.toString();
		}
		return retJson.toString();
	}
	
	
	@RequestMapping(value = "/getProducts", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON
			+ ";charset=UTF-8")
	@ResponseBody
	public String getProducts(@RequestBody String jsonString, HttpServletRequest request) throws JSONException {
		try {
			init();
			checkJSONPara(jsonString);
			if (reqJson.getJSONArray("functionIdList") == null || reqJson.getJSONArray("functionIdList").length() == 0) {
				retJson = CommonUtils.setResponse(false, "call api fail!", "500", "customerId can't be null");
			} else {
				String upperPrice = reqJson.optString("upperPrice");
				String lowerPrice = reqJson.optString("lowerPrice");
				String orderType = reqJson.optString("orderType");
				String page = reqJson.optString("page");
				JSONArray functionIdList =  reqJson.getJSONArray("functionIdList");
				JSONArray cityNameJsonArray =  reqJson.getJSONArray("cityList");
				List<String> cityNameList = new ArrayList<String> ();
				for (int i = 0 ; i < cityNameJsonArray.length() ; i ++) {
					JSONObject obj = cityNameJsonArray.getJSONObject(i);
					cityNameList.add(obj.optString("cityName"));
				}
				JSONArray cityIdJsonArray = new JSONArray();
				List<HashMap<String, Object>> queryResult = AIDoctorDao.getCityGuid(cityNameList);
				for (int i = 0 ; i < queryResult.size() ; i++) {
					HashMap<String, Object> obj = queryResult.get(i);
					JSONObject json = new JSONObject();
					json.put("city_sort", obj.get("GUID").toString());
					cityIdJsonArray.put(json);
				}
				JSONObject functionRetuenObj = aIDoctorService.getProductss(functionIdList, cityIdJsonArray, lowerPrice, upperPrice, page, orderType);
				retJson = CommonUtils.setResponse(true, Message.APIMSG200, "200", "", functionRetuenObj.optJSONObject("data"));
			}

		} catch (Exception e) {
			if (retJson == null || retJson.isNull("result")) {
				retJson = CommonUtils.setResponse(false, Message.APIMSG500, "500", e.getMessage());
			}
			e.printStackTrace();
			return retJson.toString();
		}
		return retJson.toString();
	}
	
	@RequestMapping(value = "/getDoctors", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON
			+ ";charset=UTF-8")
	@ResponseBody
	public String getMoreDoctors(@RequestBody String jsonString, HttpServletRequest request) throws JSONException {
		try {
			init();
			checkJSONPara(jsonString);
			if (CommonUtils.isBlank(reqJson.optString("division"))) {
				retJson = CommonUtils.setResponse(false, "call api fail!", "500", "division can't be null");
			} else {
				String division = reqJson.optString("division");
				String orderType = reqJson.optString("orderType");
				JSONArray cityNameJsonArray =  reqJson.getJSONArray("cityList");
				List<String> cityNameList = new ArrayList<String> ();
				for (int i = 0 ; i < cityNameJsonArray.length() ; i ++) {
					JSONObject obj = cityNameJsonArray.getJSONObject(i);
					cityNameList.add(obj.optString("cityName"));
				}
				JSONArray cityIdJsonArray = new JSONArray();
				List<HashMap<String, Object>> queryResult = AIDoctorDao.getDoctorCityId(cityNameList);
				for (int i = 0 ; i < queryResult.size() ; i++) {
					HashMap<String, Object> obj = queryResult.get(i);
					JSONObject json = new JSONObject();
					json.put("country", obj.get("Value").toString());
					cityIdJsonArray.put(json);
				}
				JSONObject functionRetuenObj = aIDoctorService.getDoctors(division, cityIdJsonArray, orderType);
				retJson = CommonUtils.setResponse(true, Message.APIMSG200, "200", "", functionRetuenObj.optJSONObject("data"));
			}

		} catch (Exception e) {
			if (retJson == null || retJson.isNull("result")) {
				retJson = CommonUtils.setResponse(false, Message.APIMSG500, "500", e.getMessage());
			}
			e.printStackTrace();
			return retJson.toString();
		}
		return retJson.toString();
	}
	

	public JSONObject getDoctors(List<String> divisions, JSONArray countryList, String orderType) {
		List<HashMap<String, Object>> regionCountryList = AIDoctorDao.getRegionCountry();
		HashMap<String, Object> resultHashMap = new HashMap<String, Object>();
		for (int i = 0; i < divisions.size(); i++) {
			String divisionId = divisions.get(i).toString();
			JSONObject apiRetrunObj = aIDoctorService.getDoctors(divisionId, countryList, orderType);
			if (apiRetrunObj.optJSONObject("data") != null
					&& (apiRetrunObj.optJSONObject("data").optJSONArray("doctorList")) != null
					&& (apiRetrunObj.optJSONObject("data").optJSONArray("doctorList").length() > 0)) {
				JSONArray doctors = apiRetrunObj.optJSONObject("data").optJSONArray("doctorList");
				List<Object> doctorsList = doctors.toList();
				for (int k = 0; k < doctorsList.size(); k++) {
					HashMap<String, Object> newData = setRegionForDoctor((HashMap<String, Object>) doctorsList.get(k),
							regionCountryList);
					if (resultHashMap.get(newData.get("regionName").toString()) == null) {
						List<HashMap<String, Object>> newList = new ArrayList<HashMap<String, Object>>();
						newList.add(newData);
						resultHashMap.put(newData.get("regionName").toString(), newList);
					} else {
						List<HashMap<String, Object>> existList = (List<HashMap<String, Object>>) resultHashMap
								.get(newData.get("regionName").toString());

						if (existList.size() > 1) {
							for (int j = 1; j < existList.size(); j++) {
								HashMap<String, Object> oldData = (HashMap<String, Object>) existList.get(j);
								// 熱門排序
								if (orderType == "1" && Integer.valueOf(oldData.get("popularity").toString()) < Integer
										.valueOf(newData.get("popularity").toString())) {
									existList.add(j, newData);
									break;
								} else if (orderType == "2"
										&& Integer.valueOf(oldData.get("grade").toString()) > Integer
												.valueOf(newData.get("grade").toString())) {
									existList.add(j, newData);
									break;
								} else if (oldData.get("outpatient") != oldData.get("outpatient").toString()) {
									existList.add(j, newData);
									break;
								} else if (j == existList.size() - 1) {
									existList.add(j, newData);
									break;
								}
							}
						} else {
							existList.add(newData);
						}

						resultHashMap.put(newData.get("regionName").toString(), existList);
					}
				}
			}
		}

		return new JSONObject(resultHashMap);
	}
	
	

	public HashMap<String, Object> setRegionForDoctor(HashMap<String, Object> doctordata,
			List<HashMap<String, Object>> regionCountryList) {
		for (int i = 0; i < regionCountryList.size(); i++) {
			if (doctordata.get("area").toString().equals(regionCountryList.get(i).get("CodeName").toString())) {
				doctordata.put("regionName", regionCountryList.get(i).get("RegionName").toString());
				doctordata.put("regionId", regionCountryList.get(i).get("RegionID").toString());
				break;
			}
		}
		return doctordata;
	}

	public static int calculateAge(LocalDate birthDate, LocalDate currentDate) {
		if ((birthDate != null) && (currentDate != null)) {
			return Period.between(birthDate, currentDate).getYears();
		} else {
			return 0;
		}
	}
}
