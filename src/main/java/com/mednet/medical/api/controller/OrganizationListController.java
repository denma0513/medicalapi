package com.mednet.medical.api.controller;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.MediaType;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mednet.medical.api.base.baseController;
import com.mednet.medical.api.utils.CommonUtils;
import com.mednet.medical.api.utils.Message;
import com.mednet.medical.api.utils.TimerUtils;

@Controller
// @RequestMapping("/OrganizationListC")
public class OrganizationListController extends baseController {
	private Log log = LogFactory.getLog("Controller");

	@RequestMapping(value = "/OrganizationList", produces = MediaType.APPLICATION_JSON + ";charset=UTF-8")
	@ResponseBody
	public String OrganizationList(@RequestBody String jsonString, HttpServletRequest request) throws JSONException {
		
		try {
			init () ;
			JSONObject retJson;
			checkJSONPara(jsonString);
			String version = reqJson.optString("version");
			String device = reqJson.optString("device");
			if (device == null || CommonUtils.isBlank(device)) {
				device = "ios";
			}
			else if ("2".equals(device)) {
				device = "android";
			}
			else if ("1".equals(device)) {
				device = "ios";
			}
			
			boolean testMode = false;
			if (version != null && !CommonUtils.isBlank(version)) {
				String pathname = "C://usr/local/data/versionVerify.txt"; // 絕對路徑或相對路徑都可以，這裡是絕對路徑，寫入檔案時演示相對路徑
//				String pathname = "data/versionVerify.txt"; // 絕對路徑或相對路徑都可以，這裡是絕對路徑，寫入檔案時演示相對路徑
				File filename = new File(pathname); // 要讀取以上路徑的input。txt檔案
				
				InputStreamReader reader;
				reader = new InputStreamReader(new FileInputStream(filename));
				BufferedReader br = new BufferedReader(reader); // 建立一個物件，它把檔案內容轉成計算機能讀懂的語言
				String line = "";
				String versionStr = "";
				while ((line = br.readLine()) != null) {
					versionStr += line;
				}
				
				int i = versionStr.indexOf("{");
				versionStr = versionStr.substring(i);
				JSONObject versionObj = new JSONObject(versionStr);

				if (versionObj.opt(device).toString().equals(version)) {
					testMode = true;
				}
			}
			
			JSONArray jsonArray = new JSONArray();
			for (int i = 0;; i++) {
				boolean finish = false;
				JSONObject dataObj = new JSONObject();
				switch (i) {
				case 0:
					if (testMode) {
						dataObj.put("Name", "眾匯智能健檢");
						dataObj.put("NameEn", "TestEnvirment 82");
						dataObj.put("urlPrifix", "http://survey.med-net.com:82");
						dataObj.put("orgID", "828F5FAC-C7F3-471B-AEBB-F82139781086");
						dataObj.put("orgMap", "http://survey.med-net.com:82/Content/images/orgMap/map82.png");
						dataObj.put("ratingUrl", "https://med-net.com/Channel/hvc?ModelType=OwnExpense");
						finish = true;
						jsonArray.put(dataObj);
						break;
					}
					else {
						dataObj.put("Name", "柏忕健康管理中心");
						dataObj.put("NameEn", "EXECUTIVE HEALTH MANAGEMENT CENTER");
						dataObj.put("urlPrifix", "http://34.80.52.49:81");
						dataObj.put("orgID", "828F5FAC-C7F3-471B-AEBB-F82139781086");
						dataObj.put("orgMap", "http://34.80.52.49:81/Content/images/orgMap/map_executive_health.png");
						dataObj.put("ratingUrl", "https://med-net.com/Channel/bthealthtc?ModelType=OwnExpense");
						break;
					}
//				case 1:
//					dataObj.put("Name", "博仁綜合醫院");
//					dataObj.put("NameEn", "Pojen General Hospital");
//					dataObj.put("urlPrifix", "http://35.221.203.206:81");
//					dataObj.put("orgID", "edcf0770-bb56-440d-945e-ede6dc3e59aa");
//					dataObj.put("orgMap", "http://35.221.203.206:81/Content/images/orgMap/map81.png");
//					dataObj.put("ratingUrl", "https://med-net.com/Channel/pojengh?ModelType=OwnExpense");
//					break;
//				case 2:
//					dataObj.put("Name", "聖保祿醫院");
//					dataObj.put("NameEn", "Saint Paul's Hospital");
//					dataObj.put("urlPrifix", "http://35.221.203.206:82");
//					dataObj.put("orgID", "edcf0770-bb56-440d-945e-ede6dc3e59aa");
//					dataObj.put("orgMap", "http://35.221.203.206:82/Content/images/orgMap/map82.png");
//					dataObj.put("ratingUrl", "https://med-net.com/Channel/sph?ModelType=OwnExpense");
//					break;
				default: 
					finish = true;
					break;
				}
				if (finish) {
					break;
				} else {
					jsonArray.put(dataObj);
				}
			}
			retJson = CommonUtils.setResponse(true, Message.APIMSG200, "200", "", jsonArray);
			return retJson.toString();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			if (retJson == null || retJson.isNull("result")) {
				retJson = CommonUtils.setResponse(false, Message.APIMSG500, "500", e.getMessage());
			}
			
		} // 建立一個輸入流物件reader
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			if (retJson == null || retJson.isNull("result")) {
				retJson = CommonUtils.setResponse(false, Message.APIMSG500, "500", e.getMessage());
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return retJson.toString();
	}
	
	
	@RequestMapping(value = "/OrganizationListV2", produces = MediaType.APPLICATION_JSON + ";charset=UTF-8")
	@ResponseBody
	public String OrganizationListV2(@RequestBody String jsonString, HttpServletRequest request) throws JSONException {
		try {
			init () ;
			checkJSONPara(jsonString);
			String version = reqJson.optString("version");
			String device = reqJson.optString("device");
			if (device == null || CommonUtils.isBlank(device)) {
				device = "ios";
			}
			else if ("2".equals(device)) {
				device = "android";
			}
			else if ("1".equals(device)) {
				device = "ios";
			}
			
			boolean testMode = false;
			if (version != null && !CommonUtils.isBlank(version)) {
				String pathname = "C://usr/local/data/versionVerify.txt"; // 絕對路徑或相對路徑都可以，這裡是絕對路徑，寫入檔案時演示相對路徑
//				String pathname = "data/versionVerify.txt"; // 絕對路徑或相對路徑都可以，這裡是絕對路徑，寫入檔案時演示相對路徑
				File filename = new File(pathname); // 要讀取以上路徑的input。txt檔案
				
				InputStreamReader reader;
				reader = new InputStreamReader(new FileInputStream(filename),"UTF-8");
				BufferedReader br = new BufferedReader(reader); // 建立一個物件，它把檔案內容轉成計算機能讀懂的語言
				String line = "";
				String versionStr = "";
				while ((line = br.readLine()) != null) {
					versionStr += line;
				}
				
				int i = versionStr.indexOf("{");
				versionStr = versionStr.substring(i);
				JSONObject versionObj = new JSONObject(versionStr);

				if (versionObj.opt(device).toString().equals(version)) {
					testMode = true;
				}
			}
			
			String pathname = "C://usr/local/data/" + (testMode ? "organizationList_test" : "organizationList") + ".txt"; // 絕對路徑或相對路徑都可以，這裡是絕對路徑，寫入檔案時演示相對路徑
//			String pathname = "data/" + (testMode ? "organizationList_test" : "organizationList") + ".txt" ;
			File filename = new File(pathname); // 要讀取以上路徑的input。txt檔案
			InputStreamReader reader = new InputStreamReader(new FileInputStream(filename),"UTF-8"); // 建立一個輸入流物件reader
			BufferedReader br = new BufferedReader(reader); // 建立一個物件，它把檔案內容轉成計算機能讀懂的語言
			String line = "";
			String jsonStr = "";
			try {
			    StringBuilder sb = new StringBuilder();
			    line = br.readLine();
			    
			    while (line != null) {
			        sb.append(line);
			        line = br.readLine();
			    }
			    jsonStr = sb.toString();
			} finally {
			    reader.close();
			}
			jsonStr = jsonStr.replaceAll(" ", "");
			
			JSONObject jsonObj =  CommonUtils.parseJson(jsonStr);
			
			retJson = jsonObj;

		} catch (Exception e) {
			if (retJson == null || retJson.isNull("result")) {
				retJson = CommonUtils.setResponse(false, Message.APIMSG500, "500", e.getMessage());
			}
			e.printStackTrace();
			return retJson.toString();
		}
		return retJson.toString();
	}
	
	
	@RequestMapping(value = "/OrganizationListTest", produces = MediaType.APPLICATION_JSON + ";charset=UTF-8")
	@ResponseBody
	public String OrganizationListTest(@RequestBody String jsonString, HttpServletRequest request) throws JSONException {
		try {
			init () ;
			checkJSONPara(jsonString);
			
			String pathname = "C://usr/local/data/organizationList_test.txt"; // 絕對路徑或相對路徑都可以，這裡是絕對路徑，寫入檔案時演示相對路徑
//			String pathname = "data/" + (testMode ? "organizationList_test" : "organizationList") + ".txt" ;
			File filename = new File(pathname); // 要讀取以上路徑的input。txt檔案
			InputStreamReader reader = new InputStreamReader(new FileInputStream(filename),"UTF-8"); // 建立一個輸入流物件reader
			BufferedReader br = new BufferedReader(reader); // 建立一個物件，它把檔案內容轉成計算機能讀懂的語言
			String line = "";
			String jsonStr = "";
			try {
			    StringBuilder sb = new StringBuilder();
			    line = br.readLine();
			    
			    while (line != null) {
			        sb.append(line);
			        line = br.readLine();
			    }
			    jsonStr = sb.toString();
			} finally {
			    reader.close();
			}
			jsonStr = jsonStr.replaceAll(" ", "");
			
			JSONObject jsonObj =  CommonUtils.parseJson(jsonStr);
			
			retJson = jsonObj;

		} catch (Exception e) {
			if (retJson == null || retJson.isNull("result")) {
				retJson = CommonUtils.setResponse(false, Message.APIMSG500, "500", e.getMessage());
			}
			e.printStackTrace();
			return retJson.toString();
		}
		return retJson.toString();
	}
	
	@RequestMapping(value = "/GetAppVersion", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON
			+ ";charset=UTF-8")
	@ResponseBody
	public String getAppVersion() throws JSONException {
		try {
			init () ;
			String pathname = "C://usr/local/data/version.txt"; // 絕對路徑或相對路徑都可以，這裡是絕對路徑，寫入檔案時演示相對路徑
//			String pathname = "data/version.txt"; // 絕對路徑或相對路徑都可以，這裡是絕對路徑，寫入檔案時演示相對路徑
			File filename = new File(pathname); // 要讀取以上路徑的input。txt檔案
			System.out.println(filename.exists());
			InputStreamReader reader = new InputStreamReader(new FileInputStream(filename)); // 建立一個輸入流物件reader
			BufferedReader br = new BufferedReader(reader); // 建立一個物件，它把檔案內容轉成計算機能讀懂的語言
			String line = "";
			String versionStr = "";
			while ((line = br.readLine()) != null) {
				versionStr += line;
			}
			int i = versionStr.indexOf("{");
			versionStr = versionStr.substring(i);
			JSONObject versionObj = new JSONObject(versionStr);

			retJson = CommonUtils.setResponse(true, Message.APIMSG200, "200", "", versionObj);

		} catch (Exception e) {
			if (retJson == null || retJson.isNull("result")) {
				retJson = CommonUtils.setResponse(false, Message.APIMSG500, "500", e.getMessage());
			}
			e.printStackTrace();
			log.error(e.getStackTrace());
			return retJson.toString();
		}
		return retJson.toString();
	}

	@RequestMapping(value = "/AppVersion", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON
			+ ";charset=UTF-8")
	@ResponseBody
	public String appVersion(@RequestBody String jsonString, HttpServletRequest request) throws JSONException {
		try {
			init () ;
			checkJSONPara(jsonString);
			String device = reqJson.optString("device").equals("1") ? "ios"
					: reqJson.optString("device").equals("2") ? "android" : "";
			String version = reqJson.optString("version");
			String url = reqJson.optString("device").equals("1") ? "https://apps.apple.com/tw/app/%E6%99%BA%E6%85%A7%E5%81%A5%E8%A8%BA/id1459458752"
					: reqJson.optString("device").equals("2") ? "https://play.google.com/store/apps/details?id=com.mgear.mednetapp" : "";

			String pathname = "C://usr/local/data/version.txt"; // 絕對路徑或相對路徑都可以，這裡是絕對路徑，寫入檔案時演示相對路徑
			//String pathname = "data/version.txt"; // 絕對路徑或相對路徑都可以，這裡是絕對路徑，寫入檔案時演示相對路徑
			File filename = new File(pathname); // 要讀取以上路徑的input。txt檔案
			InputStreamReader reader = new InputStreamReader(new FileInputStream(filename)); // 建立一個輸入流物件reader
			BufferedReader br = new BufferedReader(reader); // 建立一個物件，它把檔案內容轉成計算機能讀懂的語言
			String line = "";
			String versionStr = "";
			while ((line = br.readLine()) != null) {
				versionStr += line;
			}
			int i = versionStr.indexOf("{");
			versionStr = versionStr.substring(i);
			JSONObject versionObj = new JSONObject(versionStr);

			String releaseVersion = versionObj.optString(device);

			String[] releaseVersionArr = releaseVersion.trim().split("\\.");
			String[] versionArr = version.trim().split("\\.");

			boolean result = true;
			for (int j = 0; j < releaseVersionArr.length; j++) {
				int num = 0;
				int rnum = Integer.parseInt(releaseVersionArr[j]);
				if (versionArr.length >= (j + 1)) {
					num = Integer.parseInt(versionArr[j]);
				}
				if (rnum > num) {
					result = false;
					break;
				}
			}
			JSONObject ret = new JSONObject();
			ret.put("result", result);
			ret.put("url", url);
			ret.put("newVersion", releaseVersion);
			retJson = CommonUtils.setResponse(true, Message.APIMSG200, "200", "", ret);

		} catch (Exception e) {
			if (retJson == null || retJson.isNull("result")) {
				retJson = CommonUtils.setResponse(false, Message.APIMSG500, "500", e.getMessage());
			}
			e.printStackTrace();
			log.error(e.getStackTrace());
			return retJson.toString();
		}
		return retJson.toString();
	}

	@RequestMapping(value = "/SetAppVersion", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON
			+ ";charset=UTF-8")
	@ResponseBody
	public String setAppVersion(@RequestBody String jsonString, HttpServletRequest request) throws JSONException {
		try {
			init () ;
			System.out.println("jsonString = "+jsonString);
			checkJSONPara(jsonString);
			String pathname = "C://usr/local/data/"; // 絕對路徑或相對路徑都可以，這裡是絕對路徑，寫入檔案時演示相對路徑
			File filename = new File(pathname); // 要讀取以上路徑的input。txt檔案
			System.out.println(filename.getCanonicalPath());
			System.out.println(filename.exists());
			if (!filename.exists()) {
				filename.mkdir();
			}

			pathname = "C://usr/local/data/version.txt"; // 絕對路徑或相對路徑都可以，這裡是絕對路徑，寫入檔案時演示相對路徑
			filename = new File(pathname); // 要讀取以上路徑的input。txt檔案

			filename.createNewFile(); // 建立新檔案
			BufferedWriter out = new BufferedWriter(new FileWriter(filename));
			out.write(jsonString.toString()); // \r\n即為換行
			out.flush(); // 把快取區內容壓入檔案
			out.close(); // 最後記得關閉檔案
		} catch (Exception e) {
			if (retJson == null || retJson.isNull("result")) {
				retJson = CommonUtils.setResponse(false, Message.APIMSG500, "500", e.getMessage());
			}
			e.printStackTrace();
			log.error(e.getStackTrace());
			return "error";
		}
		return "success";
	}
}
