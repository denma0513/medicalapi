package com.mednet.medical.api.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.MediaType;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mednet.medical.api.utils.CommonUtils;
import com.mednet.medical.api.utils.Message;

import com.mednet.medical.api.base.baseController;
import com.mednet.medical.api.dao.MemberDao;

@Controller
public class MdmberController extends baseController {
	@Autowired
	MemberDao memberDao;
	
	
	
	
	@RequestMapping(value = "/GetAuthTypeApi", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON
			+ ";charset=UTF-8")
	@ResponseBody
	public String getAuthTypeApi(@RequestBody String jsonString, HttpServletRequest request) throws JSONException {
		try {
			init();
			checkJSONPara(jsonString);
			String userID = reqJson.optString("userID");

			if (CommonUtils.isBlank(userID)) {
				retJson = CommonUtils.setResponse(false, Message.APIMSG501, "501", "file path empty");
			} else {
				HashMap<String, Object> ret = memberDao.getAtuhType(userID);
				
				if (ret == null) {
					ret = new HashMap<>();
					ret.put("UserId", userID);
					ret.put("isExpert", "0");
					ret.put("NotRelease", "0");
					ret.put("Release", "0");
					ret.put("drID", "0");
				}
				retJson = CommonUtils.setResponse(true, Message.APIMSG200, "200", "", ret);
			}

		} catch (Exception e) {
			if (retJson == null || retJson.isNull("result")) {
				retJson = CommonUtils.setResponse(false, Message.APIMSG500, "500", e.getMessage());
			}
			e.printStackTrace();
			return retJson.toString();
		}
		return retJson.toString();
	}
}
