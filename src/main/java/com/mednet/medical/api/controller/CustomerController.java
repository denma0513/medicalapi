package com.mednet.medical.api.controller;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.MediaType;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.mednet.medical.api.base.baseController;
import com.mednet.medical.api.service.CustomerService;
import com.mednet.medical.api.service.ExamitemService;
import com.mednet.medical.api.service.LocateService;
import com.mednet.medical.api.utils.CommonUtils;
import com.mednet.medical.api.utils.Message;
import com.mednet.medical.api.utils.RandomGuid;
import com.mednet.medical.api.utils.TimerUtils;

@Controller
@RequestMapping("/customer")
public class CustomerController extends baseController {
	private Log log = LogFactory.getLog("Controller");

	@Autowired
	CustomerService customerService;
	
	
	@RequestMapping(value = "/importCustomerWait", method = RequestMethod.POST , produces = MediaType.APPLICATION_JSON + ";charset=UTF-8")
	@ResponseBody
	public String importCustomerWait(@RequestBody String jsonString, HttpServletRequest request) throws JSONException {
		try {
			init();
			checkJSONPara(jsonString);
			log.info("importCustomerWait request : "+jsonString);
			TimerUtils timer = new TimerUtils();
			timer.setTitle("all").startTimer();
			retJson = customerService.createCustomer(reqJson);
			timer.endTimer();
			if (retJson == null || retJson.isNull("result")) {
				retJson = CommonUtils.setResponse(true, Message.APIMSG200, "200", "");
			}

		} catch (Exception e) {
			if (retJson == null || retJson.isNull("result")) {
				retJson = CommonUtils.setResponse(false, Message.APIMSG500, "500", e.getMessage());
			}
			e.printStackTrace();
			log.error(e.getStackTrace());
			return retJson.toString();
		}
		return retJson.toString();
	}
	
	@RequestMapping(value = "/setUserLog", method = RequestMethod.POST , produces = MediaType.APPLICATION_JSON + ";charset=UTF-8")
	@ResponseBody
	public String setUserLog(@RequestBody String jsonString, HttpServletRequest request) throws JSONException {
		try {
			init();
			checkJSONPara(jsonString);
			log.info("setUserLog request : "+jsonString);
			retJson = customerService.setUserLog(reqJson);

		} catch (Exception e) {
			if (retJson == null || retJson.isNull("result")) {
				retJson = CommonUtils.setResponse(false, Message.APIMSG500, "500", e.getMessage());
			}
			e.printStackTrace();
			log.error(e.getStackTrace());
			return retJson.toString();
		}
		return retJson.toString();
	}
	
	@RequestMapping(value = "/updateUserStatus", method = RequestMethod.POST , produces = MediaType.APPLICATION_JSON + ";charset=UTF-8")
	@ResponseBody
	public String updateUserStatus(@RequestBody String jsonString, HttpServletRequest request) throws JSONException {
		try {
			init();
			checkJSONPara(jsonString);
			log.info("updateUserStatus request : "+jsonString);
			retJson = customerService.updateUserStatus(reqJson);

		} catch (Exception e) {
			if (retJson == null || retJson.isNull("result")) {
				retJson = CommonUtils.setResponse(false, Message.APIMSG500, "500", e.getMessage());
			}
			e.printStackTrace();
			log.error(e.getStackTrace());
			return retJson.toString();
		}
		return retJson.toString();
	}
	
	@RequestMapping(value = "/testApi", method = RequestMethod.POST , produces = MediaType.APPLICATION_JSON + ";charset=UTF-8")
	@ResponseBody
	public String test(@RequestBody String jsonString, HttpServletRequest request) throws JSONException {
		try {
			init();
			//checkJSONPara(jsonString);
		} catch (Exception e) {
			if (retJson == null || retJson.isNull("result")) {
				retJson = CommonUtils.setResponse(false, Message.APIMSG500, "500", e.getMessage());
			}
			e.printStackTrace();
			log.error(e.getStackTrace());
			return retJson.toString();
		}
		return "test";
	}
	
//	@RequestMapping(value = "/testView", method = RequestMethod.POST , produces = MediaType.APPLICATION_JSON + ";charset=UTF-8")
//	@ResponseBody
//	public ModelAndView testView(@RequestBody String jsonString, HttpServletRequest request) throws JSONException {
//		try {
//			//checkJSONPara(jsonString);
//		} catch (Exception e) {
//			if (retJson == null || retJson.isNull("result")) {
//				retJson = CommonUtils.setResponse(false, Message.APIMSG500, "500", e.getMessage());
//			}
//			e.printStackTrace();
//			log.error(e.getStackTrace());
//			return retJson.toString();
//		}
//		return "test";
//	}
	
	public String getIpAddr(HttpServletRequest request) {
		String ip = request.getHeader("x-forwarded-for");
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}
		return ip;
	}

}
