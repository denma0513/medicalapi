package com.mednet.medical.api.controller;

import javax.ws.rs.core.MediaType;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mednet.medical.api.base.baseController;
import com.mednet.medical.api.service.ExamitemService;
import com.mednet.medical.api.service.LocateService;
import com.mednet.medical.api.utils.CommonUtils;
import com.mednet.medical.api.utils.Message;
import com.mednet.medical.api.utils.RandomGuid;

@Controller
@RequestMapping("/examitem")
public class ExamitemController extends baseController {
	private Log log = LogFactory.getLog("Controller");

	@Autowired
	ExamitemService examitemService;

	@RequestMapping(value = "/importExamitem", method = RequestMethod.POST , produces = MediaType.APPLICATION_JSON + ";charset=UTF-8")
	@ResponseBody
	public String importExamitem(@RequestBody String jsonString) throws JSONException {
		try {
			init();
			checkJSONPara(jsonString);
			
			retJson = examitemService.createExamitem(reqJson);

			if (retJson == null || retJson.isNull("result")) {
				retJson = CommonUtils.setResponse(true, Message.APIMSG200, "200", "");
			}

		} catch (Exception e) {
			if (retJson == null || retJson.isNull("result")) {
				retJson = CommonUtils.setResponse(false, Message.APIMSG500, "500", e.getMessage());
			}
			e.printStackTrace();
			log.error(e.getStackTrace());
			return retJson.toString();
		}
		return retJson.toString();
	}

}
