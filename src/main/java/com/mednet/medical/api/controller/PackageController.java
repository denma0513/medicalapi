package com.mednet.medical.api.controller;

import javax.ws.rs.core.MediaType;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mednet.medical.api.base.baseController;
import com.mednet.medical.api.service.LocateService;
import com.mednet.medical.api.service.PackageService;
import com.mednet.medical.api.utils.CommonUtils;
import com.mednet.medical.api.utils.RandomGuid;

@Controller
@RequestMapping("/package")
public class PackageController extends baseController {
	private Log log = LogFactory.getLog("file");

	@Autowired
	PackageService packageService;

	@RequestMapping(value = "/importPackage", method = RequestMethod.POST , produces = MediaType.APPLICATION_JSON + ";charset=UTF-8")
	@ResponseBody
	public String importPackage(@RequestBody String jsonString) throws JSONException {
		try {
			init () ;
			checkJSONPara(jsonString);

			retJson = packageService.createPackage(reqJson);

			if (retJson == null || retJson.isNull("result")) {
				retJson = CommonUtils.setResponse(true, "call api success!", "200", "");
			}

		} catch (Exception e) {
			if (retJson == null || retJson.isNull("result")) {
				retJson = CommonUtils.setResponse(false, "call api fail!", "500", e.getMessage());
			}
			log.error(e.getStackTrace());
			return retJson.toString();
		}
		return retJson.toString();
	}

}
