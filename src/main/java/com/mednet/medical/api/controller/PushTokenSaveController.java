package com.mednet.medical.api.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.File;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.MediaType;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.mednet.medical.api.base.baseController;
import com.mednet.medical.api.utils.CommonUtils;
import com.mednet.medical.api.utils.Message;
import com.turo.pushy.apns.ApnsClient;
import com.turo.pushy.apns.ApnsClientBuilder;
import com.turo.pushy.apns.util.ApnsPayloadBuilder;
import com.turo.pushy.apns.util.SimpleApnsPushNotification;
import com.turo.pushy.apns.util.TokenUtil;
import com.mednet.medical.api.dao.PushTokenSaveDao;
import com.mednet.medical.api.service.PushnotificationService;

@Controller
public class PushTokenSaveController extends baseController{

	@Autowired
	PushTokenSaveDao pushTokenSaveDao;
	@Autowired
	PushnotificationService pushnotificationService;
	private Log log = LogFactory.getLog("PushTokenSave");


	// device:  ios:1  android:2
	@RequestMapping(value = "/RegisterPushTokenSave", produces = MediaType.APPLICATION_JSON + ";charset=UTF-8")
	@ResponseBody
	//public String OrganizationList(@RequestBody String jsonString, HttpServletRequest request) throws JSONException {
	public String customerPushTokenData(@RequestBody String jsonString) throws JSONException {
		try {
			init () ;
			checkJSONPara(jsonString);
			String message = "";
			if (CommonUtils.isBlank(reqJson.optString("pushToken"))) {
				message = "pushtoken can't be null";
			}
			if (CommonUtils.isBlank(reqJson.optString("device"))) {
				message = "device can't be null";
			}
			if (CommonUtils.isBlank(reqJson.optString("customerId"))) {
				message = "customerid can't be null";
			}
			
			if (CommonUtils.isBlank(message)) {
				Thread thread1 = new Thread(new Runnable() {
	                public void run() {
	                	pushTokenSaveDao.createPushToken(reqJson.optString("pushToken"), 
	        					reqJson.optString("device"), 
	        					reqJson.optString("customerId")
	                			
	                			);
	                }
	            });
				thread1.start();
				JSONObject retJson;
				retJson = CommonUtils.setResponse(true,  Message.APIMSG200, "200", "");
				return retJson.toString();
			}
			else {
				retJson = CommonUtils.setResponse(false, "call api fail!", "500", message);
				return retJson.toString();
			}
		} catch (Exception e) {
			if (retJson == null || retJson.isNull("result")) {
				retJson = CommonUtils.setResponse(false, "call api fail!", "500", e.getMessage());
			}
			return retJson.toString();
		}
	}
	

	// device:  ios:1  android:2
	@RequestMapping(value = "/sendMessageWithCustomerId", produces = MediaType.APPLICATION_JSON + ";charset=UTF-8")
	@ResponseBody
	public String sendMessageWithCustomerId(@RequestBody String jsonString) throws JSONException {
		try {
			init () ;
			checkJSONPara(jsonString);
			String message = "";
			if (CommonUtils.isBlank(reqJson.optString("customerId"))) {
				message = "CustomerID can't be null";
			}

			if (CommonUtils.isBlank(message)) {
				Thread thread1 = new Thread(new Runnable() {
	                public void run() {
	                	List<HashMap<String, Object>> result = pushTokenSaveDao.queryPushTokenWithCustomerId(reqJson.optString("customerId"));
	                	for (int i = 0 ; i < result.size() ; i++) {
	            			HashMap<String, Object> obj = result.get(i);
	            			String title = "醫聯網";
	            			String messageText1 = "您有一個好友邀請。";
	            			if (obj.get("device").toString().equals("1")) {
	            				//ios
	            				String Url = "mednetapp://go=addfriend";
	            				pushnotificationService.sendiOSNotification(obj.get("pushtoken").toString(), title, messageText1, false, Url);
	            			} 
	            			else {
	            				//android  
	            				String Url = "mednetapp://shareapp?go=addfriend";
	            				pushnotificationService.sendAndroidNotification(obj.get("pushtoken").toString(), title, messageText1,false, Url);
	            			}
	            		}
	                }
	            });

				thread1.start();
				JSONObject retJson;
				retJson = CommonUtils.setResponse(true,  Message.APIMSG200, "200", "");
				return retJson.toString();
			}
			else {
				retJson = CommonUtils.setResponse(false, "call api fail!", "500", message);
				return retJson.toString();
			}
		} catch (Exception e) {
			if (retJson == null || retJson.isNull("result")) {
				retJson = CommonUtils.setResponse(false, "call api fail!", "500", e.getMessage());
			}
			return retJson.toString();
		}
	}	
	
	

	@RequestMapping(value = "/RejectAddFriend", produces = MediaType.APPLICATION_JSON + ";charset=UTF-8")
	@ResponseBody
	//public String OrganizationList(@RequestBody String jsonString, HttpServletRequest request) throws JSONException {
	public String RejectAddFriendData(@RequestBody String jsonString) throws JSONException {
		try {
			init () ;
			checkJSONPara(jsonString);
			String message = "";
			if (CommonUtils.isBlank(reqJson.optString("appId"))) {
				message = "appid can't be null";
			}
			if (CommonUtils.isBlank(reqJson.optString("customerId"))) {
				message = "customerid can't be null";
			}
			
			if (CommonUtils.isBlank(message)) {
				Thread thread1 = new Thread(new Runnable() {
	                public void run() {
	                	pushTokenSaveDao.rejectAddFriendWithCustomerIdAndAppid(reqJson.optString("customerId"), 
	        					reqJson.optString("appId")
	                			);
	                }
	            });
				thread1.start();
				JSONObject retJson;
				retJson = CommonUtils.setResponse(true,  Message.APIMSG200, "200", "");
				return retJson.toString();
			}
			else {
				retJson = CommonUtils.setResponse(false, "call api fail!", "500", message);
				return retJson.toString();
			}
		} catch (Exception e) {
			if (retJson == null || retJson.isNull("result")) {
				retJson = CommonUtils.setResponse(false, "call api fail!", "500", e.getMessage());
			}
			return retJson.toString();
		}
	}	
	
	
	
}