package com.mednet.medical.api.controller;

import java.util.HashMap;
import java.util.List;
import javax.ws.rs.core.MediaType;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mednet.medical.api.base.baseController;
import com.mednet.medical.api.dao.FixEmailDao;
import com.mednet.medical.api.utils.CommonUtils;
import com.mednet.medical.api.utils.Message;
import com.mednet.medical.api.service.FixEmailService;


@Controller
public class FixEmailController extends baseController{

	
	@Autowired
	FixEmailDao FixEmailDao;
	
	@Autowired
	FixEmailService FixEmailService;
	
	//修正健促部分用戶缺少Email導致無法註冊問題
	@RequestMapping(value = "/FixEmail", produces = MediaType.APPLICATION_JSON + ";charset=UTF-8")
	@ResponseBody
	public String FixEmail(@RequestBody String jsonString) throws JSONException {
		try {
			init () ;
			checkJSONPara(jsonString);
			String message = "";
			if (CommonUtils.isBlank(reqJson.optString("customerAppId"))) {
				message = "customer app id can't be null";
			}
			if (CommonUtils.isBlank(reqJson.optString("email"))) {
				message = "event can't be null";
			}
			FixEmailDao.getCustomerInformation(reqJson.optString("customerAppId"), 
					reqJson.optString("email")
					);
    		System.out.println(reqJson.optString("customerAppId"));
    		System.out.println(reqJson.optString("email"));
			if (CommonUtils.isBlank(message)) {
            	List<HashMap<String, Object>> result =FixEmailDao.getCustomerInformation(reqJson.optString("customerAppId"),reqJson.optString("email"));
            	if(result.size() ==0)
            	{
            		message = "健促DB沒有這個人";
            	}
            	else
            	{
            		
            		HashMap<String, Object> obj = result.get(0);
            		String  oAuth_Type= obj.get("oAuth_Type").toString();
            		String oAuth_id = obj.get("oAuth_id").toString();
            		String customerId = FixEmailService.mednetCreateUser(reqJson.optString("customerAppId"), reqJson.optString("email"), oAuth_Type, oAuth_id);
            		FixEmailDao.fixAppCustomerId(reqJson.optString("customerAppId"), customerId,reqJson.optString("email"));
            	}
            	
            	JSONObject retJson;
				retJson = CommonUtils.setResponse(true,  Message.APIMSG200, "200", "");
				return retJson.toString();
			}
			else {
				retJson = CommonUtils.setResponse(false, "call api fail!", "500", message);
				return retJson.toString();
			}
		} catch (Exception e) {
			if (retJson == null || retJson.isNull("result")) {
				retJson = CommonUtils.setResponse(false, "call api fail!", "500", e.getMessage());
			}
			return retJson.toString();
		}
	}
}
