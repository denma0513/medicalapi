package com.mednet.medical.api.controller;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.MediaType;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mednet.medical.api.base.baseController;
import com.mednet.medical.api.dao.MednetAppLogDao;
import com.mednet.medical.api.utils.CommonUtils;
import com.mednet.medical.api.utils.Message;


@Controller
public class MednetAppLogController extends baseController{
	
	@Autowired
	MednetAppLogDao mednetAppLogDao;
	private Log log = LogFactory.getLog("MednetAppLog");
	
	
	//新增MedNetAppLogEven
	@RequestMapping(value = "/CreateMednetAppLogEven", produces = MediaType.APPLICATION_JSON + ";charset=UTF-8")
	@ResponseBody
	public String createMednetAppLogEven(@RequestBody String jsonString) throws JSONException {
		try {
			init () ;
			checkJSONPara(jsonString);
			String message = "";
			if (CommonUtils.isBlank(reqJson.optString("customerId"))) {
				message = "customer id can't be null";
			}
			if (CommonUtils.isBlank(reqJson.optString("customerAppId"))) {
				message = "customer app id can't be null";
			}
			if (CommonUtils.isBlank(reqJson.optString("event"))) {
				message = "event can't be null";
			}
			if (CommonUtils.isBlank(reqJson.optString("value"))) {
				message = "value can't be null";
			}
			if (CommonUtils.isBlank(reqJson.optString("insertUser"))) {
				message = "insert user can't be null";
			}
			if (CommonUtils.isBlank(reqJson.optString("insertAddress"))) {
				message = "insert address user can't be null";
			}			
			if (CommonUtils.isBlank(reqJson.optString("device"))) {
				message = "device can't be null";
			}
			
			
			if (CommonUtils.isBlank(message)) {
				Thread thread1 = new Thread(new Runnable() {
	                public void run() {
	                	mednetAppLogDao.createMednetAppLogEven(reqJson.optString("customerId"), 
	        					reqJson.optString("customerAppId"),
	        					reqJson.optString("event"),
	        					reqJson.optString("value"),
	        					reqJson.optString("insertUser"),
	        					reqJson.optString("insertAddress"),
	        					reqJson.optString("device")
	                			);
	                }
	            });
				thread1.start();
				JSONObject retJson;
				retJson = CommonUtils.setResponse(true,  Message.APIMSG200, "200", "");
				return retJson.toString();
			}
			else {
				retJson = CommonUtils.setResponse(false, "call api fail!", "500", message);
				return retJson.toString();
			}
		} catch (Exception e) {
			if (retJson == null || retJson.isNull("result")) {
				retJson = CommonUtils.setResponse(false, "call api fail!", "500", e.getMessage());
			}
			return retJson.toString();
		}
	}

	
	
	//新增MedNetAppLogNotice
	@RequestMapping(value = "/CreateMednetAppLogNotice", produces = MediaType.APPLICATION_JSON + ";charset=UTF-8")
	@ResponseBody
	public String createMednetAppLogNotice(@RequestBody String jsonString) throws JSONException {
		try {
			init () ;
			checkJSONPara(jsonString);
			String message = "";
			if (CommonUtils.isBlank(reqJson.optString("customerId"))) {
				message = "customer id can't be null";
			}
			if (CommonUtils.isBlank(reqJson.optString("customerAppId"))) {
				message = "customer app id can't be null";
			}
			if (CommonUtils.isBlank(reqJson.optString("healthType"))) {
				message = "health type can't be null";
			}
			if (CommonUtils.isBlank(reqJson.optString("noticeTime"))) {
				message = "notice time can't be null";
			}
			if (CommonUtils.isBlank(reqJson.optString("action"))) {
				message = "action can't be null";
			}			
			if (CommonUtils.isBlank(reqJson.optString("insertUser"))) {
				message = "insert user can't be null";
			}
			if (CommonUtils.isBlank(reqJson.optString("insertAddress"))) {
				message = "insert address user can't be null";
			}	
			if (CommonUtils.isBlank(reqJson.optString("device"))) {
				message = "device can't be null";
			}
			
			if (CommonUtils.isBlank(message)) {
				Thread thread1 = new Thread(new Runnable() {
	                public void run() {
	                	mednetAppLogDao.createMednetAppLogNotice(reqJson.optString("customerId"), 
	        					reqJson.optString("customerAppId"),
	        					reqJson.optString("healthType"),
	        					reqJson.optString("noticeTime"),
	        					reqJson.optString("action"),
	        					reqJson.optString("insertUser"),
	        					reqJson.optString("insertAddress"),
	        					reqJson.optString("device")
	                			);
	                }
	            });
				thread1.start();
				JSONObject retJson;
				retJson = CommonUtils.setResponse(true,  Message.APIMSG200, "200", "");
				return retJson.toString();
			}
			else {
				retJson = CommonUtils.setResponse(false, "call api fail!", "500", message);
				return retJson.toString();
			}
		} catch (Exception e) {
			if (retJson == null || retJson.isNull("result")) {
				retJson = CommonUtils.setResponse(false, "call api fail!", "500", e.getMessage());
			}
			return retJson.toString();
		}
	}
	
	
	
	//更新或新增HealthOrder
	@RequestMapping(value = "/RegisterMednetAppLogHealthOrder", produces = MediaType.APPLICATION_JSON + ";charset=UTF-8")
	@ResponseBody
	public String registerMednetAppLogHealthOrder(@RequestBody String jsonString) throws JSONException {
		try {
			init () ;
			checkJSONPara(jsonString);
			String message = "";
			if (CommonUtils.isBlank(reqJson.optString("customerId"))) {
				message = "customer id can't be null";
			}
			if (CommonUtils.isBlank(reqJson.optString("customerAppId"))) {
				message = "customer app id can't be null";
			}
			if (CommonUtils.isBlank(reqJson.optString("healthType"))) {
				message = "health type can't be null";
			}
			if (CommonUtils.isBlank(reqJson.optString("healthOrder"))) {
				message = "health order can't be null";
			}
			if (CommonUtils.isBlank(reqJson.optString("insertUser"))) {
				message = "insert user can't be null";
			}			
			if (CommonUtils.isBlank(reqJson.optString("insertAddress"))) {
				message = "insert address can't be null";
			}
			if (CommonUtils.isBlank(reqJson.optString("device"))) {
				message = "device can't be null";
			}	
			
			if (CommonUtils.isBlank(message)) {
				Thread thread1 = new Thread(new Runnable() {
	                public void run() {
	                	mednetAppLogDao.registerMednetAppLogHealthOrder(reqJson.optString("customerId"), 
	        					reqJson.optString("customerAppId"), 
	        					reqJson.optString("healthType"), 
	        					reqJson.optString("healthOrder"), 
	        					reqJson.optString("insertUser"), 
	        					reqJson.optString("insertAddress"),
	        					reqJson.optString("device")
	                			);
	                }
	            });
				thread1.start();
				JSONObject retJson;
				retJson = CommonUtils.setResponse(true,  Message.APIMSG200, "200", "");
				return retJson.toString();
			}
			else {
				retJson = CommonUtils.setResponse(false, "call api fail!", "500", message);
				return retJson.toString();
			}
		} catch (Exception e) {
			if (retJson == null || retJson.isNull("result")) {
				retJson = CommonUtils.setResponse(false, "call api fail!", "500", e.getMessage());
			}
			return retJson.toString();
		}
	}
	
	
	//新增HealthOrder V2
		@RequestMapping(value = "/RegisterMednetAppLogHealthOrderV2", produces = MediaType.APPLICATION_JSON + ";charset=UTF-8")
		@ResponseBody
		public String registerMednetAppLogHealthOrderV2(@RequestBody String jsonString) throws JSONException {
			try {
				init () ;
				checkJSONPara(jsonString);
				String message = "";
				if (CommonUtils.isBlank(reqJson.optString("customerId"))) {
					message = "customer id can't be null";
				}
				if (CommonUtils.isBlank(reqJson.optString("customerAppId"))) {
					message = "customer app id can't be null";
				}
				if (CommonUtils.isBlank(reqJson.optString("insertUser"))) {
					message = "insert user can't be null";
				}			
				if (CommonUtils.isBlank(reqJson.optString("insertAddress"))) {
					message = "insert address can't be null";
				}
				if (CommonUtils.isBlank(reqJson.optString("device"))) {
					message = "device can't be null";
				}	
				
				System.out.println(reqJson.optString("healthData"));
				
				
				
				
				
				
				if (CommonUtils.isBlank(message)) {
					Thread thread1 = new Thread(new Runnable() {
		                public void run() {
		                	JSONArray HealthArray =  reqJson.getJSONArray("healthData");
		    				String blood_sugar="";
		    				String blood_pressure="";
		    				String blood_oxygen="";
		    				String drinking="";
		    				String sleeping="";
		    				String step_count="";
		    				String body_mass_weight="";
		    				
		    				
		    				for (int i=0 ; i< HealthArray.length();i++)
		    				{
		    					JSONObject HealthObject = HealthArray.getJSONObject(i);
		    					int Type = Integer.parseInt(HealthObject.optString("healthType"));
		    					switch (Type)
		    					{
		    					case 1:
		    						blood_sugar = HealthObject.optString("healthOrder");
		    					case 2:	
		    						blood_pressure = HealthObject.optString("healthOrder");
		    					case 3:	
		    						blood_oxygen = HealthObject.optString("healthOrder");
		    					case 4:	
		    						drinking = HealthObject.optString("healthOrder");	
		    					case 5:	
		    						sleeping = HealthObject.optString("healthOrder");	
		    					case 6:	
		    						step_count = HealthObject.optString("healthOrder");
		    					case 7:	
		    						body_mass_weight = HealthObject.optString("healthOrder");
		    					}
		    				}
		                	
		                	
		                	mednetAppLogDao.registerMednetAppLogHealthOrderV2(reqJson.optString("customerId"), 
		        					reqJson.optString("customerAppId"),
		        					reqJson.optString("insertUser"), 
		        					reqJson.optString("insertAddress"),
		        					blood_sugar,
		        					blood_pressure,
		        					blood_oxygen,
		        					drinking,
		        					sleeping,
		        					step_count,
		        					body_mass_weight,
		        					reqJson.optString("device")
		                			);
		                }
		            });
					thread1.start();
					JSONObject retJson;
					retJson = CommonUtils.setResponse(true,  Message.APIMSG200, "200", "");
					return retJson.toString();
				}
				else {
					retJson = CommonUtils.setResponse(false, "call api fail!", "500", message);
					return retJson.toString();
				}
				
			} catch (Exception e) {
				if (retJson == null || retJson.isNull("result")) {
					retJson = CommonUtils.setResponse(false, "call api fail!", "500", e.getMessage());
				}
				return retJson.toString();
			}
		}

}
