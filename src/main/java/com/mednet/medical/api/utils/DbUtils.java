package com.mednet.medical.api.utils;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.sql.DataSource;

public class DbUtils implements Serializable {
	private static final long serialVersionUID = 1L;

	private static DbUtils instance;
	private Connection con = null;
	private ResultSetMetaData meta = null;
	PreparedStatement stmt = null;

	public static DbUtils getInstance() {
		if (instance == null) {
			synchronized (DbConnectionManager.class) {
				if (instance == null) {
					instance = new DbUtils();
				}
			}
		}
		return instance;
	}

	public List<HashMap<String, Object>> excuteQueryL(String query) {
		DbConnectionManager connMgr = DbConnectionManager.getInstance();
		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		List<HashMap<String, Object>> retList = new ArrayList<HashMap<String, Object>>();
		try {
			con = connMgr.getConnection();
			stmt = con.createStatement();
			rs = stmt.executeQuery(query);
			ResultSetMetaData md = rs.getMetaData();
			int columns = md.getColumnCount();
			assert rs.getMetaData().getColumnCount() == 1;
			while (rs.next()) {
				HashMap<String, Object> row = new HashMap<String, Object>(columns);
				for (int i = 1; i <= columns; ++i) {
					row.put(md.getColumnName(i), rs.getObject(i));
				}
				retList.add(row);
			}
			stmt.close();
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}

		return retList;
	}

	public List<HashMap<String, Object>> excuteQueryL(String query, ArrayList<String> columnType,
			ArrayList<String> columnValue) {
		DbConnectionManager connMgr = DbConnectionManager.getInstance();
		Connection con = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		ResultSetMetaData meta = null;
		List<HashMap<String, Object>> retList = new ArrayList<HashMap<String, Object>>();
		try {
			con = connMgr.getConnection();
			stmt = con.prepareStatement(query);
			for (int i = 0; i < columnValue.size(); i++) {
				if ("String".equals(columnType.get(i))) {
					stmt.setString(i + 1, columnValue.get(i));
				} else if ("int".equals(columnType.get(i))) {
					stmt.setInt(i + 1, Integer.parseInt(columnValue.get(i)));
				} else if ("double".equals(columnType.get(i))) {
					stmt.setDouble(i + 1, Double.parseDouble(columnValue.get(i)));
				} else if ("boolean".equals(columnType.get(i))) {
					stmt.setBoolean(i + 1, "true".equals(columnType.get(i)) ? true : false);
				} else {
					stmt.setString(i + 1, columnValue.get(i));
				}
			}

			rs = stmt.executeQuery();
			ResultSetMetaData md = rs.getMetaData();
			int columns = md.getColumnCount();
			assert rs.getMetaData().getColumnCount() == 1;
			while (rs.next()) {
				HashMap<String, Object> row = new HashMap<String, Object>(columns);
				for (int i = 1; i <= columns; ++i) {
					row.put(md.getColumnName(i), rs.getObject(i));
				}
				retList.add(row);
			}
			stmt.close();
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}

		return retList;
	}

	public int excuteQueryCount(String query, ArrayList<String> columnType, ArrayList<String> columnValue) {
		DbConnectionManager connMgr = DbConnectionManager.getInstance();
		Connection con = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		ResultSetMetaData meta = null;
		List<HashMap<String, Object>> retList = new ArrayList<HashMap<String, Object>>();
		int result = 0;
		try {
			con = connMgr.getConnection();
			stmt = con.prepareStatement(query);
			for (int i = 0; i < columnValue.size(); i++) {
				if ("String".equals(columnType.get(i))) {
					stmt.setString(i + 1, columnValue.get(i));
				} else if ("int".equals(columnType.get(i))) {
					stmt.setInt(i + 1, Integer.parseInt(columnValue.get(i)));
				} else if ("double".equals(columnType.get(i))) {
					stmt.setDouble(i + 1, Double.parseDouble(columnValue.get(i)));
				} else if ("boolean".equals(columnType.get(i))) {
					stmt.setBoolean(i + 1, "true".equals(columnType.get(i)) ? true : false);
				} else {
					stmt.setString(i + 1, columnValue.get(i));
				}
			}

			rs = stmt.executeQuery();
			ResultSetMetaData md = rs.getMetaData();
			int columns = md.getColumnCount();
			assert rs.getMetaData().getColumnCount() == 1;
			while (rs.next()) {
				// System.out.println(rs);
				// System.out.println(rs.getInt(0));
				// result = (int) rs.getObject(0);
				// HashMap<String, Object> row = new HashMap<String,
				// Object>(columns);
				for (int i = 1; i <= columns; ++i) {
					result = rs.getInt(i);
					// row.put(md.getColumnName(i), rs.getObject(i));
				}
				// retList.add(row);
			}
			stmt.close();
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}

		return result;
	}

	public int updateExcute(String query, ArrayList<String> columnType, ArrayList<String> columnValue) {
		DbConnectionManager connMgr = DbConnectionManager.getInstance();
		Connection con = null;
		PreparedStatement stmt = null;
		ResultSetMetaData meta = null;
		int result = 0;
		List<HashMap<String, Object>> retList = new ArrayList<HashMap<String, Object>>();
		try {
			con = connMgr.getConnection();
			stmt = con.prepareStatement(query);
			for (int i = 0; i < columnValue.size(); i++) {
				if ("String".equals(columnType.get(i))) {
					stmt.setString(i + 1, columnValue.get(i));
				} else if ("int".equals(columnType.get(i))) {
					stmt.setInt(i + 1, Integer.parseInt(columnValue.get(i)));
				} else if ("double".equals(columnType.get(i))) {
					stmt.setDouble(i + 1, Double.parseDouble(columnValue.get(i)));
				} else {
					stmt.setString(i + 1, columnValue.get(i));
				}
			}
			result = stmt.executeUpdate();
			stmt.close();
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}

		return result;
	}

	public List<HashMap<String, Object>> excuteExpertQAQueryL(String query, ArrayList<String> columnType,
			ArrayList<String> columnValue) {
		DbConnectionManager connMgr = DbConnectionManager.getInstance();
		Connection con = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		ResultSetMetaData meta = null;
		List<HashMap<String, Object>> retList = new ArrayList<HashMap<String, Object>>();
		try {
			con = connMgr.getExpertQAConnection();
			stmt = con.prepareStatement(query);
			for (int i = 0; i < columnValue.size(); i++) {
				if ("String".equals(columnType.get(i))) {
					stmt.setString(i + 1, columnValue.get(i));
				} else if ("int".equals(columnType.get(i))) {
					stmt.setInt(i + 1, Integer.parseInt(columnValue.get(i)));
				} else if ("double".equals(columnType.get(i))) {
					stmt.setDouble(i + 1, Double.parseDouble(columnValue.get(i)));
				} else if ("boolean".equals(columnType.get(i))) {
					stmt.setBoolean(i + 1, "true".equals(columnType.get(i)) ? true : false);
				} else {
					stmt.setString(i + 1, columnValue.get(i));
				}
			}

			rs = stmt.executeQuery();
			ResultSetMetaData md = rs.getMetaData();
			int columns = md.getColumnCount();
			assert rs.getMetaData().getColumnCount() == 1;
			while (rs.next()) {
				HashMap<String, Object> row = new HashMap<String, Object>(columns);
				for (int i = 1; i <= columns; ++i) {
					row.put(md.getColumnName(i), rs.getObject(i));
				}
				retList.add(row);
			}
			stmt.close();
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}

		return retList;
	}

	/**
	 * 更新多筆資料時候用 當有資料更新異常時會自動roolback並回傳null getInstance
	 */
	public DbUtils getMultipleUpdateIntent() {
		DbConnectionManager connMgr = DbConnectionManager.getInstance();
		con = connMgr.getConnection();
		return this;
	}

	/**
	 * 更新多筆資料時候用 當有資料更新異常時會自動roolback並回傳null 下SQL
	 */
	public DbUtils updateMultipleSql(String query, ArrayList<String> columnType, ArrayList<String> columnValue) {
		int result = 0;
		List<HashMap<String, Object>> retList = new ArrayList<HashMap<String, Object>>();
		try {
			stmt = con.prepareStatement(query);
			for (int i = 0; i < columnValue.size(); i++) {
				if ("String".equals(columnType.get(i))) {
					stmt.setString(i + 1, columnValue.get(i));
				} else if ("int".equals(columnType.get(i))) {
					stmt.setInt(i + 1, Integer.parseInt(columnValue.get(i)));
				} else if ("double".equals(columnType.get(i))) {
					stmt.setDouble(i + 1, Double.parseDouble(columnValue.get(i)));
				} else {
					stmt.setString(i + 1, columnValue.get(i));
				}
			}
			result = stmt.executeUpdate();
			if (result <= 0) {
				rollback();
				return null;
			}
		} catch (Exception e) {
			rollback();
			e.printStackTrace();
			return null;
		}

		return this;
	}

	public DbUtils batchUpdateSql(String query) {
		try {
			stmt = con.prepareStatement(query);
		} catch (SQLException e) {
			rollback();
			e.printStackTrace();
		}
		return this;
	}

	/**
	 * 批次新增資料時候用
	 * 
	 */
	public DbUtils batchUpdateColumn(ArrayList<String> columnType, ArrayList<String> columnValue) {
		int result = 0;
		try {
			for (int i = 0; i < columnValue.size(); i++) {
				if ("String".equals(columnType.get(i))) {
					stmt.setString(i + 1, columnValue.get(i));
				} else if ("int".equals(columnType.get(i))) {
					stmt.setInt(i + 1, Integer.parseInt(columnValue.get(i)));
				} else if ("double".equals(columnType.get(i))) {
					stmt.setDouble(i + 1, Double.parseDouble(columnValue.get(i)));
				} else {
					stmt.setString(i + 1, columnValue.get(i));
				}
			}
			stmt.addBatch();
			// result = stmt.executeUpdate();
		} catch (Exception e) {
			rollback();
			e.printStackTrace();
			return null;
		}
		return this;
	}

	/**
	 * 批次commit
	 */
	public DbUtils batchCommit() {
		try {
			int[] result = stmt.executeBatch();
			if (result == null || result.length <= 0) {
				rollback();
			}
			con.commit();
		} catch (SQLException e) {
			rollback();
			e.printStackTrace();
			return null;
		} finally {
			try {
				con.close();
				stmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			}
		}
		return this;
	}

	/**
	 * 更新多筆資料時候用 當有資料更新異常時會自動roolback並回傳null commit
	 */
	public DbUtils commit() {
		try {
			con.commit();
		} catch (SQLException e) {
			rollback();
			e.printStackTrace();
			return null;
		} finally {
			try {
				con.close();
				stmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			}
		}
		return this;
	}

	/**
	 * 更新多筆資料時候用 當有資料更新異常時會自動roolback並回傳null rollback
	 */
	public DbUtils rollback() {
		try {
			con.rollback();
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			try {
				con.close();
				stmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			}
		}
		return this;
	}
	//對ExpertQA DB 執行Query

	public int excuteExpertQAQuery(String query, ArrayList<String> columnType, ArrayList<String> columnValue) {
		DbConnectionManager connMgr = DbConnectionManager.getInstance();
		Connection con = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		ResultSetMetaData meta = null;
		List<HashMap<String, Object>> retList = new ArrayList<HashMap<String, Object>>();
		int result = 0;
		
		try {
			con = connMgr.getExpertQAConnection();
			stmt = con.prepareStatement(query);
			for (int i = 0; i < columnValue.size(); i++) {
				if ("String".equals(columnType.get(i))) {
					stmt.setString(i + 1, columnValue.get(i));
				} else if ("int".equals(columnType.get(i))) {
					stmt.setInt(i + 1, Integer.parseInt(columnValue.get(i)));
				} else if ("double".equals(columnType.get(i))) {
					stmt.setDouble(i + 1, Double.parseDouble(columnValue.get(i)));
				} else {
					stmt.setString(i + 1, columnValue.get(i));
				}
			}
		
			result = stmt.executeUpdate();
			stmt.close();
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}

		return result;
	}
	
	
	public int updateExpertQAExcute(String query, ArrayList<String> columnType, ArrayList<String> columnValue) {
		DbConnectionManager connMgr = DbConnectionManager.getInstance();
		Connection con = null;
		PreparedStatement stmt = null;
		ResultSetMetaData meta = null;
		int result = 0;
		List<HashMap<String, Object>> retList = new ArrayList<HashMap<String, Object>>();
		try {
			con = connMgr.getExpertQAConnection();
			stmt = con.prepareStatement(query);
			for (int i = 0; i < columnValue.size(); i++) {
				if ("String".equals(columnType.get(i))) {
					stmt.setString(i + 1, columnValue.get(i));
				} else if ("int".equals(columnType.get(i))) {
					stmt.setInt(i + 1, Integer.parseInt(columnValue.get(i)));
				} else if ("double".equals(columnType.get(i))) {
					stmt.setDouble(i + 1, Double.parseDouble(columnValue.get(i)));
				} else {
					stmt.setString(i + 1, columnValue.get(i));
				}
			}
			result = stmt.executeUpdate();
			stmt.close();
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}

		return result;
	}
}
