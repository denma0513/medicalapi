package com.mednet.medical.api.utils;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;
import com.microsoft.sqlserver.jdbc.SQLServerDriver;

public class DbConnectionManager implements Serializable {
	private static final long serialVersionUID = 1L;
	private static DbConnectionManager instance;
	private DataSource ds;

	public static DbConnectionManager getInstance() {
		if (instance == null) {
			synchronized (DbConnectionManager.class) {
				if (instance == null) {
					instance = new DbConnectionManager();
				}
			}
		}
		return instance;
	}

	/**
	 * datasource connection
	 * @author dennis
	 * */
	public Connection getConnection() {
		return getConnection(Const.SERVER_POOL_NAME);
		//return getConnection(Const.SQLSERVER_POOL_NAME);
	}
	/**
	 * datasource connection
	 * @author dennis
	 * */
	public Connection getConnection(String pool) {
		String poolName = "";
		Connection connection = null;
		try {
			if ("".equals(Const.isBlank(pool))) {
				poolName = Const.SERVER_POOL_NAME;
				//poolName = Const.SQLSERVER_POOL_NAME;
			} else {
				poolName = pool;
			}
			
			Class.forName(Const.SQLSERVER_DRIVER);
			String connectionUrl = String.format(Const.SQLSERVER_DB_URL, Const.SQLSERVER_MEDNET, Const.SQLSERVER_DB_USERNAME, Const.SQLSERVER_DB_PASSWORD);
			connection = DriverManager.getConnection(connectionUrl); 
			return connection;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}

	/**
	 * datasource connection
	 * @author dennis
	 * */
	public Connection getExpertQAConnection() {
		return getExpertQAConnection(Const.SERVER_POOL_NAME);
		//return getConnection(Const.SQLSERVER_POOL_NAME);
	}
	/**
	 * datasource connection
	 * @author dennis
	 * */
	public Connection getExpertQAConnection(String pool) {
		String poolName = "";
		Connection connection = null;
		try {
			if ("".equals(Const.isBlank(pool))) {
				poolName = Const.SERVER_POOL_NAME;
				//poolName = Const.SQLSERVER_POOL_NAME;
			} else {
				poolName = pool;
			}
			
			Class.forName(Const.SQLSERVER_DRIVER);
			String connectionUrl = String.format(Const.MEDNET_DB_URL, Const.MEDNET_DB_NAME, Const.MEDNET_DB_USERNAME, Const.MEDNET_DB_PASSWORD);
			connection = DriverManager.getConnection(connectionUrl); 
			return connection;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public Connection getMedNetConnection(String pool) {
		String poolName = "";
		Connection connection = null;
		try {
			if ("".equals(Const.isBlank(pool))) {
				poolName = Const.SERVER_POOL_NAME;
				//poolName = Const.SQLSERVER_POOL_NAME;
			} else {
				poolName = pool;
			}
			
			Class.forName(Const.SQLSERVER_DRIVER);
			String connectionUrl = String.format(Const.MEDNET_DB_URL, Const.MEDNET_DB_NAME_MedNet, Const.MEDNET_DB_USERNAME, Const.MEDNET_DB_PASSWORD);
			connection = DriverManager.getConnection(connectionUrl); 
			return connection;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}	
	
	public Connection getMedNetAppConnection(String pool) {
		String poolName = "";
		Connection connection = null;
		try {
			if ("".equals(Const.isBlank(pool))) {
				poolName = Const.SERVER_POOL_NAME;
				//poolName = Const.SQLSERVER_POOL_NAME;
			} else {
				poolName = pool;
			}
			
			Class.forName(Const.SQLSERVER_DRIVER);
			String connectionUrl = String.format(Const.MEDNET_DB_URL, Const.MEDNET_DB_NAME_APP, Const.MEDNET_DB_USERNAME, Const.MEDNET_DB_PASSWORD);
			connection = DriverManager.getConnection(connectionUrl); 
			return connection;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
}
