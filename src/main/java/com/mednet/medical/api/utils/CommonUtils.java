package com.mednet.medical.api.utils;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

public class CommonUtils {

	public static boolean isBlank(String key) {
		if (key == null || "".equals(key)) {
			return true;
		}
		return false;
	}
	
	public static String chkValue(Object obj) {
		return chkValue(obj, null);
	}

	public static String chkValue(Object obj, String replaceStr) {
		if (obj == null) {
			return replaceStr==null?"":replaceStr;
		}
		return chkValue(obj.toString(), replaceStr);
	}
	
	public static String chkValue(String str) {
		return chkValue(str, null);
	}
	
	public static String chkValue(String str, String replaceStr) {
		if (str != null && !"".equals(str)) {
			return str;
		}
		return replaceStr==null?"":replaceStr;
	}
	
	public static JSONObject setResponse (boolean result, String message, String code, String exception){
		JSONObject retJson = new JSONObject();
		retJson.put("result", result?"success":"fail");
		retJson.put("code", code);
		retJson.put("message", message);
		retJson.put("exception", exception);
		return retJson;	
	}
	
	public static JSONObject setResponse (boolean result, String message, String code, String exception, JSONArray data){
		JSONObject retJson = new JSONObject();
		retJson.put("result", result?"success":"fail");
		retJson.put("code", code);
		retJson.put("message", message);
		retJson.put("exception", exception);
		retJson.put("data", data);
		return retJson;
	}
	
	public static JSONObject setResponse (boolean result, String message, String code, String exception, JSONObject data){
		JSONObject retJson = new JSONObject();
		retJson.put("result", result?"success":"fail");
		retJson.put("code", code);
		retJson.put("message", message);
		retJson.put("exception", exception);
		retJson.put("data", data);
		return retJson;
	}
	
	public static JSONObject setResponse (boolean result, String message, String code, String exception, Object data){
		JSONObject retJson = new JSONObject();
		retJson.put("result", result?"success":"fail");
		retJson.put("code", code);
		retJson.put("message", message);
		retJson.put("exception", exception);
		retJson.put("data", data);
		return retJson;	
	}
	
	public static JSONObject parseJson (String jsonString){
		JSONObject retJson = new JSONObject();
		try {
			int i = jsonString.indexOf("{");
			jsonString = jsonString.substring(i);
			return new JSONObject(jsonString);
		}catch(Exception e){
			
		}
		return retJson;
	}
	
}
