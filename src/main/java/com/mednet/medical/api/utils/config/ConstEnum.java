package com.mednet.medical.api.utils.config;

public interface ConstEnum {
	public String getRequest();

	public String getServer();

	public String getMednetScema();

	public String getEmsScema();
}
