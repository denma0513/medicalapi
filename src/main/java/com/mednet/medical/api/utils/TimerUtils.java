package com.mednet.medical.api.utils;

public class TimerUtils {
	private long startTime;
	private long endTime;
	private double totleTime;
	private String title;
	
	public TimerUtils startTimer(){
		startTime = System.currentTimeMillis();
		totleTime = 0;
		return this;
	}

	public TimerUtils endTimer(){
		endTime = System.currentTimeMillis();
		totleTime = endTime - startTime;
		System.out.println(title+": "+totleTime/1000);
		return this;
	}
	
	public double getTotleTime(){
		return totleTime;
	}

	public TimerUtils setTitle(String title) {
		this.title = title;
		return this;
	}
}
