package com.mednet.medical.api.utils.config;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

public class ConstManager implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final boolean ConstEnum = false;

	private static HttpServletRequest request;

	private static String ip = "http://survey.med-net.com:8080";
	private static int port = 0;

	public static String getMednetSchema(HttpServletRequest req) {
		String schema = "MDENET_DEMO";
		try {
			request = req;
			ConstEnum machine = getConstEnum();
			if (machine != null) {
				schema = machine.getMednetScema();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return schema;
	}

	public static String getEmsSchema(HttpServletRequest req) {
		String schema = "EMS_DEMO";
		try {
			request = req;
			ConstEnum machine = getConstEnum();
			if (machine != null) {
				schema = machine.getEmsScema();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return schema;
	}

	private static ConstEnum getConstEnum() throws Exception {
		ConstEnum machine = null;

		List<String> ipList = new ArrayList<String>();
		ip = request.getRemoteAddr();
		port = request.getRemotePort();
		System.out.println(ip + " " + port);
		ipList.add(ip);

		if (!ipList.isEmpty()) {
			for (ConstEnum tmp : ConstEnumEmpl.values()) {
				for (String ipStr : ipList) {
					machine = tmp;
					break;
				}
			}
		}

		if (machine == null) {
			throw new Exception();
		} else {
			return machine;
		}

	}

}
