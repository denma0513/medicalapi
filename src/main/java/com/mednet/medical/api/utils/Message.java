package com.mednet.medical.api.utils;

public class Message {
	
	//###### API Message start ######
	public static String  APIMSG200 = "成功。"; 
	public static String  APIMSG500 = "呼叫失敗。"; 
	public static String  APIMSG501 = "呼叫失敗，呼叫API參數有誤，請檢查參數是否正確"; 
	public static String  APIMSG502 = "呼叫失敗，機構代碼有誤。"; 
	public static String  APIMSG503 = "呼叫失敗，客戶資料有誤。"; 
	//###### API Message end ######
	
	
	//###### result Message ######
	public static String  RESULTMSG200 = "成功。";
	public static String  RESULTMSG500 = "新增失敗，健診項目有缺，請確認是否有該檢診項目。";
	public static String  RESULTMSG501 = "新增失敗，請檢查匯入參數，或從後台建立資料。";
	public static String  RESULTMSG502 = "新增失敗，此套組沒有任何健診項目。";
	public static String  RESULTMSG503 = "新增失敗，此客戶資料缺少欄位%s，請重新匯入此筆資料。";
	public static String  RESULTMSG504 = "新增失敗，請至少匯入身分證字號，護照號碼或居留許可證。";
	public static String  RESULTMSG505 = "新增失敗，時間格式有誤。";
	public static String  RESULTMSG506 = "新增失敗，身分證字號，護照號碼或居留許可證有誤。";
	public static String  RESULTMSG507 = "新增失敗，查無此套組名稱。";
	public static String  RESULTMSG508 = "新增失敗，請至少輸入欄位%s或%s。";
	
		
	
}
