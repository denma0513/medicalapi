package com.mednet.medical.api.utils.config;

public enum ConstEnumEmpl implements ConstEnum{

//	local {
//		public String getRequest() {
//			return "0:0:0:0:0:0:0:1,127.0.0.1";
//		}
//
//		public String getServer() {
//			return "http://survey.med-net.com:81";
//		}
//
//		@Override
//		public String getMednetScema() {
//			return "APP_MEDNET";
//		}
//
//		@Override
//		public String getEmsScema() {
//			return "APP_EMS";
//		}
//
//	},	
	
	Mednet82 {
		public String getRequest() {
			return "0:0:0:0:0:0:0:1,127.0.0.1";
		}

		public String getServer() {
			return "http://survey.med-net.com:82";
		}

		@Override
		public String getMednetScema() {
			return "MEDNET_DEMO";
		}

		@Override
		public String getEmsScema() {
			return "EMS_DEMO";
		}

	},
}
