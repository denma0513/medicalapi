package com.mednet.medical.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.mednet.medical.api.base.baseController;

@Controller
public class IndexViewCrotroller extends baseController {
	@RequestMapping(value = "/Index.do", method = RequestMethod.GET)
	public ModelAndView Index(HttpServletRequest request, HttpServletResponse response) throws JSONException {
		ModelAndView modelAndView = new ModelAndView("index");
		modelAndView.addObject("model", "index");
		return modelAndView;
	}

	@RequestMapping(value = "/Version.do", method = RequestMethod.GET)
	public ModelAndView Version(HttpServletRequest request, HttpServletResponse response, HttpSession session)
			throws JSONException {
		ModelAndView modelAndView = new ModelAndView("index");
		modelAndView.addObject("model", "version");
		return modelAndView;

	}
	@RequestMapping(value = "/PushNotification.do", method = RequestMethod.GET)
	public ModelAndView pushNotification(HttpServletRequest request, HttpServletResponse response, HttpSession session)
			throws JSONException {
		ModelAndView modelAndView = new ModelAndView("index");
		modelAndView.addObject("model", "pushNotification");
		return modelAndView;

	}
	
	@RequestMapping(value = "/Link.do", method = RequestMethod.GET)
	public ModelAndView link(HttpServletRequest request, HttpServletResponse response, HttpSession session)
			throws JSONException {
		ModelAndView modelAndView = new ModelAndView("index");
		modelAndView.addObject("model", "轉址中，請耐心等待");
		return modelAndView;

	}	
	
	@RequestMapping(value = "/ActionLink.do", method = RequestMethod.GET)
	public ModelAndView actionlink(HttpServletRequest request, HttpServletResponse response, HttpSession session)
			throws JSONException {
		ModelAndView modelAndView = new ModelAndView("index");
		modelAndView.addObject("model", "轉址中，請耐心等待。");
		return modelAndView;

	}	
	
	@RequestMapping(value = "/Push.do", method = RequestMethod.GET)
	public ModelAndView push(HttpServletRequest request, HttpServletResponse response, HttpSession session)
			throws JSONException {
		ModelAndView modelAndView = new ModelAndView("index");
		modelAndView.addObject("model", "push");
		return modelAndView;

	}	
}
